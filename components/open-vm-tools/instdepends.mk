INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
# dkms
INSTDEPENDS += doxygen
INSTDEPENDS += libcunit1-dev
INSTDEPENDS += libdumbnet-dev
#INSTDEPENDS += libfuse-dev
INSTDEPENDS += libgtk2.0-dev
INSTDEPENDS += libgtkmm-2.4-dev
INSTDEPENDS += libicu-dev
INSTDEPENDS += libnotify-dev
#INSTDEPENDS += libpam0g-dev
INSTDEPENDS += libpam-dev
#INSTDEPENDS += libprocps-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxinerama-dev
INSTDEPENDS += libxss-dev
INSTDEPENDS += libxtst-dev
INSTDEPENDS += dh-autoreconf
# dh-systemd
INSTDEPENDS += libmspack-dev
INSTDEPENDS += libssl1.0-dev
#INSTDEPENDS += libxerces-c-dev
#INSTDEPENDS += libxml-security-c-dev
#
INSTDEPENDS += libkrb5-dev
INSTDEPENDS += libcrypt-dev
INSTDEPENDS += librpcsvc-dev
