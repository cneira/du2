mpi4py (2.0.0-2.1+deb9u1+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- Denis Kozadaev <denis@dilos.org>  Sat, 06 Oct 2018 18:34:39 +0300

mpi4py (2.0.0-2.1+deb9u1) stretch; urgency=medium

  [ Andreas Beckmann ]
  * Non-maintainer upload.
  * Backport fix from 2.0.0-3 to stretch.

  [ Stuart Prescott ]
  * Fix sover list used in dlopen so that current libmpi.so is found
    (Closes: #860476)

 -- Andreas Beckmann <anbe@debian.org>  Sat, 03 Mar 2018 17:12:50 +0100

mpi4py (2.0.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * In debian/rules:override_dh_auto_test, treat test errors as warnings.
    Closes: #856349.

 -- Drew Parsons <dparsons@debian.org>  Wed, 08 Mar 2017 16:18:00 +0800

mpi4py (2.0.0-2) unstable; urgency=medium

  * Point to libm.so.6 in the test, not libm.so (Closes: #817884)
    (Thanks Alastair McKinstry)
  * Removed dangling symlink /usr/include/mpi4py.  include/ is symlinked
    for each python version under /usr/include/python*.*/mpi4py/
    (Closes: #805301)

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 02 Aug 2016 10:42:00 -0400

mpi4py (2.0.0-1) unstable; urgency=medium

  * New upstream release
    - should be compatible with python 3.5 (Closes: 793839)
  * debian/watch adjusted to monitor bitbucket
  * debian/control
    - boosted policy to 3.9.7
  * debian/rules
    - hardcode sphinx date for reproducible build (Closes: #788476)
      Thanks Juan Picca

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 29 Feb 2016 08:23:56 -0500

mpi4py (1.3.1+hg20131106-2) unstable; urgency=medium

  * debian/rules
    - assure absent reliance on network via http*_proxy env var
  * debian/patches
    - up_tests_network_attr tag BaseTestSpawn tests as relying on network
      access and exclude them from running at pkg build time (Closes: #769801)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 16 Nov 2014 15:06:21 -0500

mpi4py (1.3.1+hg20131106-1) unstable; urgency=low

  * Fresh upstream snapshot which includes 1.3.1 stable release
  * debian/rules:
    - get-orig-dev-source to generate snapshot tarballs out from HG
  * debian/rules,control:
    - use dh_autoreconf and rebuild Cython extensions if cython is newer
      than 0.19.1
    - use dh_sphinxdoc (build-depends version for sphinx boosted to
      1.0.7+dfsg~) and remove manual symlinking of jquery.js
      (Closes: #725601)
  * debian/control
    - boost policy to 3.9.4

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 06 Nov 2013 13:22:56 -0500

mpi4py (1.3+hg20130509-1) unstable; urgency=low

  * Fresh upstream snapshot:
    - fixes Cython 0.19 compatibility (Closes: #707314)
    - incorporates patches cython_version_check.patch,
      up_test_win_python3.3.patch)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 09 May 2013 12:33:57 -0400

mpi4py (1.3+hg20120611-3) unstable; urgency=medium

  * Create a suffixed (e.g. python3.2mu) python3 directory matching the
    one present on the system for the given version of python3  (Closes:
    #700995)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 20 Feb 2013 14:51:54 -0500

mpi4py (1.3+hg20120611-2) unstable; urgency=low

  * Cherry-picked patch from upstream for python3.3 compatibility (failing
    unittests) (Closes: #691244)

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 23 Oct 2012 10:23:29 -0400

mpi4py (1.3+hg20120611-1) unstable; urgency=low

  [ Bradley M. Froehle ]
  * New snapshot from the upstream's release-1.3 branch in HG, revision
    4f6ac1ea8b9d.  Includes post-release bugfixes for kFreeBSD, pickling
    etc.

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 11 Jun 2012 21:47:41 -0400

mpi4py (1.3-1) unstable; urgency=low

  [ Bradley M. Froehle ]
  * New upstream release (Closes: #675520)
  * Import "Safer Cython version check" from upstream to allow building
    against pre-release versions of Cython
  * Fix FTBFS issues on some platforms where the default MPI implementation
    is not OpenMPI
  * python-mpi executables are not packaged; they are not required by OpenMPI
    or MPICH2

  [ Yaroslav Halchenko ]
  * Boosted mpi-default-bin into Depends from Recommends since according
    to tireless Bradley otherwise it is useless (Closes: #670768)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 06 Jun 2012 16:15:51 -0400

mpi4py (1.2.2-4) unstable; urgency=low

  [ Bradley M. Froehle ]
  * Build using dh_python2
  * Add package for Python 3 (Closes: #673911)

  [ Yaroslav Halchenko ]
  * Guard all for loops with 'set -e' to guarantee failure if any
    iteration fails
  * up_no_modlibs patch to exclude seems unneeded linking against MODLIBS
    which carry unnecessary -lffi on recent debian systems preventing correct
    build
  * debian/copyright: adjusted for dep5 and list Bradley 
  * Boosted policy to 3.9.3 -- no further changes

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 22 May 2012 13:24:18 -0400

mpi4py (1.2.2-3) unstable; urgency=low

  [ Bradley M. Froehle ]
  * Symbolic link /usr/include/mpi4py ->
    /usr/share/pyshared/mpi4py/include/mpi4py (Closes: #650329)

  [ Yaroslav Halchenko ]
  * Boosted policy to 3.9.2 -- no changes
  * Adjusted gbp.conf to do overlay build
  * Ajudsted debian/copyright to comply with changes in DEP5
  * Added python-support to build-depends (Closes: #642451)
  * pyshared -> pymodules for -dbg package

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 28 Nov 2011 17:09:46 -0500

mpi4py (1.2.2-2) unstable; urgency=low

  * Use mpi-default-{dev,bin} as *Depends to allow building on platforms
    without openmpi implementation available

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 07 Apr 2011 09:26:13 -0400

mpi4py (1.2.2-1) unstable; urgency=low

  * Initial release (Closes: #604161)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 06 Apr 2011 15:09:51 -0400
