#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1


# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_MULTIARCH  ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
libdir = /usr/lib/$(DEB_HOST_MULTIARCH)


COMMA = ,
ifneq (,$(filter parallel=%,$(subst $(COMMA), ,$(DEB_BUILD_OPTIONS))))
  NJOBS := -j $(subst parallel=,,$(filter parallel=%,$(subst $(COMMA), ,$(DEB_BUILD_OPTIONS))))
endif

# shared library versions, option 1
version=2.0.5
major=2
# option 2, assuming the library is created as src/.libs/libfoo.so.2.0.5 or so
#version=`ls src/.libs/lib*.so.* | \
# awk '{if (match($$0,/[0-9]+\.[0-9]+\.[0-9]+$$/)) print substr($$0,RSTART)}'`
#major=`ls src/.libs/lib*.so.* | \
# awk '{if (match($$0,/\.so\.[0-9]+$$/)) print substr($$0,RSTART+4)}'`

configure-stamp:
	dh_testdir
	dh_autoreconf
	$(shell DEB_CFLAGS_MAINT_APPEND=-Wall dpkg-buildflags --export=cmdline) \
	    ./configure --host=$(DEB_HOST_GNU_TYPE) --build=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr --mandir=\$${prefix}/share/man --infodir=\$${prefix}/share/info \
		--with-ui --with-readline --disable-rpath --libdir=$(libdir)
	touch configure-stamp

build: build-stamp
build-stamp:  configure-stamp
	dh_testdir

	$(MAKE) $(NJOBS)
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	$(MAKE) check
endif
	pod2man --utf8 debian/ispellaff2myspell \
		> debian/ispellaff2myspell.1

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp

	[ ! -f Makefile ] || $(MAKE) distclean
	rm -f debian/ispellaff2myspell.1
	rm -f config.log

	dh_autoreconf_clean
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

	$(MAKE) install DESTDIR=$(CURDIR)/debian/tmp

	dh_install

binary-indep: build install

binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs ChangeLog
	dh_installdocs
	dh_installexamples src/tools/example.cxx
	dh_install
	dh_installman
	ln -s libhunspell-1.4.a debian/libhunspell-dev/$(libdir)/libhunspell.a
	ln -s libhunspell-1.4.so debian/libhunspell-dev/$(libdir)/libhunspell.so
	dh_strip
	dh_compress
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_perl
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install 
