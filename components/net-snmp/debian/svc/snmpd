#!/bin/sh
#
# Copyright (c) 2012-2018, DilOS.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"),  to deal in
# the Software without restriction, including without  limitation  the rights to
# use, copy, modify, merge, publish, distribute, sublicense,  and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT  WARRANTY  OF  ANY  KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT,  TORT  OR  OTHERWISE,  ARISING  FROM,  OUT  OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Standard prolog
#
. /lib/svc/share/smf_include.sh

method=$1

CONF=/etc/snmp/snmpd.conf

#
if [ ! -f ${CONF} ]; then
	echo "Error: Configuration file '${CONF}' not found." \
	    "  See snmpd(8)."
	exit $SMF_EXIT_ERR_CONFIG
fi

# for debug, uncomment it
#set -x

LANG=C
export LANG

DAEMON=/usr/sbin/snmpd
PIDFILE=/var/run/snmpd.pid
MIBS=
SNMPDRUN=yes
SNMPDOPTS="-Lsd -Lf /dev/null -u Debian-snmp -g Debian-snmp -I -smux,mteTrigger,mteTriggerConf -p ${PIDFILE}"

test -x $DAEMON || exit $SMF_EXIT_ERR

if [ -r /etc/default/snmpd ]; then
	. /etc/default/snmpd
fi

snmpd_start()
{

	if [ -L /var/run/agentx ]; then
		rm -f /var/run/agentx
	fi
	if [ ! -d /var/run/agentx ]; then
		mkdir -p /var/run/agentx
	fi
	if [ -f ${CONF} ]; then
		start-stop-daemon --quiet --start --oknodo --exec ${DAEMON}\
			-- $SNMPDOPTS
	fi
}


snmpd_stop()
{

	if [ `pgrep -f ${DAEMON}` -eq `cat ${PIDFILE}` ]; then
		kill -TERM `cat ${PIDFILE}`
	fi
	[ -f ${PIDFILE} ] && rm -f ${PIDFILE}
}


case $method in
'start')
	snmpd_start
	;;

'stop')
	snmpd_stop
	;;

'restart')
	snmpd_stop
	snmpd_start
	;;

*)
	echo "Usage: $0 [start|stop|restart]"
	exit $SMF_EXIT_ERR
	;;
esac
exit $SMF_EXIT_OK
