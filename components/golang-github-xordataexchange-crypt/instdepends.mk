INSTDEPENDS += debhelper
INSTDEPENDS += dh-golang
INSTDEPENDS += golang-any
INSTDEPENDS += dh-exec
INSTDEPENDS += golang-github-armon-consul-api-dev
# golang-etcd-server-dev | golang-github-coreos-etcd-dev
INSTDEPENDS += golang-golang-x-crypto-dev
