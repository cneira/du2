INSTDEPENDS += debhelper
INSTDEPENDS += automake
INSTDEPENDS += autopoint
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += file
INSTDEPENDS += gettext
INSTDEPENDS += ghostscript
INSTDEPENDS += imagemagick
INSTDEPENDS += libassuan-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += libcurl4-gnutls-dev
INSTDEPENDS += libgcrypt20-dev
INSTDEPENDS += libgnutls28-dev
INSTDEPENDS += libgpg-error-dev
INSTDEPENDS += libksba-dev
INSTDEPENDS += libldap2-dev
INSTDEPENDS += libnpth0-dev
INSTDEPENDS += libreadline-dev
INSTDEPENDS += librsvg2-bin
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libusb-1.0-0-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += texinfo
INSTDEPENDS += transfig
INSTDEPENDS += zlib1g-dev

