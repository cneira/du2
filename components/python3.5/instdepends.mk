INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += quilt autoconf
INSTDEPENDS += lsb-release
INSTDEPENDS += sharutils
INSTDEPENDS += libreadline-dev
INSTDEPENDS += libncursesw5-dev
# gcc (>= 4:6.3)
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libbz2-dev
INSTDEPENDS += liblzma-dev
INSTDEPENDS += libgdbm-dev
INSTDEPENDS += libdb-dev
INSTDEPENDS += tk-dev
INSTDEPENDS += blt-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += libexpat1-dev
INSTDEPENDS += libmpdec-dev
# libbluetooth-dev [!hurd-i386 !kfreebsd-i386 !kfreebsd-amd64],
# locales [!armel !avr32 !hppa !ia64 !mipsel],
INSTDEPENDS += libsqlite3-dev
INSTDEPENDS += libffi-dev
# libgpm2 [!hurd-i386 !kfreebsd-i386 !kfreebsd-amd64],
INSTDEPENDS += mime-support
# , netbase, bzip2,
INSTDEPENDS += time
INSTDEPENDS += python3
# net-tools, xvfb,
INSTDEPENDS += xauth
#Build-Depends-Indep:
INSTDEPENDS += python3-sphinx
#
#INSTDEPENDS += developer-dtrace
