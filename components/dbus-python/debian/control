Source: dbus-python
Section: devel
Priority: optional
Maintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>
Uploaders:
 Sjoerd Simons <sjoerd@debian.org>,
 Sebastian Dröge <slomo@debian.org>,
 Simon McVittie <smcv@debian.org>,
 Loic Minier <lool@dooz.org>,
Build-Depends:
 autoconf,
 autoconf-archive,
 automake,
 dbus,
 debhelper (>= 9),
 dh-autoreconf,
 dh-python,
 dpkg-dev (>= 1.16.1),
 libdbus-1-dev (>= 1.6),
 libdbus-glib-1-dev (>= 0.71),
 python-all-dbg (>= 2.6.6-3~),
 python-all-dev (>= 2.6.6-3~),
 python-gi,
 python3-all-dbg,
 python3-all-dev,
 python3-gi,
 xmlto,
Build-Depends-Indep:
 python-docutils,
 python-epydoc (>= 3.0~beta1),
Standards-Version: 3.9.7
X-Python-Version: >= 2.6
X-Python3-Version: >= 3.2
Homepage: http://www.freedesktop.org/wiki/Software/DBusBindings#Python
Vcs-Git: https://anonscm.debian.org/git/pkg-utopia/dbus-python.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-utopia/dbus-python.git

Package: python-dbus
Section: python
Architecture: any
Depends:
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Recommends:
 python-gi,
Suggests:
 python-dbus-dbg,
 python-dbus-doc,
Replaces:
 python2.4-dbus,
Conflicts:
 python2.4-dbus,
Breaks:
 gajim (<< 0.11.1),
 gnome-osd (<< 0.12.0),
 python-qt4-dbus (<< 4.8.3-3),
Provides:
 ${python:Provides},
Description: simple interprocess messaging system (Python interface)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 This package provides a Python interface to D-Bus.
 .
 See the dbus description for more information about D-Bus in general.

Package: python-dbus-dbg
Section: debug
Priority: extra
Architecture: linux-any
Depends:
 python-dbg,
 python-dbus (= ${binary:Version}),
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: debug build of the D-Bus Python 2 interface
 This package provides a version of the python-dbus package built for
 debugging versions of Python 2, and debug symbols for python-dbus.

Package: python-dbus-dev
Section: python
Architecture: all
Depends:
 libdbus-1-dev (>= 1.6),
 ${misc:Depends},
Breaks:
 python-dbus (<< 1.0),
 python-dbus-common (<< 1.0),
Replaces:
 python-dbus (<< 1.0),
 python-dbus-common (<< 1.0),
Description: main loop integration development files for python-dbus
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 This package provides development files required to compile main-loop
 integration modules for python-dbus, such as dbus.mainloop.pyqt5 provided
 by PyQt. It is Python-version-independent: packages that build-depend
 on python-dbus-dev should also build-depend on python-dbus,
 python3-dbus, python-dbus-dbg and/or python3-dbus-dbg.

Package: python-dbus-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
Suggests:
 python-dbus | python3-dbus,
Description: Documentation for the D-Bus Python interface
 This package provides text and HTML documentation, and examples, for the
 python-dbus and python3-dbus packages.

Package: python-dbus-tests
Section: python
Architecture: any
Depends:
 dbus,
 python-dbus,
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Recommends:
 gnome-desktop-testing,
 python-gi,
Description: simple interprocess messaging system (Python interface - tests)
 D-Bus is a message bus, used for sending messages between applications.
 .
 This package contains automated tests for the "dbus" Python bindings for
 the reference D-Bus implementation, to be run under Python 2.
 .
 See the dbus description for more information about D-Bus in general.

Package: python3-dbus
Section: python
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 python3-gi,
Suggests:
 python-dbus-doc,
 python3-dbus-dbg,
Provides:
 ${python3:Provides},
Description: simple interprocess messaging system (Python 3 interface)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 This package provides a Python 3 interface to D-Bus.
 .
 See the dbus description for more information about D-Bus in general.

Package: python3-dbus-dbg
Section: debug
Priority: extra
Architecture: linux-any
Depends:
 python3-dbg,
 python3-dbus (= ${binary:Version}),
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Description: debug build of the D-Bus Python 3 interface
 This package provides a version of the python3-dbus package built for
 debugging versions of Python 3, and debug symbols for python3-dbus.

Package: python3-dbus-tests
Section: python
Architecture: any
Depends:
 dbus,
 python3-dbus,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 gnome-desktop-testing,
 python3-gi,
Description: simple interprocess messaging system (Python 3 interface - tests)
 D-Bus is a message bus, used for sending messages between applications.
 .
 This package contains automated tests for the "dbus" Python bindings for
 the reference D-Bus implementation, to be run under Python 3.
 .
 See the dbus description for more information about D-Bus in general.
