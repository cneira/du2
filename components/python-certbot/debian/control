Source: python-certbot
Section: python
Priority: optional
Maintainer: Debian Let's Encrypt <team+letsencrypt@tracker.debian.org>
Uploaders: Harlan Lieberman-Berg <hlieberman@debian.org>,
           Francois Marier <francois@debian.org>
Build-Depends: debhelper (>= 11~),
               dh-python,
               python-sphinx,
               python-sphinx-rtd-theme,
               python-repoze.sphinx.autointerface,
               python3,
               python3-acme (>= 0.26.0~),
               python3-configargparse (>= 0.10.0),
               python3-configobj,
               python3-cryptography (>= 1.2),
	       python3-distutils | python3 (<< 3.6.5~),
	       python3-josepy,
               python3-mock,
               python3-parsedatetime (>= 1.3),
               python3-repoze.sphinx.autointerface,
               python3-requests (>= 2.4.3),
               python3-rfc3339,
               python3-setuptools (>= 1.0),
               python3-sphinx (>= 1.6),
               python3-sphinx-rtd-theme,
               python3-tz,
               python3-zope.component,
               python3-zope.interface
Standards-Version: 4.2.1
Homepage: https://certbot.eff.org/
Vcs-Git: https://salsa.debian.org/letsencrypt-team/certbot/certbot.git
Vcs-Browser: https://salsa.debian.org/letsencrypt-team/certbot/certbot
Testsuite: autopkgtest-pkg-python
#Rules-Requires-Root: no

Package: python3-certbot
Architecture: all
Depends: python3-acme (>= 0.25.0~),
         python3-requests (>= 2.4.3),
         ${misc:Depends},
         ${python3:Depends}
Recommends: certbot
Suggests: python-certbot-doc
Breaks: python-certbot-nginx (<< 0.20.0),
        python-certbot-apache (<< 0.20.0),
        python-letsencrypt (<= 0.6.0)
Replaces: python-letsencrypt
Description: main library for certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the main libraries.

Package: certbot
Section: web
Architecture: all
Depends: python3-certbot (= ${source:Version}),
         ${misc:Depends},
         ${python3:Depends}
Breaks: letsencrypt (<= 0.6.0)
Replaces: letsencrypt
Provides: letsencrypt
Suggests: python3-certbot-apache,
	  python3-certbot-nginx,
          python-certbot-doc
Description: automatically configure HTTPS using Let's Encrypt
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the main application, including the standalone
 and the manual authenticators.

Package: python-certbot-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: client documentation for certbot
 The objective of Certbot, Let's Encrypt, and the ACME (Automated
 Certificate Management Environment) protocol is to make it possible
 to set up an HTTPS server and have it automatically obtain a
 browser-trusted certificate, without any human intervention. This is
 accomplished by running a certificate management agent on the web
 server.
 .
 This agent is used to:
 .
   - Automatically prove to the Let's Encrypt CA that you control the website
   - Obtain a browser-trusted certificate and set it up on your web server
   - Keep track of when your certificate is going to expire, and renew it
   - Help you revoke the certificate if that ever becomes necessary.
 .
 This package contains the documentation.

Package: letsencrypt
Section: oldlibs
Architecture: all
Depends: certbot,
         ${misc:Depends}
Description: transitional dummy package
 This is a transitional dummy package for the rename of letsencrypt to certbot.
 It can safely be removed.
