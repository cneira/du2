Description: Fix some spelling errors in fio binary, fio manpage and HOWTO.
Author: Martin Steigerwald <martin.steigerwald@teamix.de>

--- a/HOWTO
+++ b/HOWTO
@@ -685,13 +685,13 @@
 				the next. Multiple files can still be
 				open depending on 'openfiles'.
 
-			zipf	Use a zipfian distribution to decide what file
+			zipf	Use a Zipfian distribution to decide what file
 				to access.
 
-			pareto	Use a pareto distribution to decide what file
+			pareto	Use a Pareto distribution to decide what file
 				to access.
 
-			gauss	Use a gaussian (normal) distribution to decide
+			gauss	Use a Gaussian (normal) distribution to decide
 				what file to access.
 
 		For random, roundrobin, and sequential, a postfix can be
@@ -998,7 +998,7 @@
 		random		Uniform random distribution
 		zipf		Zipf distribution
 		pareto		Pareto distribution
-		gauss		Normal (gaussian) distribution
+		gauss		Normal (Gaussian) distribution
 		zoned		Zoned random distribution
 
 		When using a zipf or pareto distribution, an input value
@@ -1696,7 +1696,7 @@
 
 log_hist_msec=int Same as log_avg_msec, but logs entries for completion
 		latency histograms. Computing latency percentiles from averages of
-		intervals using log_avg_msec is innacurate. Setting this option makes
+		intervals using log_avg_msec is inacurate. Setting this option makes
 		fio log histogram entries over the specified period of time, reducing
 		log sizes for high IOPS devices while retaining percentile accuracy.
 		See log_hist_coarseness as well. Defaults to 0, meaning histogram
--- a/fio.1
+++ b/fio.1
@@ -592,13 +592,13 @@
 Do each file in the set sequentially.
 .TP
 .B zipf
-Use a zipfian distribution to decide what file to access.
+Use a Zipfian distribution to decide what file to access.
 .TP
 .B pareto
-Use a pareto distribution to decide what file to access.
+Use a Pareto distribution to decide what file to access.
 .TP
 .B gauss
-Use a gaussian (normal) distribution to decide what file to access.
+Use a Gaussian (normal) distribution to decide what file to access.
 .RE
 .P
 For \fBrandom\fR, \fBroundrobin\fR, and \fBsequential\fR, a postfix can be
@@ -1575,7 +1575,7 @@
 .BI log_hist_msec \fR=\fPint
 Same as \fBlog_avg_msec\fR, but logs entries for completion latency histograms.
 Computing latency percentiles from averages of intervals using \fBlog_avg_msec\fR
-is innacurate. Setting this option makes fio log histogram entries over the
+is inacurate. Setting this option makes fio log histogram entries over the
 specified period of time, reducing log sizes for high IOPS devices while
 retaining percentile accuracy. See \fBlog_hist_coarseness\fR as well. Defaults
 to 0, meaning histogram logging is disabled.
--- a/options.c
+++ b/options.c
@@ -2234,7 +2234,7 @@
 			  },
 			  { .ival = "gauss",
 			    .oval = FIO_FSERVICE_GAUSS,
-			    .help = "Normal (gaussian) distribution",
+			    .help = "Normal (Gaussian) distribution",
 			  },
 			  { .ival = "roundrobin",
 			    .oval = FIO_FSERVICE_RR,
