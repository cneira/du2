INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
# gmp is only used when running the tests from the separate test tarball
INSTDEPENDS += libgmp-dev
#Build-Depends-Indep:
INSTDEPENDS += python3-sphinx
