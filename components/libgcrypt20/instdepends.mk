INSTDEPENDS += debhelper
INSTDEPENDS += libgpg-error-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += automake
#Build-Depends-Indep:
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += texlive-generic-recommended
INSTDEPENDS += texinfo
# mingw-w64, libgpg-error-mingw-w64-dev
