websockify (0.8.0+dfsg1-7+dilos3) unstable; urgency=medium

  * build for dilos

 -- Igor Kozhukhov <igor@dilos.org>  Sun, 25 Feb 2018 14:46:49 +0300

websockify (0.8.0+dfsg1-7) unstable; urgency=medium

  * 4th iteration of the non-linux TCP socket option patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 11 Oct 2016 16:14:26 +0200

websockify (0.8.0+dfsg1-6) unstable; urgency=medium

  * Fixes TCP_KEEPCNT-and-TCP_KEEPIDLE-not-available-in-non-linux.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 11 Oct 2016 08:53:34 +0200

websockify (0.8.0+dfsg1-5) unstable; urgency=medium

  * Do not attempt to log error in the TCP_KEEPCNT patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Oct 2016 14:11:34 +0200

websockify (0.8.0+dfsg1-4) unstable; urgency=medium

  * Add patch for non-linux platforms where TCP_KEEPCNT doesn't exist
    (Closes: #840035).
  * debian/source/options: add .gitreview to extend-diff-ignore.

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Oct 2016 10:50:22 +0200

websockify (0.8.0+dfsg1-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Oct 2016 15:34:15 +0200

websockify (0.8.0+dfsg1-2) experimental; urgency=medium

  * Add support for Python 3:
    - d/control: Add websockify-common, python{3}-websockify packages,
      amend dependencies and deal with upgrades.
    - d/rules,*.{prerm,postinst}: Tweak install of websockify binary
      for alternative use.
  * Enable unit test execution:
    - d/control: Add requires BD's for unit tests.
    - d/p/mox3.patch: Update to use mox3 for py2 and py3.
    - d/rules: Execute unit tests for python 2 and 3.

 -- James Page <james.page@ubuntu.com>  Thu, 30 Jun 2016 09:51:15 +0100

websockify (0.8.0+dfsg1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed debian/copyright ordering.
  * Standards-Version is now 3.9.8 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Jun 2016 16:00:26 +0200

websockify (0.6.1+dfsg1-1) unstable; urgency=high

  * New upstream release (LP: #1447675).
  * Switching to gz compression for the orig.tar.
  * Added dh-python as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 15 Jun 2015 18:04:28 +0200

websockify (0.6.0+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: is now 3.9.5.
  * Removed fix-rebind.so-not-found-when-installed.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Jun 2014 20:17:08 +0800

websockify (0.5.1+dfsg1-3) unstable; urgency=low

  * Fixes the shell wrapper for rebind. Thanks to Philipp Hahn
    <hahn@univention.de> for reporting and his fix (Closes: #726304).
  * Cleans correctly, and allow 2 builds in a raw.

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Dec 2013 06:20:11 +0000

websockify (0.5.1+dfsg1-2) unstable; urgency=low

  * Added upstream patch to fix rebind.so not found (Closes: #719889).

 -- Thomas Goirand <zigo@debian.org>  Sat, 28 Sep 2013 22:45:51 +0800

websockify (0.5.1+dfsg1-1) unstable; urgency=low

  * New upstream release.
  * Ran wrap-and-sort.
  * Do not install include/web-socket-js/WebSocketMain.swf in setup.py
    (patches setup.py).

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 May 2013 16:00:31 +0800

websockify (0.3.0+dfsg1-6) unstable; urgency=low

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 May 2013 08:15:02 +0000

websockify (0.3.0+dfsg1-5) experimental; urgency=low

  * Really adds build-depends: openstack-pkg-tools.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Apr 2013 13:38:29 +0000

websockify (0.3.0+dfsg1-4) experimental; urgency=low

  * Added missing build-dependency: openstack-pkg-tools.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Apr 2013 20:50:49 +0800

websockify (0.3.0+dfsg1-3) experimental; urgency=low

  * The breaks + replaces now include the novnc epoch (this was missing in
    previous upload), thanks to Jeremy Bicha for reporting (Closes: #706357).

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Apr 2013 18:08:34 +0800

websockify (0.3.0+dfsg1-2) experimental; urgency=low

  * Added Breaks: + Replaces: novnc (<< 0.4+dfsg+1-6) since websockify now
    holds the /usr/bin/websockify and /usr/bin/rebind binaries.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Apr 2013 16:35:29 +0000

websockify (0.3.0+dfsg1-1) experimental; urgency=low

  * Initial release. (Closes: #701822).

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Feb 2013 01:22:51 +0800
