Source: matplotlib
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Build-Depends: debhelper (>= 7),
               dh-python,
               dvipng,
               ghostscript,
               graphviz,
               inkscape,
               ipython,
               libfreetype6-dev,
               libgtk2.0-dev,
               libpng-dev,
               locales-all,
               python-all-dbg (>= 2.4.4-6),
               python-all-dev (>= 2.3.5-7),
               python3-all-dbg,
               python3-all-dev,
               python-cycler (>= 0.10.0),
               python3-cycler (>= 0.10.0),
               python-dateutil,
               python3-dateutil,
               python-colorspacious,
               python3-colorspacious,
               python-cairocffi,
               python3-cairocffi,
               python-cxx-dev,
               python3-cxx-dev,
               python-functools32,
               python-gi,
               python3-gi,
               python-gtk2-dev,
               python-mock,
               python3-mock,
               python-mpltoolkits.basemap <!stage1>,
               python3-mpltoolkits.basemap <!stage1>,
               python-nose,
               python3-nose,
               python-numpy,
               python-numpy-dbg,
               python-numpydoc,
               python3-numpy,
               python3-numpy-dbg,
#               python-pandas,
#               python3-pandas,
               python-pil,
               python3-pil,
               python-pkg-resources,
               python3-pkg-resources,
               python-pyparsing (>= 1.5.6),
               python3-pyparsing (>= 1.5.6),
               python-qt4,
               python3-pyqt4,
               python-pyqt5,
               python3-pyqt5,
               python-setuptools,
               python3-setuptools,
               python-six (>= 1.4),
               python3-six (>= 1.4),
               python-sphinx (>= 1.0.7+dfsg),
               python-subprocess32,
               python-tk (>= 2.5.2-1.1),
               python-tk-dbg (>= 2.5.2-1.1),
               python3-tk,
               python3-tk-dbg,
               python-tornado,
               python3-tornado,
               python-tz,
               python3-tz,
               python-wxgtk3.0,
               python-xlwt,
               tcl8.6-dev,
               texlive-fonts-recommended,
               texlive-latex-extra,
               texlive-latex-recommended,
               tk8.6-dev,
               xauth,
               xvfb,
               zlib1g-dev
XS-Python-Version: all
X-Python3-Version: >= 3.2
Standards-Version: 3.9.8
Homepage: http://matplotlib.org/
Vcs-Git: https://anonscm.debian.org/git/python-modules/packages/matplotlib.git
Vcs-Browser: https://anonscm.debian.org/cgit/python-modules/packages/matplotlib.git
XS-Testsuite: autopkgtest

Package: python-matplotlib
Architecture: any
Depends: python-dateutil,
         python-matplotlib-data (>= ${source:Version}),
         python-pyparsing (>= 1.5.6),
         python-tz,
         libjs-jquery,
         libjs-jquery-ui,
         ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Recommends: python-glade2,
            python-imaging,
            python-tk (>= 2.5.2-1.1)
Enhances: ipython
Suggests: dvipng,
          ffmpeg,
          gir1.2-gtk-3.0,
          ghostscript,
          inkscape,
          ipython (>= 0.6.3),
          librsvg2-common,
          python-cairocffi,
          python-configobj,
          python-excelerator,
          python-gobject,
          python-gtk2,
          python-matplotlib-doc,
          python-nose,
          python-qt4,
          python-scipy,
          python-sip,
          python-tornado,
          python-traits (>= 2.0),
          python-wxgtk3.0,
          texlive-extra-utils,
          texlive-latex-extra,
          ttf-staypuft
Description: Python based plotting system in a style similar to Matlab
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.

Package: python3-matplotlib
Architecture: any
Depends: python3-dateutil,
         python-matplotlib-data (>= ${source:Version}),
         python3-pyparsing (>= 1.5.6),
         python3-six (>= 1.4),
         python3-tz,
         libjs-jquery,
         libjs-jquery-ui,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: python3-pil,
            python3-tk
Enhances: ipython3
Suggests: dvipng,
          ffmpeg,
          gir1.2-gtk-3.0,
          ghostscript,
          inkscape,
          ipython3,
          librsvg2-common,
          python-matplotlib-doc,
          python3-cairocffi,
          python3-gi,
          python3-gi-cairo,
          python3-gobject,
          python3-nose,
          python3-pyqt4,
          python3-scipy,
          python3-sip,
          python3-tornado,
          texlive-extra-utils,
          texlive-latex-extra,
          ttf-staypuft
Description: Python based plotting system in a style similar to Matlab (Python 3)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains the Python 3 version of matplotlib.

Package: python-matplotlib-data
Architecture: all
Depends: fonts-lyx, ${misc:Depends}, ttf-bitstream-vera
Description: Python based plotting system (data package)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains architecture independent data for python-matplotlib.

Package: python-matplotlib-doc
Architecture: all
Section: doc
Depends: libjs-jquery, ${misc:Depends}
Description: Python based plotting system (documentation package)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains documentation for python-matplotlib.

Package: python-matplotlib-dbg
Architecture: linux-any
Section: debug
Priority: extra
Depends: python-all-dbg,
         python-matplotlib (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Python based plotting system (debug extension)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains the debug extension for python-matplotlib.

Package: python3-matplotlib-dbg
Architecture: linux-any
Section: debug
Priority: extra
Depends: python3-all-dbg,
         python3-matplotlib (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Python based plotting system (debug extension, Python 3)
 Matplotlib is a pure Python plotting library designed to bring
 publication quality plotting to Python with a syntax familiar to
 Matlab users. All of the plotting commands in the pylab interface can
 be accessed either via a functional interface familiar to Matlab
 users or an object oriented interface familiar to Python users.
 .
 This package contains the debug extension for python3-matplotlib.
