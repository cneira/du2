INSTDEPENDS += debhelper
INSTDEPENDS += dpkg-dev
INSTDEPENDS += libgl1-mesa-dri
INSTDEPENDS += libqt5opengl5-dev
INSTDEPENDS += libqt5xmlpatterns5-dev
INSTDEPENDS += pkg-kde-tools
INSTDEPENDS += python
INSTDEPENDS += qtbase5-private-dev
INSTDEPENDS += xauth
#INSTDEPENDS += xvfb [fixme]
# Build-Depends-Indep:
INSTDEPENDS += libqt5sql5-sqlite
INSTDEPENDS += qtbase5-doc-html
INSTDEPENDS += qttools5-dev-tools
