commit 1795c50fbf0a82f7a8333c966e64ef11a6e8af31
Author: Richard Lowe <richlowe@richlowe.net>
Date:   Sat Oct 27 02:44:09 2012 +0100

    Implement -fstrict-calling-conventions
    
    Stock GCC is overly willing to violate the ABI when calling local functions,
    such that it passes arguments in registers on i386.  This hampers debugging
    with anything other than a fully-aware DWARF debugger, and is generally not
    something we desire.
    
    Implement a flag which disables this behaviour, enabled by default.  The flag is
    global, though only effective on i386, to more easily allow its globalization
    later which, given the odds, is likely to be necessary.
    
    # Conflicts:
    #       gcc/config/i386/i386.c

--- a/gcc/common.opt
+++ b/gcc/common.opt
@@ -2236,6 +2236,10 @@
 Common Report Var(flag_strict_aliasing) Optimization
 Assume strict aliasing rules apply.
 
+fstrict-calling-conventions
+Common Report Var(flag_strict_calling_conventions) Init(1)
+Use strict ABI calling conventions even for static functions
+
 fstrict-overflow
 Common Report Var(flag_strict_overflow) Optimization
 Treat signed overflow as undefined.
--- a/gcc/config/i386/i386.c
+++ b/gcc/config/i386/i386.c
@@ -2645,7 +2645,7 @@
   const int align_func;
 };
 
-/* This table must be in sync with enum processor_type in i386.h.  */ 
+/* This table must be in sync with enum processor_type in i386.h.  */
 static const struct ptt processor_target_table[PROCESSOR_max] =
 {
   {"generic", &generic_cost, 16, 10, 16, 10, 16},
@@ -4508,14 +4508,14 @@
 	| PTA_SSE4A | PTA_CX16 | PTA_ABM | PTA_SSSE3 | PTA_SSE4_1
 	| PTA_SSE4_2 | PTA_AES | PTA_PCLMUL | PTA_AVX | PTA_FMA4
 	| PTA_XOP | PTA_LWP | PTA_BMI | PTA_TBM | PTA_F16C
-	| PTA_FMA | PTA_PRFCHW | PTA_FXSR | PTA_XSAVE 
+	| PTA_FMA | PTA_PRFCHW | PTA_FXSR | PTA_XSAVE
 	| PTA_XSAVEOPT | PTA_FSGSBASE},
      {"bdver4", PROCESSOR_BDVER4, CPU_BDVER4,
 	PTA_64BIT | PTA_MMX | PTA_SSE | PTA_SSE2 | PTA_SSE3
 	| PTA_SSE4A | PTA_CX16 | PTA_ABM | PTA_SSSE3 | PTA_SSE4_1
-	| PTA_SSE4_2 | PTA_AES | PTA_PCLMUL | PTA_AVX | PTA_AVX2 
-	| PTA_FMA4 | PTA_XOP | PTA_LWP | PTA_BMI | PTA_BMI2 
-	| PTA_TBM | PTA_F16C | PTA_FMA | PTA_PRFCHW | PTA_FXSR 
+	| PTA_SSE4_2 | PTA_AES | PTA_PCLMUL | PTA_AVX | PTA_AVX2
+	| PTA_FMA4 | PTA_XOP | PTA_LWP | PTA_BMI | PTA_BMI2
+	| PTA_TBM | PTA_F16C | PTA_FMA | PTA_PRFCHW | PTA_FXSR
 	| PTA_XSAVE | PTA_XSAVEOPT | PTA_FSGSBASE | PTA_RDRND
 	| PTA_MOVBE | PTA_MWAITX},
       {"znver1", PROCESSOR_ZNVER1, CPU_ZNVER1,
@@ -5303,7 +5303,7 @@
   /* For all chips supporting SSE2, -mfpmath=sse performs better than
      fpmath=387.  The second is however default at many targets since the
      extra 80bit precision of temporaries is considered to be part of ABI.
-     Overwrite the default at least for -ffast-math. 
+     Overwrite the default at least for -ffast-math.
      TODO: -mfpmath=both seems to produce same performing code with bit
      smaller binaries.  It is however not clear if register allocation is
      ready for this setting.
@@ -6296,9 +6296,9 @@
 
   tree old_optimize = build_optimization_node (&global_options);
 
-  /* Get the optimization options of the current function.  */  
+  /* Get the optimization options of the current function.  */
   tree func_optimize = DECL_FUNCTION_SPECIFIC_OPTIMIZATION (fndecl);
- 
+
   if (!func_optimize)
     func_optimize = old_optimize;
 
@@ -6306,7 +6306,7 @@
   memset (&func_options, 0, sizeof (func_options));
   init_options_struct (&func_options, NULL);
   lang_hooks.init_options_struct (&func_options);
- 
+
   cl_optimization_restore (&func_options,
 			   TREE_OPTIMIZATION (func_optimize));
 
@@ -7110,6 +7110,7 @@
 	 and callee not, or vice versa.  Instead look at whether the callee
 	 is optimized or not.  */
       if (target && opt_for_fn (target->decl, optimize)
+	  && !flag_strict_calling_conventions
 	  && !(profile_flag && !flag_fentry))
 	{
 	  cgraph_local_info *i = &target->local;
@@ -7201,6 +7202,7 @@
       /* TARGET_SSE_MATH */
       && (target_opts_for_fn (target->decl)->x_ix86_fpmath & FPMATH_SSE)
       && opt_for_fn (target->decl, optimize)
+      && !flag_strict_calling_conventions
       && !(profile_flag && !flag_fentry))
     {
       cgraph_local_info *i = &target->local;
@@ -7811,7 +7813,7 @@
 
    The midde-end can't deal with the vector types > 16 bytes.  In this
    case, we return the original mode and warn ABI change if CUM isn't
-   NULL. 
+   NULL.
 
    If INT_RETURN is true, warn ABI change if the vector mode isn't
    available for function return value.  */
@@ -9858,7 +9860,7 @@
 
 	  /* Unless ABI prescibes otherwise,
 	     MMX/3dNow values are returned in MM0 if available.  */
-	     
+
 	  if (size == 8)
 	    return TARGET_VECT8_RETURNS || !TARGET_MMX;
 
@@ -14213,7 +14215,7 @@
 	  addr = XEXP (addr, 0);
 	  if (CONST_INT_P (addr))
 	    return 0;
-	}	      
+	}
       else if (GET_CODE (addr) == AND
 	       && const_32bit_mask (XEXP (addr, 1), DImode))
 	{
@@ -15736,7 +15738,7 @@
 #ifdef SUB_TARGET_RECORD_STUB
       SUB_TARGET_RECORD_STUB (name);
 #endif
-    }      
+    }
 
   rtl = gen_const_mem (Pmode, rtl);
   set_mem_alias_set (rtl, ix86_GOT_alias_set ());
@@ -15783,7 +15785,7 @@
   return x;
 }
 
-/* Expand SYMBOL into its corresponding dllimport or refptr symbol.  WANT_REG 
+/* Expand SYMBOL into its corresponding dllimport or refptr symbol.  WANT_REG
    is true if we require the result be a register.  */
 
 static rtx
@@ -16879,7 +16881,7 @@
 	    case 2:
 	      putc ('w', file);
 	      break;
-  
+
 	    case 4:
 	      putc ('l', file);
 	      break;
@@ -18207,7 +18209,7 @@
 }
 
 /* Check if a 256bit AVX register is referenced in stores.   */
- 
+
 static void
 ix86_check_avx256_stores (rtx dest, const_rtx, void *data)
  {
@@ -18216,7 +18218,7 @@
       bool *used = (bool *) data;
       *used = true;
     }
- } 
+ }
 
 /* Calculate mode of upper 128bit AVX registers after the insn.  */
 
@@ -19287,7 +19289,7 @@
 	    t = gen_reg_rtx (V4SFmode);
 	  else
 	    t = op0;
-	    
+
 	  if (TARGET_SSE_PARTIAL_REG_DEPENDENCY)
 	    emit_move_insn (t, CONST0_RTX (V4SFmode));
 	  else
@@ -20323,7 +20325,7 @@
 
   op = gen_rtx_SET (dst, gen_rtx_fmt_ee (code, mode, dst, src));
   clob = gen_rtx_CLOBBER (VOIDmode, gen_rtx_REG (CCmode, FLAGS_REG));
-  
+
   emit_insn (gen_rtx_PARALLEL (VOIDmode, gen_rtvec (2, op, clob)));
 }
 
@@ -23966,7 +23968,7 @@
 
   if (TARGET_XOP)
     {
-      /* The XOP VPPERM insn supports three inputs.  By ignoring the 
+      /* The XOP VPPERM insn supports three inputs.  By ignoring the
 	 one_operand_shuffle special case, we avoid creating another
 	 set of constant vectors in memory.  */
       one_operand_shuffle = false;
@@ -25872,7 +25874,7 @@
    DONE_LABEL is a label after the whole copying sequence. The label is created
    on demand if *DONE_LABEL is NULL.
    MIN_SIZE is minimal size of block copied.  This value gets adjusted for new
-   bounds after the initial copies. 
+   bounds after the initial copies.
 
    DESTMEM/SRCMEM are memory expressions pointing to the copies block,
    DESTPTR/SRCPTR are pointers to the block. DYNAMIC_CHECK indicate whether
@@ -26181,7 +26183,7 @@
   return dst;
 }
 
-/* Return true if ALG can be used in current context.  
+/* Return true if ALG can be used in current context.
    Assume we expand memset if MEMSET is true.  */
 static bool
 alg_usable_p (enum stringop_alg alg, bool memset, bool have_as)
@@ -26514,7 +26516,7 @@
 	with specified algorithm.
 
      4) Epilogue: code copying tail of the block that is too small to be
-	handled by main body (or up to size guarded by prologue guard). 
+	handled by main body (or up to size guarded by prologue guard).
 
   Misaligned move sequence
 
@@ -26721,7 +26723,7 @@
 
   /* Do the cheap promotion to allow better CSE across the
      main loop and epilogue (ie one load of the big constant in the
-     front of all code.  
+     front of all code.
      For now the misaligned move sequences do not have fast path
      without broadcasting.  */
   if (issetmem && ((CONST_INT_P (val_exp) || misaligned_prologue_used)))
@@ -28929,7 +28931,7 @@
 		       using topological ordering in the region.  */
 		    if (rgn == CONTAINING_RGN (e->src->index)
 			&& BLOCK_TO_BB (bb->index) > BLOCK_TO_BB (e->src->index))
-		      add_dependee_for_func_arg (first_arg, e->src); 
+		      add_dependee_for_func_arg (first_arg, e->src);
 		  }
 	      }
 	    insn = first_arg;
@@ -29436,7 +29438,7 @@
      other unit can not rely on the alignment.
 
      Exclude va_list type.  It is the common case of local array where
-     we can not benefit from the alignment.  
+     we can not benefit from the alignment.
 
      TODO: Probably one should optimize for size only when var is not escaping.  */
   if (TARGET_64BIT && optimize_function_for_speed_p (cfun)
@@ -36282,7 +36284,7 @@
   convert_expr = build1 (CONVERT_EXPR, ptr_type_node,
 	     		 build_fold_addr_expr (version_decl));
   result_var = create_tmp_var (ptr_type_node);
-  convert_stmt = gimple_build_assign (result_var, convert_expr); 
+  convert_stmt = gimple_build_assign (result_var, convert_expr);
   return_stmt = gimple_build_return (result_var);
 
   if (predicate_chain == NULL_TREE)
@@ -36309,7 +36311,7 @@
       gimple_seq_add_stmt (&gseq, call_cond_stmt);
 
       predicate_chain = TREE_CHAIN (predicate_chain);
-      
+
       if (and_expr_var == NULL)
         and_expr_var = cond_var;
       else
@@ -36350,7 +36352,7 @@
   gimple_set_bb (return_stmt, bb2);
 
   bb3 = e23->dest;
-  make_edge (bb1, bb3, EDGE_FALSE_VALUE); 
+  make_edge (bb1, bb3, EDGE_FALSE_VALUE);
 
   remove_edge (e23);
   make_edge (bb2, EXIT_BLOCK_PTR_FOR_FN (cfun), 0);
@@ -36406,7 +36408,7 @@
     P_FMA4,
     P_XOP,
     P_PROC_XOP,
-    P_FMA,    
+    P_FMA,
     P_PROC_FMA,
     P_BMI2,
     P_AVX2,
@@ -36479,11 +36481,11 @@
       cl_target_option_save (&cur_target, &global_options);
       target_node = ix86_valid_target_attribute_tree (attrs, &global_options,
 						      &global_options_set);
-    
+
       gcc_assert (target_node);
       new_target = TREE_TARGET_OPTION (target_node);
       gcc_assert (new_target);
-      
+
       if (new_target->arch_specified && new_target->arch > 0)
 	{
 	  switch (new_target->arch)
@@ -36642,7 +36644,7 @@
       *predicate_list = predicate_chain;
     }
 
-  return priority; 
+  return priority;
 }
 
 /* This compares the priority of target features in function DECL1
@@ -36661,7 +36663,7 @@
 
 /* V1 and V2 point to function versions with different priorities
    based on the target ISA.  This function compares their priorities.  */
- 
+
 static int
 feature_compare (const void *v1, const void *v2)
 {
@@ -36972,12 +36974,12 @@
     result = true;
 
   XDELETEVEC (target1);
-  XDELETEVEC (target2); 
-  
+  XDELETEVEC (target2);
+
   return result;
 }
 
-static tree 
+static tree
 ix86_mangle_decl_assembler_name (tree decl, tree id)
 {
   /* For function version, add the target suffix to the assembler name.  */
@@ -37047,7 +37049,7 @@
   fn_type = TREE_TYPE (decl);
   func_type = build_function_type (TREE_TYPE (fn_type),
 				   TYPE_ARG_TYPES (fn_type));
-  
+
   func_decl = build_fn_decl (func_name, func_type);
   XDELETEVEC (func_name);
   TREE_USED (func_decl) = 1;
@@ -37060,7 +37062,7 @@
   /* This will be of type IFUNCs have to be externally visible.  */
   TREE_PUBLIC (func_decl) = 1;
 
-  return func_decl;  
+  return func_decl;
 }
 
 #endif
@@ -37097,7 +37099,7 @@
   tree dispatch_decl = NULL;
 
   struct cgraph_function_version_info *default_version_info = NULL;
- 
+
   gcc_assert (fn != NULL && DECL_FUNCTION_VERSIONED (fn));
 
   node = cgraph_node::get (fn);
@@ -37105,7 +37107,7 @@
 
   node_v = node->function_version ();
   gcc_assert (node_v != NULL);
- 
+
   if (node_v->dispatcher_resolver != NULL)
     return node_v->dispatcher_resolver;
 
@@ -37252,7 +37254,7 @@
 
   gcc_assert (dispatch_decl != NULL);
   /* Mark dispatch_decl as "ifunc" with resolver as resolver_name.  */
-  DECL_ATTRIBUTES (dispatch_decl) 
+  DECL_ATTRIBUTES (dispatch_decl)
     = make_attribute ("ifunc", resolver_name, DECL_ATTRIBUTES (dispatch_decl));
 
   /* Create the alias for dispatch to resolver here.  */
@@ -37267,7 +37269,7 @@
    provide the code to dispatch the right function at run-time.  NODE points
    to the dispatcher decl whose body will be created.  */
 
-static tree 
+static tree
 ix86_generate_version_dispatcher_body (void *node_p)
 {
   tree resolver_decl;
@@ -37444,7 +37446,7 @@
     M_INTEL_SILVERMONT,
     M_INTEL_KNL,
     M_AMD_BTVER1,
-    M_AMD_BTVER2,    
+    M_AMD_BTVER2,
     M_CPU_SUBTYPE_START,
     M_INTEL_COREI7_NEHALEM,
     M_INTEL_COREI7_WESTMERE,
@@ -40456,9 +40458,9 @@
       {
 	/* Make it call __cpu_indicator_init in libgcc. */
 	tree call_expr, fndecl, type;
-        type = build_function_type_list (integer_type_node, NULL_TREE); 
+        type = build_function_type_list (integer_type_node, NULL_TREE);
 	fndecl = build_fn_decl ("__cpu_indicator_init", type);
-	call_expr = build_call_expr (fndecl, 0); 
+	call_expr = build_call_expr (fndecl, 0);
 	return expand_expr (call_expr, target, mode, EXPAND_NORMAL);
       }
     case IX86_BUILTIN_CPU_IS:
@@ -40920,7 +40922,7 @@
       if (!REG_P (op2))
 	op2 = copy_to_mode_reg (SImode, op2);
 
-      emit_insn (fcode == IX86_BUILTIN_MONITOR 
+      emit_insn (fcode == IX86_BUILTIN_MONITOR
 		 ? ix86_gen_monitor (op0, op1, op2)
 		 : ix86_gen_monitorx (op0, op1, op2));
       return 0;
@@ -52455,7 +52457,7 @@
 
       /* Multiply lower parts and add all */
       t5 = gen_reg_rtx (V2DImode);
-      emit_insn (gen_vec_widen_umult_even_v4si (t5, 
+      emit_insn (gen_vec_widen_umult_even_v4si (t5,
 					gen_lowpart (V4SImode, op1),
 					gen_lowpart (V4SImode, op2)));
       op0 = expand_binop (mode, add_optab, t5, t4, op0, 1, OPTAB_DIRECT);
--- a/gcc/testsuite/gcc.target/i386/local.c
+++ b/gcc/testsuite/gcc.target/i386/local.c
@@ -1,5 +1,6 @@
 /* { dg-do compile } */
-/* { dg-options "-O2 -funit-at-a-time" } */
+/* { dg-options "-O2 -funit-at-a-time -fno-strict-calling-conventions" { target ia32 } } */
+/* { dg-options "-O2 -funit-at-a-time" { target lp64 } } */
 /* { dg-final { scan-assembler "magic\[^\\n\]*eax" { target ia32 } } } */
 /* { dg-final { scan-assembler "magic\[^\\n\]*(edi|ecx)" { target { ! ia32 } } } } */
 
--- /dev/null
+++ b/gcc/testsuite/gcc.target/i386/strict-cc.c
@@ -0,0 +1,24 @@
+/* { dg-do compile { target { ilp32 } } } */
+/* { dg-options "-O2 -funit-at-a-time -fstrict-calling-conventions"  } */
+/* { dg-final { scan-assembler "pushl.*\\\$1" } } */
+/* { dg-final { scan-assembler "pushl.*\\\$2" } } */
+/* { dg-final { scan-assembler "pushl.*\\\$3" } } */
+/* { dg-final { scan-assembler "pushl.*\\\$4" } } */
+/* { dg-final { scan-assembler "pushl.*\\\$5" } } */
+
+#include <stdio.h>
+
+/* Verify that local calling convention is not used if strict conventions.  */
+static int t(int, int, int, int, int) __attribute__ ((noinline));
+
+int
+m()
+{
+    t(1, 2, 3, 4, 5);
+}
+
+static int
+t(int a, int b, int c, int d, int e)
+{
+    printf("%d\n", a, b, c, d, e);
+}
