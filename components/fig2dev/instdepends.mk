INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += etoolbox
# gawk
INSTDEPENDS += ghostscript
INSTDEPENDS += latex-beamer
INSTDEPENDS += libpng-dev
INSTDEPENDS += libxpm-dev
INSTDEPENDS += netpbm
INSTDEPENDS += texlive-font-utils
INSTDEPENDS += texlive-fonts-recommended
INSTDEPENDS += texlive-lang-german
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += texlive-latex-recommended
INSTDEPENDS += texlive-pictures
# | pgf,
INSTDEPENDS += xutils-dev
