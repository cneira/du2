INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += pkg-config
INSTDEPENDS += xserver-xorg-dev
INSTDEPENDS += x11proto-xext-dev
INSTDEPENDS += x11proto-core-dev
INSTDEPENDS += x11proto-fonts-dev
INSTDEPENDS += x11proto-randr-dev
INSTDEPENDS += x11proto-render-dev
INSTDEPENDS += xutils-dev
INSTDEPENDS += quilt
INSTDEPENDS += libpciaccess-dev
