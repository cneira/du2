libowfat (0.30-2+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Tue, 08 Jan 2019 11:48:17 +0300

libowfat (0.30-2) unstable; urgency=medium

  * Fix /usr/include/entities.h file conflict with libgd-dev
    (Closes: #812418)

 -- Roland Stigge <stigge@antcom.de>  Mon, 25 Jan 2016 10:35:16 +0100

libowfat (0.30-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Standards-Version: 3.9.6

 -- Roland Stigge <stigge@antcom.de>  Sun, 17 Jan 2016 14:30:47 +0100

libowfat (0.29-4) unstable; urgency=medium

  * Fix FTBFS w/ clang, patch by Arthur Marble (Closes: #739564)
  * debian/control: Standards-Version: 3.9.5

 -- Roland Stigge <stigge@antcom.de>  Sat, 12 Apr 2014 10:36:40 +0200

libowfat (0.29-3) unstable; urgency=low

  * Added patch to set FD_CLOEXEC incorrectly with F_SETFL instead of F_SETFD,
    thanks to Guillem Jover (Closes: #696251)
  * debian/control:
    - Standards-Version: 3.9.4

 -- Roland Stigge <stigge@antcom.de>  Thu, 09 May 2013 15:53:33 +0200

libowfat (0.29-2) experimental; urgency=low

  * Add conflict dependencies to libowfat-dietlibc-dev (Closes: #694360)

 -- Roland Stigge <stigge@antcom.de>  Sun, 25 Nov 2012 21:45:11 +0100

libowfat (0.29-1) experimental; urgency=low

  * New upstream release
  * debian/control:
    - Standards-Version: 3.9.3
    - Build-Depends: debhelper (>= 9)

 -- Roland Stigge <stigge@antcom.de>  Sat, 03 Nov 2012 15:31:36 +0100

libowfat (0.28-5) unstable; urgency=low

  * Added debian/watch
  * debian/rules: New debhelper dh rules

 -- Roland Stigge <stigge@antcom.de>  Mon, 15 Aug 2011 19:46:53 +0200

libowfat (0.28-4) unstable; urgency=low

  * debian/control: Adding conflict markers to duplicate headers in other
    development packages
    - Conflicts: libcdb-dev - /usr/include/cdb.h (Closes: #633520)
    - Conflicts: libudt-dev - /usr/include/buffer.h (Closes: #633521)

 -- Roland Stigge <stigge@antcom.de>  Mon, 15 Aug 2011 19:46:43 +0200

libowfat (0.28-3) unstable; urgency=low

  [ Walter Franzini ]
  * Switched to Format: 3.0 (quilt)
  * Build two flavors of libowfat, one linked against glibc, the other
    against dietlibc. (Closes: #461150)
  * Fixed dependency

  [ Roland Stigge ]
  * New maintainer (Closes: #544059)
  * Added shared library version in libowfat0 (Closes: #607274)
  * debian/control: Standards-Version: 3.9.2

 -- Roland Stigge <stigge@antcom.de>  Sat, 09 Jul 2011 18:06:52 +0200

libowfat (0.28-2) unstable; urgency=medium

  * debian/control: Build-Depends: dietlibc-dev (>= 0.32-5) [sparc]
    (fixes build failure on sparc).

 -- Gerrit Pape <pape@smarden.org>  Mon, 26 Apr 2010 00:37:21 +0000

libowfat (0.28-1) unstable; urgency=low

  * new upstream version (closes: #550684).
  * debian/diff/0001-cvs-snapshot-20100320.diff: new; cvs snapshot
    20100320.
  * debian/rules: apply patches with -p1, not -p0.
  * debian/control: Standards-Version: 3.8.4.0.
  * debian/rules: do not ignore make clean error.

 -- Gerrit Pape <pape@smarden.org>  Sat, 20 Mar 2010 14:17:18 +0000

libowfat (0.27-1) unstable; urgency=low

  * new upstream version (closes: #435478).
  * debian/control: Conflicts: libdjbdns1-dev (closes: #447312).
  * debian/control: add Vcs-Git: http://smarden.org/git/libowfat.git/.
  * debian/implicit: update to a09db2e42c8b6a2d820754d741558e5894944746.

 -- Gerrit Pape <pape@smarden.org>  Sat, 12 Jan 2008 19:38:43 +0000

libowfat (0.24-1) unstable; urgency=low

  * new upstream version.
  * debian/control: remove version restrictions on [Build-]Depends:
    dietlibc-dev.

 -- Gerrit Pape <pape@smarden.org>  Sun, 25 Sep 2005 09:44:11 +0000

libowfat (0.23-1) unstable; urgency=low

  * new upstream version.

 -- Gerrit Pape <pape@smarden.org>  Wed, 25 May 2005 20:04:25 +0000

libowfat (0.22-1) unstable; urgency=low

  * new upstream version.

 -- Gerrit Pape <pape@smarden.org>  Sun, 27 Mar 2005 10:59:43 +0000

libowfat (0.21-1) unstable; urgency=low

  * new upstream version.
  * debian/control: update long description.
  * debian/copyright: license is GPLv2.
  * debian/rules: handle debian patches more gracefully; enable target
    patch-stamp; minor.

 -- Gerrit Pape <pape@smarden.org>  Thu,  3 Feb 2005 22:37:39 +0000

libowfat (0.20-1) unstable; urgency=low

  * new upstream version.
  * debian/control: Build-Depends: dietlibc-dev (>> 0.27-0); Depends:
    dietlibc-dev (>> 0.27-0).
  * debian/copyright: minor.
  * debian/implicit: update to revision 1.10.
  * debian/rules: minor cleanup.

 -- Gerrit Pape <pape@smarden.org>  Sun,  1 Aug 2004 12:25:45 +0000

libowfat (0.19.2-2) unstable; urgency=low

  * debian/rules: install library into /usr/lib/diet/lib/; minor.
  * debian/control: Build-Depends: dietlibc-dev (>= 0.26-3); Depends:
    dietlibc-dev (>= 0.26-3).

 -- Gerrit Pape <pape@smarden.org>  Sun, 20 Jun 2004 19:17:27 +0000

libowfat (0.19.2-1) unstable; urgency=low

  * new upstream version.
  * debian/implicit: update to revision 1.8.
  * debian/diff/epoll.diff: remove; obsolete.
  * debian/rules: disable target patch; remove workaround to suppress
    gcc-3.3 warnings (fixes build failure with gcc-2.95).
  * debian/control: Build-Depends: dietlibc-dev (>> 0.25-0).

 -- Gerrit Pape <pape@smarden.org>  Mon, 29 Mar 2004 09:01:00 +0000

libowfat (0.18-1) unstable; urgency=low

  * new upstream version.
  * debian/control: remove Origin: debian, Bugs: debbugs://bugs.debian.org
    (closes: #220090); move Section: libdevel to package libowfat-dev.
  * debian/libowfat-dev.docs: add TODO.
  * debian/diff/epoll.diff: new; define __USE_XOPEN for dietlibc-0.24.
  * debian/rules: create .diet/gcc on i386 to suppress gcc warnings in build
    logs.
  * debian/diff/man-pages.diff: remove; included upstream.

 -- Gerrit Pape <pape@smarden.org>  Sat, 28 Feb 2004 11:30:40 +0000

libowfat (0.16-4) unstable; urgency=medium

  * debian/rules: set MYARCH=parisc on hppa, MYARCH=mips on mipsel; add
    target patch: apply diffs from debian/diff/, reverse apply diffs in
    target clean.
  * debian/diff/man-pages.diff: new; man pages for scan_ulonglong(),
    scan_short(), fmt_longlong(), fmt_ulonglong(), fmt_xlonglong(),
    scan_xlonglong() (taken from upstream cvs, closes: #202582).

 -- Gerrit Pape <pape@smarden.org>  Mon,  9 Feb 2004 11:06:51 +0000

libowfat (0.16-3) unstable; urgency=low

  * debian/rules: use implicit Makefile rules; install static library into
    /usr/lib/diet/lib-$(ARCH)/; support 'nostrip' in DEB_BUILD_OPTIONS.
  * debian/implicit: new; implicit rules.
  * debian/libowfat-dev.docs: new.

 -- Gerrit Pape <pape@smarden.org>  Wed, 28 Jan 2004 09:42:53 +0000

libowfat (0.16-2) unstable; urgency=low

  * debian/control: Depends: dietlibc-dev (>> 0.24-0); Standards-Version:
    3.6.1.0.
  * debian/rules: install header files into /usr/include/diet/; install man
    pages into section 3diet to resolve conflict with libdjbdns1-dev and
    possibly other packages; minor.

 -- Gerrit Pape <pape@smarden.org>  Tue, 16 Dec 2003 11:28:42 +0000

libowfat (0.16-1) unstable; urgency=low

  * new maintainer; thanks Christian Kurz.
  * new upstream version.
  * debian/copyright: minor adaptations.
  * debian/control: new maintainer; Build-Depends: dietlibc-dev (>> 0.24-0).
  * debian/rules: minor adaptations.

 -- Gerrit Pape <pape@smarden.org>  Tue, 25 Nov 2003 12:28:18 +0000

libowfat (0.15-1) unstable; urgency=low

  * New Upstream release.
  * This release will fix the typo in the function buffer_GETC and
    therefor the bug report 192955 will be resolved.

 -- Christian Kurz <shorty@debian.org>  Thu, 19 Jun 2003 08:18:50 +0200

libowfat (0.14-2) unstable; urgency=low

  * The usage of #include <ctype.h> in scan_*.c won't cause a problem
    anymore since this release is not build with the dietlibc compiler.
    Therefor it will now have a build dependency on dietlicb-dev. This
    will adress the bug report #169940.

 -- Christian Kurz <shorty@debian.org>  Fri, 30 May 2003 10:46:44 +0200

libowfat (0.14-1) unstable; urgency=low

  * New Upstream release. 
  * Removed postinst script since policy doesn't enforce it anymore. Also
    the prerm was removed due to the old script cleaning up /usr/doc.

 -- Christian Kurz <shorty@debian.org>  Thu,  7 Nov 2002 10:17:02 +0100

libowfat (0.12-1) unstable; urgency=low

  * New Upstream release.

 -- Christian Kurz <shorty@debian.org>  Thu,  9 May 2002 10:05:38 +0200

libowfat (0.10-1) unstable; urgency=low

  * New Upstream Release.

 -- Christian Kurz <shorty@debian.org>  Tue,  2 Apr 2002 09:00:55 +0200

libowfat (0.9-1) unstable; urgency=low
  
  * First Debian release.

 -- Christian Kurz <shorty@debian.org>  Tue, 18 Dec 2001 23:10:57 +0100
