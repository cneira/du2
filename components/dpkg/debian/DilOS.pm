# Copyright © 2008 Ian Jackson <ijackson@chiark.greenend.org.uk>
# Copyright © 2008 Canonical, Ltd.
#   written by Colin Watson <cjwatson@ubuntu.com>
# Copyright © 2008 James Westby <jw+debian@jameswestby.net>
# Copyright © 2009 Raphaël Hertzog <hertzog@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package Dpkg::Vendor::DilOS;

use strict;
use warnings;

our $VERSION = '0.01';

use Dpkg::ErrorHandling;
use Dpkg::Gettext;
use Dpkg::Path qw(find_command);
use Dpkg::Control::Types;
use Dpkg::BuildOptions;
use Dpkg::Arch qw(debarch_eq get_host_arch);

use parent qw(Dpkg::Vendor::Default);

=encoding utf8

=head1 NAME

Dpkg::Vendor::DilOS - DilOS vendor object

=head1 DESCRIPTION

This vendor object customizes the behaviour of dpkg scripts for DilOS
specific behavior and policies.

=cut

sub run_hook {
    my ($self, $hook, @params) = @_;

    if ($hook eq 'keyrings') {
        return $self->run_hook('package-keyrings', @params);
    } elsif ($hook eq 'package-keyrings') {
        return ($self->SUPER::run_hook($hook),
                '/usr/share/keyrings/dilos-archive-keyring.gpg');
    } elsif ($hook eq 'archive-keyrings') {
        return ($self->SUPER::run_hook($hook),
                '/usr/share/keyrings/dilos-archive-keyring.gpg');
    } elsif ($hook eq 'archive-keyrings-historic') {
        return ($self->SUPER::run_hook($hook),
                '/usr/share/keyrings/dilos-archive-removed-keys.gpg');

    } elsif ($hook eq 'update-buildflags') {

	$self->_add_dilos_flags(@params);

    } elsif ($hook eq 'builtin-system-build-paths') {
        return qw(/build/);
    } else {
        return $self->SUPER::run_hook($hook, @params);
    }

}

sub _parse_feature_area {
    my ($self, $area, $use_feature) = @_;

    # Adjust features based on user or maintainer's desires.
    my $opts = Dpkg::BuildOptions->new(envvar => 'DEB_BUILD_OPTIONS');
    $opts->parse_features($area, $use_feature);
    $opts = Dpkg::BuildOptions->new(envvar => 'DEB_BUILD_MAINT_OPTIONS');
    $opts->parse_features($area, $use_feature);
}

sub _add_dilos_flags {
    my ($self, $flags) = @_;
    my $arch = get_host_arch();

#    $self->{flags} = {
#	LD_OPTIONS => '',
#	LD_OPTIONS_SO => '',
#    };
#    $self->{origin} = {
#	LD_OPTIONS => 'vendor',
#	LD_OPTIONS_SO => 'vendor',
#    };

    # Default feature states.
    my %use_feature = (
	enable_no_app_regs => 1,
	no_app_regs => 0,
	ld_options => 1,
	ld_options_so => 1,
	ld_def_libs => 1,
	ld_b_direct => 1,
	ld_z_redlocsym => 1,
	ld_z_rescan_now => 1,
	ld_z_text => 1,
	ld_z_defs => 1,
	ld_z_ignore => 1,
	ld_z_aslr => 0,
	ld_map_noexstk => 1,
	ld_map_noexbss => 1,
	ld_map_noexdata => 1,
	ld_map_pagealign => 1,
	ctf => 1,
	saveargs => 1,
    );

    # Adjust features based on user or maintainer's desires.
    $self->_parse_feature_area('dilos', \%use_feature);

    if ($use_feature{enable_no_app_regs}) {
	if ($arch eq 'solaris-sparc') {
	    $use_feature{no_app_regs} = 1;
	}
	if ($arch eq 'solaris-i386') {
	    $use_feature{no_app_regs} = 0;
	}
    }

    if ($use_feature{no_app_regs}) {
	my $flag = "-mno-app-regs";
	$flags->append('CFLAGS', $flag);
	$flags->append('OBJCFLAGS',  $flag);
	$flags->append('OBJCXXFLAGS', $flag);
	$flags->append('FFLAGS', $flag);
	$flags->append('FCFLAGS', $flag);
	$flags->append('CXXFLAGS', $flag);
	$flags->append('GCJFLAGS', $flag);
    }

    if ($use_feature{ctf}) {
	my $flag = "-gdwarf-2 -fno-omit-frame-pointer";
	$flags->append('CFLAGS', $flag);
	$flags->append('OBJCFLAGS',  $flag);
	$flags->append('OBJCXXFLAGS', $flag);
	$flags->append('FFLAGS', $flag);
	$flags->append('FCFLAGS', $flag);
	$flags->append('CXXFLAGS', $flag);
	$flags->append('GCJFLAGS', $flag);
    }

    if ($arch eq 'solaris-sparc') {
	$use_feature{saveargs} = 0;
    }

    if ($use_feature{saveargs}) {
	my $flag = "-msave-args";
	$flags->append('CFLAGS', $flag);
	$flags->append('OBJCFLAGS',  $flag);
	$flags->append('OBJCXXFLAGS', $flag);
	$flags->append('FFLAGS', $flag);
	$flags->append('FCFLAGS', $flag);
	$flags->append('CXXFLAGS', $flag);
	$flags->append('GCJFLAGS', $flag);
    }

    my $LD_OPTIONS = '';
    my $LD_OPTIONS_SO = '';
    my $LD_DEF_LIBS = '';
    my $LD_B_DIRECT = '';
    my $LD_Z_REDLOCSYM = '';
    my $LD_Z_RESCAN_NOW = '';
    my $LD_Z_TEXT = '';
    my $LD_Z_DEFS = '';
    my $LD_Z_IGNORE = '';
    my $LD_Z_ASLR = '';
    my $LD_MAP_NOEXSTK = '';
    my $LD_MAP_NOEXBSS = '';
    my $LD_MAP_NOEXDATA = '';
    my $LD_MAP_PAGEALIGN = '';
    if ($use_feature{ld_def_libs}) {
	# LD_DEF_LIBS = -lc
	$LD_DEF_LIBS = '-lc';
    }
    if ($use_feature{ld_b_direct}) {
	# LD_B_DIRECT = -Bdirect
	$LD_B_DIRECT = '-Bdirect';
    }
    if ($use_feature{ld_z_redlocsym}) {
	# LD_Z_REDLOCSYM = -z redlocsym
	$LD_Z_REDLOCSYM = '-z redlocsym';
    }
    if ($use_feature{ld_z_rescan_now}) {
	# LD_Z_RESCAN_NOW = -z rescan-now
	$LD_Z_RESCAN_NOW = '-z rescan-now';
    }
    if ($use_feature{ld_z_text}) {
	# LD_Z_TEXT = -z text
	$LD_Z_TEXT = '-z text';
    }
    if ($use_feature{ld_z_defs}) {
	# LD_Z_DEFS = -z defs
	$LD_Z_DEFS = '-z defs';
    }
    if ($use_feature{ld_z_ignore}) {
	# LD_Z_IGNORE = -z ignore
	$LD_Z_IGNORE = '-z ignore';
    }
    if ($use_feature{ld_z_aslr}) {
	# ASLR_ENABLE = -z aslr=enable
	# ASLR_DISABLE = -z aslr=disable
	# ASLR_MODE = $(ASLR_DISABLE)
	# LD_Z_ASLR = $(ASLR_MODE)
	$LD_Z_ASLR = '-z aslr=enable';
    } else {
	$LD_Z_ASLR = '-z aslr=disable';
    }
    if ($use_feature{ld_map_noexstk}) {
	# LD_MAP_NOEXSTK.i386 = -M /usr/lib/ld/map.noexstk
	# LD_MAP_NOEXSTK.sparc = -M /usr/lib/ld/map.noexstk
	$LD_MAP_NOEXSTK = '-M /usr/lib/ld/map.noexstk';
    }
    if ($use_feature{ld_map_noexbss}) {
	# LD_MAP_NOEXBSS = -M /usr/lib/ld/map.noexbss
	$LD_MAP_NOEXBSS = '-M /usr/lib/ld/map.noexbss';
    }
    if ($use_feature{ld_map_noexdata}) {
	# LD_MAP_NOEXDATA.i386 = -M /usr/lib/ld/map.noexdata
	# LD_MAP_NOEXDATA.sparc = $(LD_MAP_NOEXBSS)
	if ($arch eq 'solaris-i386') {
	    $LD_MAP_NOEXDATA = '-M /usr/lib/ld/map.noexdata';
	} else {
	    $LD_MAP_NOEXDATA = $LD_MAP_NOEXBSS;
	}
    }
    if ($use_feature{ld_map_pagealign}) {
	# LD_MAP_PAGEALIGN = -M /usr/lib/ld/map.pagealign
	$LD_MAP_PAGEALIGN = '-M /usr/lib/ld/map.pagealign';
    }

    if ($use_feature{ld_options}) {
	# LD_OPTIONS += $(LD_MAP_NOEXSTK.$(MACH)) $(LD_MAP_NOEXDATA.$(MACH)) \
	#		$(LD_MAP_PAGEALIGN) $(LD_B_DIRECT) $(LD_Z_IGNORE)
	$LD_OPTIONS .= " $LD_MAP_NOEXSTK $LD_MAP_NOEXDATA";
	$LD_OPTIONS .= " $LD_MAP_PAGEALIGN $LD_B_DIRECT $LD_Z_IGNORE";
	$flags->set('LD_OPTIONS', $LD_OPTIONS, 'vendor');
	Dpkg::Build::Env::set("LD_OPTIONS", $LD_OPTIONS);
    }

    if ($use_feature{ld_options_so}) {
	# LD_OPTIONS_SO += $(LD_Z_TEXT) $(LD_Z_DEFS) $(LD_DEF_LIBS)
	$LD_OPTIONS_SO .= " $LD_Z_TEXT $LD_Z_DEFS $LD_DEF_LIBS";
	$flags->set('LD_OPTIONS_SO', $LD_OPTIONS_SO, 'vendor');
	Dpkg::Build::Env::set("LD_OPTIONS_SO", $LD_OPTIONS_SO);
    }

    # Store the feature usage.
    while (my ($feature, $enabled) = each %use_feature) {
       $flags->set_feature('dilos', $feature, $enabled);
    }
}

=back

=head1 CHANGES

=head2 Version 0.xx

This is a private module.

=cut

1;
