Source: mrmpi
Section: devel
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <malat@debian.org>, Dominique Belhachemi <domibel@debian.org>
Build-Depends: debhelper (>= 9), cmake (>= 2.6.3), mpi-default-dev
Build-Depends-Indep: htmldoc, python
Standards-Version: 3.9.5
Homepage: http://mapreduce.sandia.gov/
Vcs-Browser: http://anonscm.debian.org/viewvc/debian-science/packages/mrmpi/trunk/
Vcs-Svn: svn://anonscm.debian.org/debian-science/packages/mrmpi/trunk/

Package: libmrmpi-dev
Section: libdevel
Architecture: any
Depends: libmrmpi1 (= ${binary:Version}), ${misc:Depends}
Description: Implements MapReduce operation on top of standard MPI message - development
 The MapReduce-MPI (MR-MPI) library is open-source software that implements the
 MapReduce operation popularized by Google on top of standard MPI message
 passing.
 .
 The MR-MPI library is written in C++ and is callable from hi-level langauges
 such as C++, C, Fortran. A Python wrapper is also included, so MapReduce
 programs can be written in Python, including map() and reduce() user callback
 methods. A hi-level scripting interface to the MR-MPI library, called OINK, is
 also included which can be used to develop and chain MapReduce algorithms
 together in scripts with commands that simplify data management tasks. OINK has
 its own manual and doc pages.
 .
 This package contains development files needed to build MapReduce-MPI
 applications.

Package: libmrmpi1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Implements MapReduce operation on top of standard MPI message - runtime
 The MapReduce-MPI (MR-MPI) library is open-source software that implements the
 MapReduce operation popularized by Google on top of standard MPI message
 passing.
 .
 The MR-MPI library is written in C++ and is callable from hi-level langauges
 such as C++, C, Fortran. A Python wrapper is also included, so MapReduce
 programs can be written in Python, including map() and reduce() user callback
 methods. A hi-level scripting interface to the MR-MPI library, called OINK, is
 also included which can be used to develop and chain MapReduce algorithms
 together in scripts with commands that simplify data management tasks. OINK has
 its own manual and doc pages.
 .
 This package contains the libraries needed to run MapReduce-MPI applications.

Package: mrmpi-doc
Architecture: all
Depends: ${misc:Depends}, doc-base
Section: doc
Description: Implements MapReduce operation on top of standard MPI message - doc
 The MapReduce-MPI (MR-MPI) library is open-source software that implements the
 MapReduce operation popularized by Google on top of standard MPI message
 passing.
 .
 The MR-MPI library is written in C++ and is callable from hi-level langauges
 such as C++, C, Fortran. A Python wrapper is also included, so MapReduce
 programs can be written in Python, including map() and reduce() user callback
 methods. A hi-level scripting interface to the MR-MPI library, called OINK, is
 also included which can be used to develop and chain MapReduce algorithms
 together in scripts with commands that simplify data management tasks. OINK has
 its own manual and doc pages.
 .
 This package contains the documentation for MapReduce-MPI libraries.
