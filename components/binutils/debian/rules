#!/usr/bin/make -f

distribution := $(shell lsb_release -is)

DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
DEB_HOST_ARCH      ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
export DEB_HOST_GNU_TYPE

export DEB_BUILD_MAINT_OPTIONS=dilos=-saveargs,-ctf
#include /usr/share/dpkg/buildflags.mk

CC     = gcc
CXX    = g++
CFLAGS = -g -O2
CXXFLAGS = -g -O2
LDFLAGS =

#	--enable-shared \
#	--disable-compressed-debug-sections \

CONFARGS = \
	--enable-plugins \
	--enable-threads \
	--with-system-zlib \
	--prefix=/usr \
	--enable-deterministic-archives \
	--with-pkgversion="GNU Binutils for $(distribution)"

#	--build=$(DEB_BUILD_GNU_TYPE) \
#	--host=$(DEB_HOST_GNU_TYPE) \

# not yet ready for GCC 4.9
CONFARGS += --disable-werror

CONFARGS += --with-sysroot=/

# from solaris
CONFARGS += \
--disable-libtool-lock \
--enable-64-bit-bfd \
--enable-host-shared \
--enable-largefile \
--enable-nls

SAVE_ARGS=
ifeq ($(DEB_HOST_ARCH),solaris-i386)
CONFIGURE_EXTRA_FLAGS.32 += --build=i386-pc-solaris2.11
CONFIGURE_EXTRA_FLAGS.32 += --host=i386-pc-solaris2.11
CONFIGURE_EXTRA_FLAGS.64 += --enable-targets=i386-pc-solaris2.11,sparc-sun-solaris2.11,sparcv9-sun-solaris2.11
CONFIGURE_EXTRA_FLAGS.32 += --enable-targets=x86_64-pc-solaris2.11
#SAVE_ARGS = -msave-args
endif
ifeq ($(DEB_HOST_ARCH),solaris-sparc)
CONFIGURE_EXTRA_FLAGS.32 += --build=sparc-sun-solaris2.11
CONFIGURE_EXTRA_FLAGS.32 += --host=sparc-sun-solaris2.11
#CONFARGS += --disable-gold
CONFIGURE_EXTRA_FLAGS.64 += --enable-targets=sparc-sun-solaris2.11
CONFIGURE_EXTRA_FLAGS.32 += --enable-targets=sparcv9-sun-solaris2.11
endif

%:
	dh $@ --parallel

override_dh_auto_configure:
	env CC="$(CC)" CXX="$(CXX)" \
	CFLAGS="$(CFLAGS) $(SAVE_ARGS)" CXXFLAGS="$(CXXFLAGS) $(SAVE_ARGS)" LDFLAGS="$(LDFLAGS)" \
	dh_auto_configure --builddirectory=build64/ -- \
	$(CONFARGS) $(CONFIGURE_EXTRA_FLAGS.64)
	$(MAKE) -C build64 configure-host
	env CC="$(CC)" CXX="$(CXX)" CFLAGS="$(CFLAGS) -m32" CXXFLAGS="$(CXXFLAGS) -32" LDFLAGS="$(LDFLAGS)" \
	dh_auto_configure --builddirectory=build32/ -- \
	$(CONFARGS) $(CONFIGURE_EXTRA_FLAGS.32)
	$(MAKE) -C build32 configure-host

override_dh_auto_build:
	# 64bit
	$(MAKE) -C build64/bfd headers
	dh_auto_build --builddirectory=build64/
	# 32bit
	$(MAKE) -C build32/bfd headers
	dh_auto_build --builddirectory=build32/

override_dh_auto_install:
	$(MAKE) -C build64/ install DESTDIR=$(CURDIR)/debian/tmp
#	$(MAKE) -C build32/ install DESTDIR=$(CURDIR)/debian/tmp
	rm -f debian/tmp/usr/lib/*/charset.alias
	rm -f debian/tmp/usr/lib/*/*.la
	rm -f debian/tmp/usr/share/info/dir
	mv debian/tmp/usr/bin/ld debian/tmp/usr/bin/gld
	mv debian/tmp/usr/bin/ld.bfd debian/tmp/usr/bin/gld.bfd
	mv debian/tmp/usr/share/info/ld.info debian/tmp/usr/share/info/gld.info
	mv debian/tmp/usr/share/man/man1/ld.1 debian/tmp/usr/share/man/man1/gld.1
	mv debian/tmp/usr/bin/as debian/tmp/usr/bin/gas
	mv debian/tmp/usr/share/info/as.info debian/tmp/usr/share/info/gas.info
	mv debian/tmp/usr/share/man/man1/as.1 debian/tmp/usr/share/man/man1/gas.1
	mv debian/tmp/usr/bin/elfedit debian/tmp/usr/bin/gelfedit
	mv debian/tmp/usr/share/man/man1/elfedit.1 debian/tmp/usr/share/man/man1/gelfedit.1
	mv debian/tmp/usr/bin/strip debian/tmp/usr/bin/gstrip
	mv debian/tmp/usr/share/man/man1/strip.1 debian/tmp/usr/share/man/man1/gstrip.1
	if [ -d build32 ]; then \
		cp -f build32/gas/as-new debian/tmp/usr/bin/gas.32 && chmod +x debian/tmp/usr/bin/gas.32; \
	fi
	find debian/tmp/usr -name "opcodes.mo" | xargs rm -f
	find debian/tmp/usr -name "bfd.mo" | xargs rm -f
	dh_auto_install

override_dh_auto_clean:
	dh_auto_clean
	rm -rf build32 build64

override_dh_strip:
	:
