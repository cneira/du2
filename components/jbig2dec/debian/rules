#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2010-2013, 2015-2017 Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for jbig2dec
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
#CFLAGS += -g -gdwarf-2 -fno-omit-frame-pointer
#ifeq ($(DEB_HOST_ARCH),solaris-i386)
#CFLAGS += -msave-args
#endif

DEB_AUTO_UPDATE_LIBTOOL = pre
DEB_AUTO_UPDATE_ACLOCAL = ,
DEB_AUTO_UPDATE_AUTOCONF = ,
DEB_AUTO_UPDATE_AUTOHEADER = ,
DEB_AUTO_UPDATE_AUTOMAKE = ,
include /usr/share/cdbs/1/rules/utils.mk
include /usr/share/cdbs/1/class/autotools.mk
include /usr/share/cdbs/1/rules/debhelper.mk

stem = $(DEB_SOURCE_PACKAGE)
lib = lib$(stem)
major = 0
pkg-lib = lib$(stem)$(major)
pkg-dev = lib$(stem)$(major)-dev

# Needed by upstream build routines
CDBS_BUILD_DEPENDS +=, libpng-dev

# Needed by upstream tests
CDBS_BUILD_DEPENDS +=, python

# Multiarch quirk (see also other uses of that variable in this file)
DEB_CONFIGURE_EXTRA_FLAGS += --libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH)

# bootstrap autotools files (CDBS normally only updates them)
DEB_MAKE_CLEAN_TARGET = distclean
DEB_AUTOMAKE_ARGS = --add-missing --copy
clean::
	rm -f Makefile.in aclocal.m4 compile config.guess config.h.in config.sub configure depcomp install-sh ltmain.sh missing test-driver

DEB_MAKE_CHECK_TARGET = check

DEB_INSTALL_MANPAGES_jbig2dec = debian/tmp/usr/share/man/man1/*

DEB_SHLIBDEPS_LIBRARY_$(pkg-lib) = $(lib)
DEB_DH_MAKESHLIBS_ARGS = -Pdebian/$(cdbs_curpkg)

# put aside upstream-shipped temp files during build but after copyright-check
DEB_UPSTREAM_CRUFT_MOVE = config_types.h

# Let d-shlibs resolve dependencies for and install library files
CDBS_BUILD_DEPENDS +=, d-shlibs
binary-post-install/$(pkg-lib) binary-post-install/$(pkg-dev):: \
 debian/stamp-local-shlibs-$(lib)
debian/stamp-local-shlibs-$(lib): \
 binary-install/$(pkg-lib) \
 binary-install/$(pkg-dev)
	d-shlibmove --commit \
		--override s/libm2-dev// \
		--exclude-la \
		--multiarch \
		--movedev "debian/tmp/usr/include/*" usr/include/ \
		debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/$(lib).so
	touch $@
clean::
	rm -f debian/stamp-local-shlibs-$(lib)
