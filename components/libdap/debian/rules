#!/usr/bin/make -f

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
ARCH:=$(shell dpkg --print-architecture)
LIBDIR:=/usr/lib/$(DEB_HOST_MULTIARCH)


export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
CFLAGS += -Wall -pedantic
#LDFLAGS += -Wl,--as-needed

CXXFLAGS:= $(shell dpkg-buildflags --get CXXFLAGS) -std=c++14 -D__dilos__
export CXXFLAGS

# Disable tests for the moment; some input files missing
export DO_TEST=true

# Always do verbose.
export DH_VERBOSE=1

# The magic debhelper rule:
%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- --with-gnu-ld --enable-runtime-endian-check=yes

override_dh_auto_test:
	$(DO_TEST) && dh_auto_test || echo "Ignoring test failures"

override_dh_auto_build:
	dh_auto_build
	$(MAKE) docs

override_dh_auto_install:
	dh_auto_install
	# dh_links needed for multi-arch
	dh_link -p libdap25       $(LIBDIR)/libdap.so.25.0.1       $(LIBDIR)/libdap.so.25
	dh_link -p libdapclient6v5  $(LIBDIR)/libdapclient.so.6.1.7  $(LIBDIR)/libdapclient.so.6
	dh_link -p libdapserver7v5  $(LIBDIR)/libdapserver.so.7.6.7  $(LIBDIR)/libdapserver.so.7
	dh_link -p libdap-dev     $(LIBDIR)/libdap.so.25.0.1       $(LIBDIR)/libdap.so
	dh_link -p libdap-dev     $(LIBDIR)/libdapclient.so.6.1.7  $(LIBDIR)/libdapclient.so
	dh_link -p libdap-dev     $(LIBDIR)/libdapserver.so.7.6.7  $(LIBDIR)/libdapserver.so
	# Allow this cp to fail on arch-independent builds
	-cp debian/dap-config.pkg  debian/libdap-dev/usr/bin/dap-config

override_dh_installdocs:
	dh_installdocs
	[ -f debian/libdap-doc/usr/share/doc/libdap-doc/html/jquery.js ] && ( \
		rm debian/libdap-doc/usr/share/doc/libdap-doc/html/jquery.js ; \
		ln -sf /usr/share/javascript/jquery/jquery.js debian/libdap-doc/usr/share/doc/libdap-doc/html/jquery.js ) \
		|| echo "Skipped; no jquery"

clean:
	dh clean
	rm -rf html doxygen_warnings.txt
