INSTDEPENDS += debhelper
INSTDEPENDS += mpi-default-dev
INSTDEPENDS += libmpich-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libjpeg-dev
# gfortran
INSTDEPENDS += sharutils
# chrpath
INSTDEPENDS += autotools-dev
INSTDEPENDS += automake
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libaec-dev
# default-jdk-headless (>= 2:1.7) [!hppa !hurd-i386],
INSTDEPENDS += javahelper
# [!hppa !hurd-i386],
#Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += php-cli
