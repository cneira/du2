INSTDEPENDS += debhelper
INSTDEPENDS += libqt5opengl5-dev
INSTDEPENDS += libqt5sql5-sqlite
INSTDEPENDS += libqt5webkit5-dev
INSTDEPENDS += pkg-kde-tools
INSTDEPENDS += qtbase5-private-dev
INSTDEPENDS += qtdeclarative5-private-dev
INSTDEPENDS += zlib1g-dev
#Build-Depends-Indep:
INSTDEPENDS += qtbase5-doc-html
