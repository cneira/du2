pystemmer (1.3.0+dfsg-1+dilos1) unstable; urgency=low

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Mon, 04 Mar 2019 16:54:02 +0300

pystemmer (1.3.0+dfsg-1) unstable; urgency=low

  [ Stefano Rivera ]
  * New upstream release.
  * Refresh patches.
  * Drop test-exit-code.diff, superseded upstream.
  * Use CFLAGS, CPPFLAGS, and LDFLAGS from dpkg-buildflags, to pick up
    hardening flags.
  * Bump Standards-Version to 3.9.4.
    - Bump debhelper B-D to >= 8.1, for build-{arch,indep} support.
  * Bump debhelper compat level to 8.
  * Bump machine readable copyright format to 1.0.
  * Re-licence packaging under Expat.
  * Switch debian/watch URL to https.
  * Rename get-orig-source target to get-packaged-orig-source, it doesn't meet
    the policy requirements for get-orig-source.
  * Add autopkgtests.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 26 May 2013 17:48:42 +0200

pystemmer (1.2.0+dfsg-1) unstable; urgency=low

  * New upstream release.
    - Uses Cython instead of Pyrex
    - Python3 support.
  * Build python3 packages.
  * Split documentation into python-stemmer-doc.
  * Build-Depend on python-docutils for RST support in epydoc.
  * Use py_builddir_sh macro from python.mk.
    - Bump python B-D to >= 2.6.6-14~.
  * Install .egg-info

 -- Stefano Rivera <stefanor@debian.org>  Sat, 10 Sep 2011 22:02:24 +0200

pystemmer (1.1.0+dfsg-2) unstable; urgency=low

  * Forwarded test-exit-code.diff
  * Switch to dh_python2.
    - Use X-Python-Version. BD on python >= 2.6.5-13.
  * Update my e-mail address.
  * Bump Standards-Version to 3.9.2, no changes needed.
  * Update copyright format.
  * Wrap and sort *Depends.
  * Correct DEP3 headers (first line of Description is the subject).

 -- Stefano Rivera <stefanor@debian.org>  Sun, 17 Apr 2011 23:00:55 +0200

pystemmer (1.1.0+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #375441)

 -- Stefano Rivera <stefano@rivera.za.net>  Sun, 14 Mar 2010 16:31:11 +0200
