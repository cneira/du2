Source: libgnomecanvas
Section: devel
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 10),
               dpkg-dev (>= 1.17.14),
               docbook-xml,
               gettext,
               gnome-common,
               gnome-pkg-tools (>= 0.7),
               gtk-doc-tools,
               intltool,
               libart-2.0-dev (>= 2.3.16),
               libgail-dev (>= 1.9.0),
               libglade2-dev (>= 1:2.6.4-2~),
               libgtk2.0-dev (>= 2.8.17),
               xauth <!nocheck>,
               xvfb <!nocheck>
Build-Depends-Indep: libglib2.0-doc,
                     libgtk2.0-doc
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/attic/libgnomecanvas
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/attic/libgnomecanvas

Package: libgnomecanvas2-0
Architecture: any
Multi-Arch: same
Section: oldlibs
Pre-Depends: ${misc:Pre-Depends}
Depends: libgnomecanvas2-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: powerful object-oriented display engine - runtime files
 The canvas widget is a powerful and extensible object-oriented display
 engine. A GnomeCanvasItem is a GtkObject representing some element of the
 display, such as an image, a rectangle, an ellipse, or some text. You can
 refer to this architecture as structured graphics; the canvas lets you deal
 with graphics in terms of items, rather than an undifferentiated grid of
 pixels.

Package: libgnomecanvas2-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libart-2.0-dev (>= 2.3.16),
         libgail-dev (>= 1.9.0),
         libgnomecanvas2-0 (= ${binary:Version}),
         libgtk2.0-dev (>= 2.8.17),
         ${misc:Depends}
Suggests: libgnomecanvas2-doc
Description: powerful object-oriented display engine - development files
 The canvas widget is a powerful and extensible object-oriented display
 engine. A GnomeCanvasItem is a GtkObject representing some element of the
 display, such as an image, a rectangle, an ellipse, or some text. You can
 refer to this architecture as structured graphics; the canvas lets you deal
 with graphics in terms of items, rather than an undifferentiated grid of
 pixels.

Package: libgnomecanvas2-common
Architecture: all
Multi-Arch: foreign
Section: oldlibs
Depends: ${misc:Depends}
Description: powerful object-oriented display engine - common files
 The canvas widget is a powerful and extensible object-oriented display
 engine. A GnomeCanvasItem is a GtkObject representing some element of the
 display, such as an image, a rectangle, an ellipse, or some text. You can
 refer to this architecture as structured graphics; the canvas lets you deal
 with graphics in terms of items, rather than an undifferentiated grid of
 pixels.
 .
 This package contains internationalization files.

Package: libgnomecanvas2-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: powerful object-oriented display engine - documentation files
 The canvas widget is a powerful and extensible object-oriented display
 engine. A GnomeCanvasItem is a GtkObject representing some element of the
 display, such as an image, a rectangle, an ellipse, or some text. You can
 refer to this architecture as structured graphics; the canvas lets you deal
 with graphics in terms of items, rather than an undifferentiated grid of
 pixels.
 .
 This package contains documentation files.
