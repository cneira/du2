#!/usr/bin/make -f
# Sample debian/rules that uses debhelper. GNU copyright 1997 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

libpkg = libpisock9
synclibpkg = libpisync1
devpkg = libpisock-dev
runpkg = pilot-link
pythonpkg = python-pisock

CFLAGS = -Wall -g -pthread
ifneq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif

ifeq (,$(filter nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	# parallel compilation is NOT supported and will fail
	MAKEFLAGS += -j$(NUMJOBS)
endif

#ifeq (hppa,$(DEB_BUILD_ARCH))
#	CFLAGS += -ffunction-sections
#endif

PYVERS=$(shell pyversions -r)

build: build-arch build-indep
build-arch: build-stamp build-python-stamp
build-indep: build-stamp
build-stamp:
	dh_testdir
	# Add here commands to compile the package.
	dh_autoreconf
	./configure --prefix=/usr \
		--enable-conduits --enable-libusb \
		--with-python --with-libpng \
		--with-libiconv \
		CFLAGS="$(CFLAGS)"

	# force installation of dump.pl & Pilot.pm (#413502)
	touch bindings/Perl/dump.pl

	$(MAKE)
	touch build-stamp

build-python-stamp: build-stamp
	dh_testdir
	set -e; \
	for python in $(PYVERS); do \
		$(MAKE) -C bindings python-build PYTHON=$$python; \
	done
	touch build-python-stamp

clean:
	dh_testdir
	rm -f build-stamp pre-binary-stamp
	if [ -e Makefile ] ; then $(MAKE) distclean; fi
	rm -f config.log changelog libtool
	rm -rf bindings/Python/build
	dh_clean

pre-binary: build pre-binary-stamp
pre-binary-stamp:
	dh_clean
	dh_installdirs
	$(MAKE) install DESTDIR=`pwd`/debian/tmp
	$(MAKE) -C doc/man install DESTDIR=`pwd`/debian/tmp
	set -e; \
	cd bindings/Python; \
	for python in $(PYVERS); do \
		$$python setup.py install --root=$(CURDIR)/debian/tmp --install-layout=deb; \
	done

	# mini patches
	cp ChangeLog changelog

	# install the udev rule file to get the correct file group and permissions 
	mv debian/tmp/usr/share/pilot-link/udev/60-libpisock.rules  debian/libpisock9.udev

	# remove .py files to avoid conflicts with python-pisock package
#	rm debian/python-pisock-dbg/usr/lib/python*/*-packages/*.py
	# remove egg file too (LP: #202773)
#	rm debian/python-pisock-dbg/usr/lib/python*/*-packages/python_libpisock-*.egg-info

	dh_install --list-missing --sourcedir=debian/tmp

	sed -i "/dependency_libs/ s/'.*'/''/" `find debian/ -name '*.la'`
	touch pre-binary-stamp

# Build architecture-independent files here.
binary-indep: build
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build $(libpkg) $(devpkg) $(runpkg) $(synclibpkg) $(pythonpkg)

$(libpkg): build pre-binary
	echo $@
	dh_testroot
	dh_installdirs -p $@
	dh_installdocs -p $@
	dh_installudev -p $@
	dh_installmodules -p $@
	dh_installchangelogs -p $@
	dh_strip -p $@ --dbg-package=$(pythonpkg)-dbg
	dh_strip -p $@
	dh_compress -p $@
	dh_fixperms -p $@
	dh_makeshlibs -p $@
	dh_installmodules -p $@
	dh_installdeb -p $@
	dh_shlibdeps -p $@
	dh_gencontrol -p $@
	dh_md5sums -p $@
	dh_builddeb -p $@

$(synclibpkg): build pre-binary $(libpkg)
	echo $@
	dh_installdirs -p $@
	dh_installdocs -p $@
	dh_installchangelogs -p $@
	dh_strip -p $@
	dh_compress -p $@
	dh_fixperms -p $@
	dh_makeshlibs -p $@
	dh_installdeb -p $@
	dh_shlibdeps -p $@
	dh_gencontrol -p $@
	dh_md5sums -p $@
	dh_builddeb -p $@
	
$(devpkg): build pre-binary $(libpkg)
	echo $@
	dh_installdirs -p $@
	dh_installdocs -p $@
	dh_installchangelogs -p $@
	dh_strip -p $@
	dh_compress -p $@
	dh_fixperms -p $@
	dh_makeshlibs -p $@
	dh_installdeb -p $@
	dh_shlibdeps -p $@
	dh_gencontrol -p $@
	dh_md5sums -p $@
	dh_builddeb -p $@

$(runpkg): build pre-binary $(libpkg)
	echo $@
	dh_installdirs -p $@
	dh_installdocs -p $@
	dh_installexamples -p $@
	dh_installmenu -p $@
	dh_installman -p $@
	dh_installchangelogs -p $@
	dh_installdebconf -p $@
	dh_strip -p $@
	dh_compress -p $@
	dh_fixperms -p $@
	dh_installdeb -p $@
	dh_shlibdeps -p $@
	dh_gencontrol -p $@
	dh_md5sums -p $@
	dh_builddeb -p $@

$(pythonpkg): build pre-binary $(libpkg)
	echo $@
	dh_installdirs -p $@
	dh_installchangelogs -p $@
	dh_installdocs -p $@
	dh_strip -p $@
	dh_compress -p $@
	dh_fixperms -p $@
	dh_makeshlibs -p $@
	dh_python2 -p $@
	dh_installdeb -p $@
	dh_shlibdeps -p $@
	dh_gencontrol -p $@
	dh_md5sums -p $@
	dh_builddeb -p $@

$(pythonpkg)-dbg: build pre-binary $(pythonpkg)
	echo $@
	dh_installdocs -p $@
	rm -rf debian/$@/usr/share/doc/$@
	ln -sf $(pythonpkg) debian/$@/usr/share/doc/$@
	dh_compress -p $@
	dh_fixperms -p $@
	dh_makeshlibs -p $@
	dh_python2 -p $@
	dh_installdeb -p $@
	dh_shlibdeps -p $@
	dh_gencontrol -p $@
	dh_md5sums -p $@
	dh_builddeb -p $@

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary \
	$(libpkg) $(devpkg) $(runpkg) $(pythonpkg) pre-binary

