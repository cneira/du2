Author: Val Lorentz <progval@progval.net>
Description: Hide memory references in the output.
 A lot of Debian packages rely on Epydoc during their build process.  By
 default, Eypdoc-generated documentation is "unreproducible", meaning that the
 content of the generated files changes from build to build even if the source
 tree does not. 
 .
 Epydoc uses repr() on objects that don't always override the default one. As a
 consequence, the documentation shows memory references on the build system,
 e.g. <module.Foo instance at 0x7f3aff4299e0>.  This patch detects objects with
 no overriding of __repr__ and provides a fallback that does not show the
 reference.  It also takes care of not calling default __repr__ on functions. 
 .
 This patch was contributed by the Debian Reproducible Builds effort [1].
 [1]: https://wiki.debian.org/ReproducibleBuilds
Bug: https://sourceforge.net/p/epydoc/bugs/369/
Bug-Debian: http://bugs.debian.org/795826
Forwarded: https://sourceforge.net/p/epydoc/bugs/369/
Last-Update: 2015-08-17
diff -u -r epydoc-3.0.1+dfsg.old/epydoc/markup/pyval_repr.py epydoc-3.0.1+dfsg/epydoc/markup/pyval_repr.py
--- epydoc-3.0.1+dfsg.old/epydoc/markup/pyval_repr.py	2015-08-16 18:57:38.152484836 +0000
+++ epydoc-3.0.1+dfsg/epydoc/markup/pyval_repr.py	2015-08-17 09:31:08.384223756 +0000
@@ -207,9 +207,18 @@
             self._colorize_re(pyval, state)
         else:
             try:
-                pyval_repr = repr(pyval)
-                if not isinstance(pyval_repr, (str, unicode)):
-                    pyval_repr = unicode(pyval_repr)
+                pyval_class = pyval.__class__
+                if hasattr(pyval, '__repr__') and \
+                        pyval_class.__repr__ is not object.__repr__ and \
+                        pyval_class.__repr__ is not types.FunctionType.__repr__:
+                    pyval_repr = repr(pyval)
+                    if not isinstance(pyval_repr, (str, unicode)):
+                        pyval_repr = unicode(pyval_repr)
+                else:
+                    # pyval has a default repr(), which would leak
+                    # a reference to the object
+                    pyval_repr = '<%s.%s object>' % (
+                            pyval_class.__module__, pyval_class.__name__)
                 pyval_repr_ok = True
             except KeyboardInterrupt:
                 raise
