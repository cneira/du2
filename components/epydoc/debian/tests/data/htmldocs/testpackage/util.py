"""
Provides general-purpose utilities.

@sort: FirstClass, _SecondClass, method2, methodOne, CONSTANT_ONE, CONSTANT_TWO

@var CONSTANT_ONE: The first constant
@var CONSTANT_TWO: The second constant

@author: Kenneth J. Pronovici <pronovic@ieee.org>
"""

CONSTANT_ONE       = "one"
CONSTANT_TWO       = "two"

class FirstClass(list):

   """
   Special list class.

   This is a special class that extends list.

   It has some special behavior that users will find really interesting.
   Or, it would if I had remembered to implement that.
   """

   def __eq__(self, other):
      """
      Definition of C{==} operator for this class.
      @param other: Other object to compare to.
      @return: True/false depending on whether C{other is None}
      """
      if other is None:
         return False
      else:
         return True

class _SecondClass(object):

   """
   Represents something else that I forgot just now.
   """

   def __init__(self, name):
      """
      Constructor.
      @param name: Name of this instance
      """
      self.name = name
      self.state = None

def methodOne(d):
   """
   Returns the keys of the dictionary sorted by value.

   There are cuter ways to do this in Python 2.4, but we were originally
   attempting to stay compatible with Python 2.3.

   @param d: Dictionary to operate on
   @return: List of dictionary keys sorted in order by dictionary value.
   """
   items = d.items()
   items.sort(lambda x, y: cmp(x[1], y[1]))
   return [key for key, value in items]

def method2(d, keys):
   """
   Removes all of the keys from the dictionary.
   The dictionary is altered in-place.
   Each key must exist in the dictionary.
   @param d: Dictionary to operate on
   @param keys: List of keys to remove
   @raise KeyError: If one of the keys does not exist
   """
   for key in keys:
      del d[key]

