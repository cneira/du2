INSTDEPENDS += debhelper
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += libcppunit-dev
INSTDEPENDS += libcurl4-openssl-dev
INSTDEPENDS += libsigc++-2.0-dev
INSTDEPENDS += libssl-dev
INSTDEPENDS += zlib1g-dev
