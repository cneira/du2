INSTDEPENDS += debhelper
INSTDEPENDS += gnome-pkg-tools
INSTDEPENDS += gnome-common
INSTDEPENDS += libgtk2.0-dev
INSTDEPENDS += libxml2-dev
INSTDEPENDS += intltool
INSTDEPENDS += gtk-doc-tools
INSTDEPENDS += libglib2.0-dev
# Build-Depends-Indep:
INSTDEPENDS += libglib2.0-doc
INSTDEPENDS += libgtk2.0-doc
