INSTDEPENDS += debhelper
INSTDEPENDS += pkg-config
INSTDEPENDS += default-jdk
INSTDEPENDS += libfreetype6-dev
INSTDEPENDS += libfontconfig-dev
INSTDEPENDS += libxml2-dev
#Build-Depends-Indep:
INSTDEPENDS += ant
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
INSTDEPENDS += javahelper
INSTDEPENDS += texlive-latex-base
INSTDEPENDS += texlive-latex-recommended
INSTDEPENDS += texlive-latex-extra
INSTDEPENDS += latex-xcolor
INSTDEPENDS += texlive-fonts-recommended
INSTDEPENDS += libasm4-java
