graphicsmagick (1.3.25-8+dilos2) unstable; urgency=high

  * Build for DilOS.

 -- Denis Kozadaev <denis@tambov.ru>  Sun, 14 Jan 2018 19:55:03 +0300

graphicsmagick (1.3.25-8) unstable; urgency=high

  * Backport security fix for out of bounds access when reading CMYKA tiff.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Fri, 24 Feb 2017 19:17:41 +0000

graphicsmagick (1.3.25-7) unstable; urgency=medium

  * Add hack to build self-tests on mips* architectures.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sun, 25 Dec 2016 14:42:18 +0000

graphicsmagick (1.3.25-6) unstable; urgency=high

  * Fix CVE-2016-9830: memory allocation failure in MagickRealloc
    (closes: #847072).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Tue, 06 Dec 2016 17:45:52 +0000

graphicsmagick (1.3.25-5) unstable; urgency=high

  * Fix CVE-2016-8682: stack-based buffer overflow in ReadSCTImage (sct.c).
  * Fix CVE-2016-8683: memory allocation failure in ReadPCXImage (pcx.c).
  * Fix CVE-2016-8684: memory allocation failure in MagickMalloc (memory.c).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Tue, 18 Oct 2016 18:52:13 +0000

graphicsmagick (1.3.25-4) unstable; urgency=high

  * Fix CVE-2016-7997: correctly flip image->blob and rotated_image->blob.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sat, 08 Oct 2016 18:54:05 +0000

graphicsmagick (1.3.25-3) unstable; urgency=high

  * Fix CVE-2016-7800: unsigned underflow leading to heap overflow when
    parsing 8BIM chunk.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sun, 02 Oct 2016 20:20:45 +0000

graphicsmagick (1.3.25-2) unstable; urgency=medium

  * Compile magick/semaphore.c without optimization on ppc64el to prevent
    Perl self-test segfaults (closes: #837719).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Fri, 16 Sep 2016 14:25:47 +0000

graphicsmagick (1.3.25-1) unstable; urgency=high

  * New upstream release, with the following security updates:
    - fix heap overflow in EscapeParenthesis() used in the text annotation
      code,
    - Utah RLE: Reject truncated/absurd files which caused huge memory
      allocations and/or consumed huge CPU,
    - SVG/MVG: Fix another case of CVE-2016-2317 (heap buffer overflow) in
      the MVG rendering code (also impacts SVG),
    - TIFF: Fix heap buffer read overflow while copying sized TIFF attributes.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Tue, 06 Sep 2016 17:38:39 +0000

graphicsmagick (1.3.24+hg20160808-1) unstable; urgency=low

  * New upstream, Mercurial snapshot release.
  * Fixes DrawPrimitive() issue (closes: #829063).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sun, 14 Aug 2016 14:24:32 +0000

graphicsmagick (1.3.24-2) unstable; urgency=low

  * Backport upstream fix for DrawPrimitive() (closes: #829063).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Thu, 28 Jul 2016 16:28:45 +0000

graphicsmagick (1.3.24-1) unstable; urgency=high

  * New upstream release, focusing on security fixes for the following image
    formats:
    - DIB: fix out of bound reads and add more header validations,
    - JNG: file size limits are enforced,
    - MATLAB: fix DoS and hang on corrupt deflate stream,
    - META (Embedded Image Profiles): fix out of bounds reads and writes,
    - MIFF (Magick): fix thrown assertion,
    - CVE-2016-3716: Magick Scripting Language file processing is not done by
      default but need to be prefixed with 'msl:',
    - Magick Vector Graphics file processing is not done by default but need
      to be prefixed with 'mvg:' and prevent head overflow problems,
    - PCX: fix unreasonable memory allocation due to intentionally corrupt
      file,
    - PDB: fix heap buffer overflow and out of bounds read,
    - PICT: fix out of bounds write,
    - CVE-2016-3717: for PostScript files always run Ghostscript with -dSAFER
      for safer execution,
    - PSD: fix segmentation violations, heap buffer overflows and out of
      bound writes,
    - RLE: fix out of bounds reads and writes,
    - ReadImages(): fix possible infinite recursion due to a crafted input
      file,
    - RotateImage(): fix thrown assertion,
    - SGI: fix out of bounds writes,
    - SUN: fix out of bounds reads and writes,
    - SVG: fix CVE-2016-2317 and CVE-2016-2318, heap and stack buffer
      overflows, as well as segmentation violations (closes: #814732);
      also fix endless loop, unexpectedly large memory allocation, divide by
      zero and recursion issues,
    - TIFF: fix assertion while reading and fix benign heap overflow,
    - VIFF: fix excessive memory allocation with intentonally corrupted
      input file,
    - XCF: fix heap buffer overflow,
    - XPM: fix several heap buffer overflows and out of bound reads/writes;
      also fix a case of excessive memory allocation,
    - CVE-2016-5118: popen() shell vulnerability via filename that contains
      '|', remove pipe support entirely (closes: #825800);
      file names starting with a '|' character are no longer interpreted as
      shell commands to be executed as input or output,
    - default.mgk file has been pared down in order to reduce security
      exposure,
    - CVE-2016-3714: Gnuplot ('gplt' delegate) support for rendering these
      files is removed since the format is inherently insecure,
    - CVE-2016-3715: adding a 'tmp:' prefix to a filename no longer removes
      the file since this seems dangerous,
    - CVE-2016-3718: sanity check the image file path or URL before passing
      it to ReadImage(),
    - fix several Coverity issues like dereference after null check, multiple
      resource leaks and logically dead code.
  * Update library symbols for this release.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Mon, 30 May 2016 20:02:31 +0000

graphicsmagick (1.3.23-3) unstable; urgency=low

  * Remove JasPer JPEG-2000 codec support build dependency and remove its
    symbols from the libgraphicsmagick-q16-3 library (closes: #818199).
  * Update Standards-Version to 3.9.8 .

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Tue, 24 May 2016 19:26:58 +0000

graphicsmagick (1.3.23-2) unstable; urgency=low

  * Add previously transient gsfonts build dependency (closes: #815736).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Wed, 24 Feb 2016 18:36:00 +0100

graphicsmagick (1.3.23-1) unstable; urgency=medium

  * New upstream release.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sun, 08 Nov 2015 07:35:33 +0100

graphicsmagick (1.3.22-2) unstable; urgency=low

  * Transition libgraphicsmagick++-q16-11 to libgraphicsmagick++-q16-12
    (closes: #803958).
  * Conflict and replace version 1.3.22-1 of libgraphicsmagick++-q16-11 .

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Tue, 03 Nov 2015 23:39:25 +0100

graphicsmagick (1.3.22-1) unstable; urgency=low

  * New upstream release.
  * Update libgraphicsmagick-q16-3 symbols file.
  * Update watch file.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Fri, 23 Oct 2015 21:01:39 +0200

graphicsmagick (1.3.21-4) unstable; urgency=low

  * Change C library name to ending with -q16 for QuantumDepth=16 ABI change
    and compile shared library to include the QuantumDepth value
    (closes: #796310).
  * Remove breaks on pdf2djvu.
  * Make rebuildable (closes: #796307).

  [ Jakub Wilk <jwilk@debian.org> ]
  * Remove obsolete conflicts/replaces on libgraphicsmagick.
  * Version conflicts/replaces on libgraphicsmagick3.
  * No longer need to pass -l and -L switches to dh_shlibdeps.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Mon, 21 Sep 2015 18:10:49 +0200

graphicsmagick (1.3.21-3) unstable; urgency=medium

  * libgraphicsmagick++3 and libgraphicsmagick++11 are co-installable
    (closes: #795099).
  * libgraphicsmagick1-dev needs recent libgraphicsmagick++1-dev
    (closes: #795102).
  * Fix images symlink for development packages (closes: #795172).
  * libgraphicsmagick3 breaks old versions of pdf2djvu .

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Tue, 11 Aug 2015 18:40:11 +0200

graphicsmagick (1.3.21-2) unstable; urgency=medium

  * Upload to unstable for GCC 5 transition.
  * Enable WebP support (closes: #789745).
  * Make rebuildable.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sat, 08 Aug 2015 13:10:35 +0000

graphicsmagick (1.3.21-1) experimental; urgency=high

  * New upstream release, including many security fixes.
  * Start transition from libgraphicsmagick++3 to libgraphicsmagick++11 .
  * Update libgraphicsmagick3 symbols.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sat, 07 Mar 2015 13:10:07 +0000

graphicsmagick (1.3.20-4) experimental; urgency=low

  * Test build with QuantumDepth 16 (closes: #557879).
  * Update Standards-Version to 3.9.6 .

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Wed, 28 Jan 2015 17:56:41 +0000

graphicsmagick (1.3.20-3) unstable; urgency=medium

  * Use upstream fix for AnnotateImage() return value (closes: #759956).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sat, 27 Sep 2014 07:37:31 +0000

graphicsmagick (1.3.20-2) unstable; urgency=medium

  * Change binary libtiff4-dev dependency to libtiff-dev as well
    (closes: #759595).
  * Version perl build dependency to 5.20 or later.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Thu, 28 Aug 2014 21:22:22 +0000

graphicsmagick (1.3.20-1) unstable; urgency=medium

  * New upstream release (closes: #710716).
  * Use GraphicsMagick-1.3.20-CVE-2014-1947.patch from Fedora to fix
    CVE-2014-1947.
  * Add homepage field.
  * Disable update_freetype.h_location.patch , upstream solved freetype
    detection.
  * Sync with Ubuntu.

  [ Matthias Klose <doko@ubuntu.com> ]
  * Build-depend/depend on libtiff-dev rather than libtiff4-dev.
  * Build-depend/depend on lcms2.
  * Build using dh-autoreconf.
  * Fix link error building the demo and test files.

  [ Bart Martens <bartm@debian.org> ]
  * Add watch file.

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Sat, 23 Aug 2014 19:12:09 +0000

graphicsmagick (1.3.18-1) unstable; urgency=high

  * New upstream release, fixing CVE-2013-4589 (closes: #729661).
  * New maintainer (closes: #731915).

  [ Cyril Brulebois <kibi@debian.org> ]
  * Fix FTBFS due to perl test failures (in t/ps/read.t) (closes: #732406).

 -- Laszlo Boszormenyi (GCS) <gcs@debian.org>  Wed, 11 Dec 2013 13:09:16 +0000

graphicsmagick (1.3.16-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix libtool on Hurd. (Closes: #724821)

 -- Pino Toscano <pino@debian.org>  Sat, 28 Sep 2013 12:22:30 +0200

graphicsmagick (1.3.16-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * [SECURITY] Fix "CVE-2012-3438": apply patch from upstream repo:
    http://graphicsmagick.hg.sourceforge.net/hgweb/graphicsmagick/graphicsmagick/rev/d6e469d02cd2
    "coders/png.c: Some typecasts were inconsistent with libpng-1.4 and
    later."
    (Closes: #683284)

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Aug 2012 15:08:57 +0200

graphicsmagick (1.3.16-1) unstable; urgency=low

  * New upstream version 1.3.16.
    + Includes build fix for Perl 5.16.
  * debian/libgraphicsmagick3.symbols: Add symbol from new upstream
    release.

 -- Daniel Kobras <kobras@debian.org>  Mon, 25 Jun 2012 20:50:44 +0200

graphicsmagick (1.3.15-1) unstable; urgency=low

  * New upstream release 1.3.15. Closes: #672982
    + Fixes crash in psiconv. Closes: #611260
  * debian/control: Change (Build-)Depends from libpng12-dev to
    libpng-dev. Closes: #662359
  * debian/control: Add (Build-)Depends on libjbig-dev. Closes: #669947
  * debian/libgraphicsmagick3.symbols: Add symbols from new upstream
    release.
  * PerlMagick/MANIFEST, PerlMagick/typemap: Add build fix for Perl 5.16,
    cherry-picked from upstream VCS. Closes: #676265

 -- Daniel Kobras <kobras@debian.org>  Mon, 11 Jun 2012 20:49:01 +0200

graphicsmagick (1.3.12-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * {Build-,}Depend on libjpeg-dev, not libjpeg62-dev (closes: #629966,
    #633941)

 -- Julien Cristau <jcristau@debian.org>  Mon, 25 Jul 2011 19:56:29 +0200

graphicsmagick (1.3.12-1) unstable; urgency=low

  * New upstream version 1.3.12.
    + Fixes writing to standard output. Closes: #571719
  * magick/effect.c: Disable OpenMP threading on Sparc for MedianFilterImage()
    and ReduceNoiseImage() as it seems to cause eccessively long runtimes.
    Should prevent build failures due to the testsuite timing out on the
    Sparc buildds. Advice from upstream.

 -- Daniel Kobras <kobras@debian.org>  Mon, 08 Mar 2010 22:05:19 +0100

graphicsmagick (1.3.11-1) unstable; urgency=low

  * New upstream version 1.3.11.
    + Merges or supersedes most Debian-specific patches, apart from
      Hurd PATH_MAX fix, added DPS stubs, and tweaks to
      GraphicsMagick-config.
    + Fixes display option -update to work without -delay. Closes: #414779
    + Adjusts selection of output file when option -adjoin is given.
      Closes: #552998
    + No longer crashes when a convolution operation has failed.
      Closes: #539251
    + Fixes conversion of transparent images to XPM. Closes: #560232
  * debian/control: Package compiles with version 3.8.4 of Debian policy.
  * debian/control: Add debhelper substitution variable misc:Depends to
    all Depends lines to placate lintian.
  * debian/copyright: Resync with Copyright.txt and www/authors.rst from
    upstream distribution.
  * debian/libgraphicsmagick3.symbols: Add two new symbols in 1.3.11.
  * debian/rules: Add logfile output if testsuite has failed.

 -- Daniel Kobras <kobras@debian.org>  Mon, 22 Feb 2010 19:33:44 +0100

graphicsmagick (1.3.8-1) unstable; urgency=low

  * New upstream version 1.3.8.
  * magick/image.c, magick/studio.h: Revert an upstream change that defined
    four global string constants as macros, causing an involuntary ABI
    change.
  * magick/static.c: Add stub definitions for registration functions of
    DPS module to ensure a stable ABI.
  * magick/xwindow.c: Debian-specific patch for CVE-2009-1882 superseded
    with upstream change.
  * debian/control: Complies with version 3.8.3 of Debian policy.
  * debian/control: Build-depend on package hardening-includes.
  * debian/libgraphicsmagick3.symbols: Add 65 new symbols in 1.3.8.
  * debian/rules: Replace homebrew hardening flags with generic version
    imported from hardening-includes.
  * debian/rules: Perl binding is no longer built by default. Adjust make
    calls.

 -- Daniel Kobras <kobras@debian.org>  Fri, 29 Jan 2010 00:52:41 +0100

graphicsmagick (1.3.5-6) unstable; urgency=high

  * debian/control: Build-depend on libltdl-dev to link with system-wide
    library. Avoid security bug in included convenience copy. (CVE-2009-3736)
    Closes: #559811
  * debian/control: Include libltdl-dev as a dependency to
    libgraphicsmagick3-dev.
  * debian/libgraphicsmagick3.symbols: Remove ltdl symbols that now get
    pulled in via a library dependency. Closes: #533410

 -- Daniel Kobras <kobras@debian.org>  Thu, 10 Dec 2009 22:00:16 +0100

graphicsmagick (1.3.5-5.2) unstable; urgency=low

  * Non-maintainer upload.
  * Applied patch to fix FTBFS on hurd-i386, by Barry deFreese and Samuel
    Thibault. Closes: #533513. 

 -- Michael Banck <mbanck@debian.org>  Mon, 28 Sep 2009 23:02:18 +0200

graphicsmagick (1.3.5-5.1) unstable; urgency=high

  * Non-maintainer upload.
  * Fixed integer overflow in XMakeImage function in xwindow.c
    (Closes: #530946) (CVE-2009-1882)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Thu, 10 Sep 2009 19:08:13 +0200

graphicsmagick (1.3.5-5) unstable; urgency=high

  * debian/control: Update Conflicts/Replaces of -dev-compat package to
    follow libmagick-dev package split. Closes: #526482
  * magick/GraphicsMagick-config.{in,1}: Do not expose compiler options
    used to build the library itself via GraphicsMagick-config. Only
    provide options that are actually useful to depending applications.
    Adjust documentation accordingly. Closes: #523596

 -- Daniel Kobras <kobras@debian.org>  Thu, 07 May 2009 20:09:28 +0200

graphicsmagick (1.3.5-4) unstable; urgency=low

  * debian/libgraphicsmagick++3.symbols*: Maintaining symbols files
    for a C++ ABI does not appear to be a good idea at present as
    arch- and optimisation-dependent symbols from instantiated standard
    templates tend to slip in. Turn off generation of symbols files
    for the C++ bindings, but keep respective files with suffix
    ".disabled" for potential later re-use. As a side-effect, this
    change also fixes the build failures on i386 and armel.
    Closes: #522706
  * debian/control: graphicsmagick-dbg has been moved to section debug.
    Adapt control file accordingly.

 -- Daniel Kobras <kobras@debian.org>  Wed, 08 Apr 2009 19:37:57 +0200

graphicsmagick (1.3.5-3) unstable; urgency=low

  * debian/rules: On some archs, -z relro has to be passed explicitly
    as a linker option to gcc.
  * debian/libgraphicsmagick++3.symbols.*.in: Strip Debian revision
    that slipped into some symbol versions.

 -- Daniel Kobras <kobras@debian.org>  Thu, 02 Apr 2009 21:51:06 +0200

graphicsmagick (1.3.5-2) unstable; urgency=low

  * debian/changelog: Previous upload ended up in unstable by mistake.
    Correct distribution field in changelog entry accordingly.
  * debian/libgraphicsmagick++3.symbols*: Handle architecture-dependent
    differences in exported symbols of C++ bindings.
  * debian/rules: Restrict hardening options to OS/architecture combinations
    where they actually work.

 -- Daniel Kobras <kobras@debian.org>  Tue, 31 Mar 2009 18:00:49 +0200

graphicsmagick (1.3.5-1) unstable; urgency=low

  * New upstream version 1.3.5. Closes: #516909
    + SONAME versions of C and C++ shared libraries change from 2 to 3.
  * magick/command.c: Avoid double free() error when calling
    "gm import" with option "-frame". Closes: #506473
  * utilities/gm.1: Quote one more single tick in gm(1) man page. Thanks
    to Vincent Mauge.
  * debian/changelog: Add information about security problems fixed in
    1.2.4 upstream release to previous changelog entry.
  * debian/control: Adjust for SONAME changes.
  * debian/control: Remove obsolete alternative dependencies on x-dev and
    gs.
  * debian/copyright: Updated list of authors in line with
    www/authors.html
  * debian/graphicsmagick.docs: Most documentation has moved below www
    and doesn't have to be installed separately. Trim file list
    accordingly.
  * debian/graphicsmagick.install: images subdirectory has moved below
    www, so doesn't have to be installed separately.
  * debian/libgraphicsmagick{,++}2.install: Renamed to
    libgraphicsmagick{,++}3.install.
  * debian/libgraphicsmagick{,_++}3.symbols: Add list of current library
    symbols for C and C++ bindings.
  * debian/rules: Adjust for SONAME changes.
  * debian/rules: Make use of improved security features in gcc and ld,
    unless DEB_BUILD_OPTIONS contain the "noharden" keyword.
  * debian/rules: Packages comply with version 3.8.1 of Debian policy.

 -- Daniel Kobras <kobras@debian.org>  Sun, 29 Mar 2009 18:23:02 +0200

graphicsmagick (1.2.4-1) experimental; urgency=low

  * New upstream version 1.2.4.
    + Fixes DoS vulnerabilities in various coders (CVE-2008-3134).
      Closes: #491439
  * debian/control: Add build-time dependencies on libsm-dev, libice-dev,
    and libxext-dev as required by AC_PATH_XTRA autoconf macro. Also add
    the above as dependencies to libgraphicsmagick1-dev for consistency
    with output of (deprecated) script GraphicsMagick-config. Thanks to
    Simon McVittie for the initial fix. Closes: #486985

 -- Daniel Kobras <kobras@debian.org>  Sun, 06 Jul 2008 19:55:04 +0200

graphicsmagick (1.2.3-1) experimental; urgency=low

  * New upstream version 1.2.3.
    + Includes remaining fixes for all reported lower-impact
      denial-of-service problems in several coders. Closes: #414370
  * debian/rules: Disable workaround for arm stack overflow in drawtest as
    toolchains problems appear to be fixed.
  * debian/rules: Explicitly configure desired docdir.

 -- Daniel Kobras <kobras@debian.org>  Sun, 22 Jun 2008 15:06:52 +0200

graphicsmagick (1.2.1-1) experimental; urgency=low

  * New upstream version 1.2.1.
    + Binary interface is incompatible with 1.1.x releases, library
      SONAME has been changed accordingly.
    + Includes fix for missing cstring include in Geometry.cpp,
      Debian-specific patch dropped.
    + Implements different method to avoid failures of WMF testsuite due
      to rendering changes in libwmf, Debian-specific patch dropped.
  * debian/control, debian/rules: Bump SONAME version of library packages
    from 1 to 2. -dev package names remain unchanged.
  * debian/copyright: Update from upstream's Copyright.txt and AUTHORS.txt.
  * debian/graphicsmagick.docs: Update to current list of documentation
    provided in upstream release.

 -- Daniel Kobras <kobras@debian.org>  Fri, 09 May 2008 16:15:24 +0200

graphicsmagick (1.1.11-3) unstable; urgency=high

  * debian/control, debian/rules: Some of the PS-related testsuites still
    fail if gs is not present. Revert build-conflicts hack and temporarily
    ignore all testsuite failures on hppa and sparc, instead.

 -- Daniel Kobras <kobras@debian.org>  Sun, 27 Apr 2008 17:06:18 +0200

graphicsmagick (1.1.11-2) unstable; urgency=high

  * debian/control: ImageMagick's -dev packages have stopped providing
    virtual, unversioned aliases, recently. Add explicit Conflicts and
    Replaces on the versioned package names libmagick9-dev and
    libmagick++9-dev in the -compat-dev package. Closes: #476584
  * debian/control: Replace obsolete package name gs-gpl with its
    successor ghostscript.
  * debian/control: Build-conflict with ghostscript on hppa and sparc to
    work around a long-standing ghostscript bug that causes our build to
    fail. Postscript support is still present on all archs, but PS-specific
    tests are skipped at build time on hppa and sparc. Closes: #475685

 -- Daniel Kobras <kobras@debian.org>  Mon, 21 Apr 2008 21:38:33 +0200

graphicsmagick (1.1.11-1) unstable; urgency=medium

  * New upstream version, containing multiple security fixes. Closes: #444266
    + Fixes denial-of-service via malicious DCM and XCF files. (CVE-2007-4985)
    + Fixes integer overflows in multiple coders. (CVE-2007-4986)
    + Fixes sign extension error when reading DIB images. (CVE-2007-4988)
    + For reference, GraphicsMagick was not affected by an off-by-one error
      in ImageMagick's ReadBlobString() function. (CVE-2007-4987)
  * Magick++/lib/Geometry.cpp: Add missing cstring include to fix build with
    gcc 4.3. Closes: #462113
  * utilities/gm.1: Fix formatting errors in man page gm(1).
  * debian/control: Packages comply with version 3.7.3 of Debian policy.
  * debian/graphicsmagick.menu: Move section of gm utility from obsolete
    section 'Apps' to current 'Applications'.

 -- Daniel Kobras <kobras@debian.org>  Tue, 26 Feb 2008 21:33:02 +0100

graphicsmagick (1.1.10-1) unstable; urgency=low

  * New upstream version.

 -- Daniel Kobras <kobras@debian.org>  Thu, 20 Sep 2007 00:14:37 +0200

graphicsmagick (1.1.8-1) unstable; urgency=medium

  * New upstream version.
    Merges or supersedes all previously applied patches outside debian/,
    except for changes to ttf testsuite.
  * PerlMagick/t/{ttf,wmf}/read.t: Rendered quality changes depending on
    improvements in external libs in these testcases, so run them to
    gather information, but don't expect them to succeed. Closes: #434343
  * debian/control: Replace ${Source-Version} substitutions with new
    syntax ${binary:Version}.
  * debian/rules: Don't ignore any error from 'make distclean' to keep
    lintian happy.
  * debian/rules: Include generic code snippet to update binary reference
    images for testsuite. Clean up after build. Closes: #424370
  * debian/reference-new/PerlMagick/t/reference/*: Move updated WMF reference
    image to new location, and include updated TTF reference images due to
    changes in rendering with recent freetype.

 -- Daniel Kobras <kobras@debian.org>  Sun, 05 Aug 2007 13:17:58 +0200

graphicsmagick (1.1.7-15) unstable; urgency=high

  * coders/dcm.c: Fix integer overflow in DCM coder. (CVE-2007-1797)
  * coders/xwd.c: Fix integer overflows in XWD coder. (CVE-2007-1797)
  * debian/changelog: Document recently assigned CVE id for xwd overflow
    in previous entry to avoid confusion with the new fixes above.
  * debian/control: Remove dependencies on obsolete version of libjasper-dev.
    Closes: #422379
  * Magick++/lib/Image.cpp: Include missing header file to fix build
    failure with gcc 4.3. Patch thanks to Martin Michlmayr.
    Closes: #417218

 -- Daniel Kobras <kobras@debian.org>  Sun,  6 May 2007 11:39:10 +0200

graphicsmagick (1.1.7-14) unstable; urgency=high

  * magick/image.c: Fix heap overflow in GrayscalePseudoClassImage() on
    64bit architectures. (Turned up by Sami Liedes' segv2.viff test case.)
    Closes: #418052, #416096
  * magick/utility.h: Avoid double free() when calling MagickReallocMemory()
    with zero size argument. (Triggered by Sami Liedes' segv2.viff test case.)
    Closes: #418053
  * coders/tiff.c: Fix segfault with certain TIFF images on amd64 due to
    va_list reusal in bogus duplicate vsprintf() call. Thanks to Kurt
    Roeckx for the fix. Closes: #415467
  * coders/viff.c: Add sanity check to prevent heap overflow reading corrupt
    viff images. (Triggered by Sami Liedes' segv.viff test case.)
    Closes: #418054
  * coders/xwd.c: Fix integer overflow in XWD coders. (Triggered by Sami
    Liedes' broken.xwd test case.) Original patch thanks to Larry
    Doolittle. (CVE-2007-1667) Closes: #417862

 -- Daniel Kobras <kobras@debian.org>  Fri,  6 Apr 2007 17:50:35 +0200

graphicsmagick (1.1.7-13) unstable; urgency=high

  * The following problems were found thanks to numerous testcases provided
    by Sami Liedes:
    + coders/pcx.c: Fix heap overflow vulnerability of scanline array
      with user-supplied input. Closes: #413034
      Also adds error checks and caps maximum number of colours to prevent
      segfaults with further testcases. Closes: #414058
    + coders/pict.c: Fix integer overflow to prevent overflowing a
      heap buffer with user-supplied input. Closes: #413036
      Validate header information to prevent segfaults with further
      testcases. Closes: #414059
    + coders/xwd.c: Check image data more strictly before passing it on to
      XGetPixel() to circumvent buffer overflow in libX11. Closes: #413040
    + Fix various segfaults with corrupt image data due to insufficient
      validation of return values from SeekBlob(). None of these are
      currently known to allow code injection.
      - coders/bmp.c: Add error checks to SeekBlob() calls. Closes: #413031
      - coders/cineon.c: Likewise. Closes: #413038
      - coders/icon.c: Likewise. Closes: #413032
                       Extend validation checks to prevent segfaults with
                       further testcases. Closes: #414057
      - magick/blob.c: Increase robustness of function ReadBlobStream() to
        mitigate the impact of missing error checks on SeekBlob() calls.
    + coders/png.c: Fix NULL pointer dereference due to insufficient
      validation of image data. Closes: #413035
    + coders/pnm.c: Fix segfault on out-of-bounds read access due to
      insufficient validation of image data. Closes: #413037
    + coders/sun.c: Fix segfaults on out-of-bounds read access due to
      insufficient validation of image data. Closes: #413039
  * utilities/miff.4: Trim name section of man page, and move overlong
    line to description. Closes: #390501
  * debian/graphicsmagick.menu: Show logo on startup from menu, rather
    than quitting immediately. Thanks Justin B. Rye. Closes: #407464

 -- Daniel Kobras <kobras@debian.org>  Sat, 10 Mar 2007 23:52:50 +0100

graphicsmagick (1.1.7-12) unstable; urgency=high

  * coders/palm.c: Fix regression introduced in patch for CVE-2006-5456.
    Avoid bogus second read in macro call. Patch thanks to Vladimir
    Nadvornik. (CVE-2007-0770)

 -- Daniel Kobras <kobras@debian.org>  Sat, 10 Feb 2007 15:50:53 +0100

graphicsmagick (1.1.7-11) unstable; urgency=medium

  * config/delegates.mgk.in: Lose obsolete option -2 when calling dcraw
    delegate. Fixes support for raw image data from digital cameras.
    Closes: #405960

 -- Daniel Kobras <kobras@debian.org>  Sun,  7 Jan 2007 17:59:16 +0100

graphicsmagick (1.1.7-10) unstable; urgency=high

  * coders/png.c: Fix syntax errors in asm controlling code of PNG
    coder.
  * debian/changelog: Add recently assigned CVE references to security
    fixes in previous changelog entry.
  * debian/control: Recommend package gsfonts that provides the fonts
    referenced in the default type map.
  * debian/control: Adjust (build-)dependencies as x-dev package was
    superseded by x11proto-core-dev. Closes: #397770
  * debian/Magick.pm: Fix typo in POD section.

 -- Daniel Kobras <kobras@debian.org>  Wed, 13 Dec 2006 19:38:31 +0100

graphicsmagick (1.1.7-9) unstable; urgency=high

  * coders/dcm.c: Fix buffer overflow, thanks to M Joonas Pihlaja.
    (CVE-2006-5456)
  * coders/palm.c: Fix multiple heap overflows, again thanks to M Joonas
    Pihlaja. (CVE-2006-5456)

 -- Daniel Kobras <kobras@debian.org>  Fri, 29 Sep 2006 15:52:41 +0200

graphicsmagick (1.1.7-8) unstable; urgency=high

  * coders/xcf.c: Fix buffer overflow in XCF coder (CVE-2006-3743).
  * It seems I've fixed the vulnerabilities described in CVE-2006-3744
    (coders/sgi.c) independently in the previous upload already while
    the original report had been embargoed.

 -- Daniel Kobras <kobras@debian.org>  Wed,  6 Sep 2006 18:24:36 +0200

graphicsmagick (1.1.7-7) unstable; urgency=high

  * coders/sgi.c: Fix multiple heap overflow vulnerabilities in SGI coder
    due to
    + missing boundary checks in SGIDecode();
    + missing validation of pixel depth field;
    + integer overflow via large columns and rows fields (CVE-2006-4144)
      Closes: #383333
    + missing validation of chunk size fields (variable 'runlength') in
      run-length encoded images.
  * coders/sgi.c: Check for bogus values of 'bytes_per_pixel' and 'depth'.
  * coders/sgi.c: Fix calculation of internal depth value.

 -- Daniel Kobras <kobras@debian.org>  Fri, 18 Aug 2006 11:50:42 +0200

graphicsmagick (1.1.7-6) unstable; urgency=low

  * debian/compat: Bump debhelper compatibility level to 5.
  * debian/control: Build-depend on debhelper version 5 and up.
  * debian/control: Remove redundant Build-Depends-Indep.
  * debian/control: Add new package graphicsmagick-dbg containing debugging
    symbols for all language bindings and the main executable.
  * debian/control: Suggest debugging package where appropriate.
  * debian/control: Build-depend on sharutils for uudecode.
  * debian/control: Version build-dependency on libwmf-dev. Earlier versions
    will fail the testsuite.
  * debian/libgraphicsmagick++1.install: There is no libGraphicsMagickWand++,
    so don't try to install it.
  * debian/libgraphicsmagick{,++}1-dev.install: Remove .la files as long as
    nobody's using them.
  * debian/rules: Give in and disable strict aliasing for the moment until
    we get fixes for all instances that currently break the rules.
  * debian/rules: Place all debugging symbols into graphicsmagick-dbg.
  * debian/rules: New libwmf yields better image quality than old reference
    image in regression test. We cannot patch the binary image directly in
    the Debian diff, so add uudecode magic to check and clean targets.
  * debian/ski.miff.uu: Updated version of reference image in WMF regression
    test. Uuencoded to fit into the Debian diff.
  * magick/cache.c: Include definition of HAVE_PREAD before checking its
    value. Now really pulls in proper declarations of pread() and pwrite().

 -- Daniel Kobras <kobras@debian.org>  Tue,  1 Aug 2006 14:00:30 +0200

graphicsmagick (1.1.7-5) unstable; urgency=low

  * coders/wpg.c: Fix segfault in WPG decoder. Closes: #366191
  * debian/control: Fix typo 'thumnails' in package description.
    Closes: #363623
  * debian/control: Prefer real package zlib1g-dev over virtual libz-dev
    in (build-)dependencies.
  * debian/control: Add (build-)dependency on libjasper-1.701-dev to
    support JPEG2000 images.
  * debian/rules: Change X11 directories from /usr/X11R6/{include,lib} to
    /usr/{include,lib}/X11.
  * debian/control: X11 change makes package comply with policy 3.7.2.
    Bump Standards-Version accordingly.

 -- Daniel Kobras <kobras@debian.org>  Sat,  6 May 2006 16:28:08 +0200

graphicsmagick (1.1.7-4) unstable; urgency=low

  * debian/rules: Lower optimisation level on magick/draw.c and
    wand/drawing_wand.c on arm to work around a compiler issue
    when calling variadic functions. Fixes crashes of the test suite
    on arm.

 -- Daniel Kobras <kobras@debian.org>  Tue, 28 Mar 2006 21:49:16 +0200

graphicsmagick (1.1.7-3) unstable; urgency=low

  * debian/control: Replace pre-etch versions of imagemagick to prevent
    file conflicts with miff.4 and quantize.5 man pages during partial
    upgrade. Closes: #351262
  * tests/drawtest.c: Make sure filename strings do not run out of bounds.
  * magick/cache.c: Define as _XOPEN_SOURCE to pull in declarations for
    Unix98 extensions pread() and pwrite().
  * magick/montage.c: Fix bogus modulation of brightness when creating
    shadows around tiles in montage. Instead, drop constant grey shadow
    like current ImageMagick.
  * PerlMagick/t/montage.t: Update reference signatures for montage test
    cases with shadow according to above change.

 -- Daniel Kobras <kobras@debian.org>  Sun,  5 Feb 2006 21:57:14 +0100

graphicsmagick (1.1.7-2) unstable; urgency=low

  * magick/tempfile.c: Canonify relative paths before referring to
    them in a symlink.
  * debian/control: Add transfig to build dependencies for xfig regression
    test.
  * debian/control: Recommend gs in libgraphicsmagick1 package as it's a
    commonly used delegate.

 -- Daniel Kobras <kobras@debian.org>  Thu, 12 Jan 2006 12:32:11 +0100

graphicsmagick (1.1.7-1) unstable; urgency=low

  * First upload to official Debian archives. Closes: #345017
  * New upstream version.
  * debian/*: Major overhaul to comply with packaging standards. Apart
    from the changelog, few lines have survived the clean-up. Still, we
    try to ensure smooth upgrade from the previous, unofficial packages.
    Most notable packaging changes:
    + Names of library packages are properly versioned.
    + Name of compatibility package expanded to -imagemagick-compat.
    + Removed compatibility shell script, and replaced with simple
      symlinks to gm. Extra functionality from wrapper was only required
      by old versions of typo3 according to previous maintainers.
    + New compatibility package -libmagick-dev-compat providing wrappers
      for package development.
    + Build separate packages for C++ library.
    + Drop separate -doc package.
    + More verbose package descriptions.
    + Run test suite at build time.
    + Disable support for obsolete libdps. Build-depend on ghostscript
      instead for ps/pdf regression tests.
    + New maintainer.
  * PerlMagick/t/ttf/read.t: Disabled ttf testcase that is known to fail
    because of problems with special characters.
  * magick/{blob.c,command.c,image.c,log.c,utility.c,utility.h}:
    FormatString() was called with unsanitized user input. Introduced
    new helper function FormatStringNumeric() to allow a single numeric
    format expansion. (This is a more complete fix for CAN-2005-0397
    reported against ImageMagick.)
  * magick/attribute.c: Apply missing piece of fix for heap overflow in
    EXIF parser from ImageMagick patch. (CAN-2004-0981)
  * configure.ac, configure: Fix typo that lead to an undefined delegate
    for HTML conversion.
  * magick/constitute.c: Apply upstream fix for potential NULL pointer
    dereference in ReadImage().
  * magick/{delegate.c,symbols.h,tempfile.h,tempfile.c}: When calling
    external delegates, check filename against whitelist of safe
    characters, and pass securely named symlink to delegate if check fails.
    (CVE-2005-4601)

 -- Daniel Kobras <kobras@debian.org>  Mon,  9 Jan 2006 22:19:07 +0100

graphicsmagick (1.1.6-3) unstable; urgency=low

  * Added colors.mgk to libgraphicsmagick

 -- Michael Stucki <michael@typo3.org>  Sun, 15 May 2005 22:15:02 +0200

graphicsmagick (1.1.6-2) unstable; urgency=low

  * changed value for MagickLibSubdir and MagickShareSubdir in configure.ac
  * changed value for includedir in Makefile.am in
    - magick/Makefile.am
    - Magick++/lib/Magick++/Makefile.am
    - Magick++/lib/Makefile.am
    - wand/Makefile.am

 -- Michael Stucki <michael@typo3.org>  Sun, 15 May 2005 15:00:48 +0200

graphicsmagick (1.1.6-1) unstable; urgency=low

  * New upstream release

 -- Michael Stucki <michael@typo3.org>  Sun, 15 May 2005 04:48:06 +0200

graphicsmagick (1.1.2-5) unstable; urgency=low

  * Backport on Debian Sarge
  * Fixed a bug in -im-compat
  * Renamed gm-wrapper to graphicsmagick_wrapper

 -- Michael Stucki <mundaun@gmx.ch>  Thu, 12 Aug 2004 00:55:27 +0200

graphicsmagick (1.1.2-4) unstable; urgency=low

  * Fixed package -im-compat, shell-script was not executable

 -- Sven Wilhelm <wilhelm@icecrash.com>  Fri,  6 Aug 2004 19:56:38 +0200

graphicsmagick (1.1.2-3) unstable; urgency=low

  * Added wrapper script for im compatibility
  * Fixed descriptions in control file
  * Changed to library libtiff4

 -- Sven Wilhelm <wilhelm@icecrash.com>  Fri,  6 Aug 2004 16:01:43 +0200

graphicsmagick (1.1.2-2) unstable; urgency=low

  * Fixed missing *.mgk files
  * -doc package now has its content

 -- Sven Wilhelm <wilhelm@icecrash.com>  Fri,  6 Aug 2004 14:34:33 +0200

graphicsmagick (1.1.2-1) unstable; urgency=low

  * Initial Release.
  * changed value for MagickLibSubdir in configure.ac
  * changed value for includedir in Makefile.am in
    - magick/Makefile.am
    - Magick++/lib/Magick++/Makefile.am
    - Magick++/lib/Makefile.am
    - wand/Makefile.am

 -- Sven Wilhelm <wilhelm@icecrash.com>  Mon,  7 Jun 2004 02:23:06 +0200
