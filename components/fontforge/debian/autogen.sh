#!/bin/sh

set -e

# if below check fails, ensure these are correct and update md5sum
GNULIB_MODULES="warnings manywarnings c-strtod gethostname getline \
iconv inet_ntop localeconv progname strcase strtok_r strndup tmpfile \
xalloc xvasprintf"

if ! echo 3669ba4f10d68e36751cb6a9e09ade03 bootstrap.conf | md5sum -c -; then
	echo ERROR: bootstrap.conf changed - please update debian/autogen.sh
	exit 1
fi

gnulib-tool --libtool --no-vc-files --import "$GNULIB_MODULES"
autoreconf --force --install
