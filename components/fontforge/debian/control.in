Source: fontforge
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <pkg-fonts-devel@lists.alioth.debian.org>
Uploaders:
 Christian Perrier <bubulle@debian.org>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
 Davide Viti <zinosat@tiscali.it>,
 Hideki Yamane <henrich@debian.org>,
 Jonas Smedegaard <dr@jones.dk>,
 Rogério Brito <rbrito@ime.usp.br>,
 Vasudev Kamath <vasudev@copyninja.info>
XS-Python-Version: all
Build-Depends: @cdbs@
Standards-Version: 4.1.0
Homepage: https://fontforge.github.io/en-US/
Vcs-Git: https://anonscm.debian.org/git/pkg-fonts/fontforge.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-fonts/fontforge.git

Package: fontforge
Architecture: any
Multi-Arch: foreign
Depends:
 fontforge-common (= ${source:Version}),
 libfontforge2 (= ${binary:Version}),
 libgdraw5 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Conflicts:
 fontforge-nox
Suggests:
 autotrace,
 fontforge-doc,
 fontforge-extras,
 potrace,
 python-fontforge
Description: font editor
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package also provides these programs and utilities:
  fontimage - produce a font thumbnail image;
  fontlint  - checks the font for certain common errors;
  sfddiff   - compare two font files.

Package: fontforge-nox
Architecture: any
Multi-Arch: foreign
Depends:
 fontforge-common (= ${source:Version}),
 libfontforge2 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Conflicts:
 defoma,
 fontforge
Description: font editor - non-X version
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package also provides these programs and utilities:
  fontimage - produce a font thumbnail image;
  fontlint  - checks the font for certain common errors;
  sfddiff   - compare two font files.
 .
 This package contains a version of FontForge compiled with support for
 scripting but no GUI, and not require the graphics library.

Package: fontforge-common
Architecture: all
Depends:
 ${misc:Depends}
Breaks:
 fontforge (<< 1:20160404~dfsg-3),
 fontforge-nox (<< 1:20160404~dfsg-3)
Recommends:
 fonts-cantarell,
 fonts-inconsolata
Description: font editor (common files)
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package contains common arch-independent files.

Package: libfontforge-dev
Section: libdevel
Architecture: any
Depends:
 libfontforge2 (= ${binary:Version}),
 libgdraw5 (= ${binary:Version}),
 ${misc:Depends},
 ${devlibs:Depends}
Description: font editor - runtime library (development files)
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package contains the runtime library's development files.

Package: libfontforge2
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Breaks:
 libfontforge1
Replaces:
 libfontforge1
Description: font editor - runtime library
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package contains the runtime library.

Package: libgdraw5
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Breaks:
 libgdraw4
Replaces:
 libgdraw4
Description: font editor - runtime graphics and widget library
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package contains the graphics and widget runtime library.

Package: python-fontforge
Architecture: any
Depends:
 libfontforge2 (= ${binary:Version}),
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Section: python
Description: font editor - Python bindings
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package provides the Python modules (the libraries fontforge and
 psMat) to access a FontForge program for font manipulations.

Package: fontforge-dbg
Section: debug
Architecture: linux-any
Priority: extra
Depends:
 fontforge (= ${binary:Version}),
 ${misc:Depends}
Description: debugging symbols for fontforge
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package contains the debugging symbols for fontforge.

Package: fontforge-doc
Section: doc
Architecture: all
Priority: optional
Depends: ${misc:Depends}
Description: documentation for fontforge
 FontForge is a font editor.
 Use it to create, edit and convert fonts
 in OpenType, TrueType, UFO, CID-keyed, Multiple Master,
 and many other formats.
 .
 This package contains the documentation for fontforge.