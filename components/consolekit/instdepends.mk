INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += libdbus-glib-1-dev
INSTDEPENDS += libglib2.0-dev
#INSTDEPENDS += libacl1-dev [linux-any]
#INSTDEPENDS += libudev-dev [linux-any]
INSTDEPENDS += libx11-dev
INSTDEPENDS += libkvm-dev
INSTDEPENDS += xmlto
INSTDEPENDS += libpam-dev
INSTDEPENDS += libpolkit-gobject-1-dev
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += libbsd-dev
INSTDEPENDS += dh-autoreconf
