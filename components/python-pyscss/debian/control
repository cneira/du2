Source: python-pyscss
Section: python
Priority: extra
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: W. Martin Borgert <debacle@debian.org>,
           Thomas Goirand <zigo@debian.org>
Build-Depends: debhelper (>= 9.20120909~),
               dh-python,
               docbook-xsl,
               libpcre3-dev,
               python (>= 2.6.6-3~),
               python-all-dev,
               python-enum34,
               python-pathlib,
               python-pil,
               python-pytest,
               python-setuptools,
               python-six,
               python3-all-dev,
               python3-pil,
               python3-pytest,
               python3-setuptools,
               python3-six,
               xsltproc
Standards-Version: 3.9.8
Homepage: https://github.com/Kronuz/pyScss
Vcs-Git: https://anonscm.debian.org/git/python-modules/packages/python-pyscss.git
Vcs-Browser: https://anonscm.debian.org/cgit/python-modules/packages/python-pyscss.git

Package: python-pyscss
Architecture: any
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}, python-pkg-resources
Description: SCSS compiler - Python 2.x
 pyScss compiles Scss (Sass), a superset of CSS that is more
 powerful, elegant and easier to maintain than plain-vanilla
 CSS. The library acts as a CSS source code preprocesor which
 allows you to use variables, nested rules, mixins, and have
 inheritance of rules, all with a CSS-compatible syntax which
 the preprocessor then compiles to standard CSS.
 .
 This package provides the Python 2.x module.

Package: python3-pyscss
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}, python3-pkg-resources
Description: SCSS compiler - Python 3.x
 pyScss compiles Scss (Sass), a superset of CSS that is more
 powerful, elegant and easier to maintain than plain-vanilla
 CSS. The library acts as a CSS source code preprocesor which
 allows you to use variables, nested rules, mixins, and have
 inheritance of rules, all with a CSS-compatible syntax which
 the preprocessor then compiles to standard CSS.
 .
 This package provides the Python 3.x module.
