Source: tnftp
Section: net
Priority: optional
Maintainer: Anibal Monsalve Salazar <anibal@debian.org>
Build-Depends: debhelper (>= 7), libncurses5-dev, libssl-dev, autotools-dev
Standards-Version: 3.9.6
Homepage: http://en.wikipedia.org/wiki/Tnftp

Package: tnftp
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: lukemftp
Replaces: lukemftp
Description: enhanced ftp client
 tnftp is what many users affectionately call the enhanced ftp
 client in NetBSD (http://www.netbsd.org).
 .
 This package is a `port' of the NetBSD ftp client to other systems.
 .
 The enhancements over the standard ftp client in 4.4BSD include:
    * command-line editing within ftp
    * command-line fetching of URLS, including support for:
        - http proxies (c.f: $http_proxy, $ftp_proxy)
        - authentication
    * context sensitive command and filename completion
    * dynamic progress bar
    * IPv6 support (from the WIDE project)
    * modification time preservation
    * paging of local and remote files, and of directory listings
      (c.f: `lpage', `page', `pdir')
    * passive mode support, with fallback to active mode
    * `set option' override of ftp environment variables
    * TIS Firewall Toolkit gate ftp proxy support (c.f: `gate')
    * transfer-rate throttling (c.f: `-T', `rate')
