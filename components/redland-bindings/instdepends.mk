INSTDEPENDS += debhelper
INSTDEPENDS += autotools-dev
INSTDEPENDS += dh-autoreconf
INSTDEPENDS += dh-python
INSTDEPENDS += gem2deb
INSTDEPENDS += librdf0-dev
INSTDEPENDS += libtool
# perl
INSTDEPENDS += python-all-dev
INSTDEPENDS += ruby
INSTDEPENDS += ruby-dev
INSTDEPENDS += swig
