INSTDEPENDS += debhelper
INSTDEPENDS += libcairo2-dev
INSTDEPENDS += libgl1-mesa-dev
INSTDEPENDS += libglu1-mesa-dev
INSTDEPENDS += libjpeg-dev
INSTDEPENDS += libpng-dev
INSTDEPENDS += libx11-dev
INSTDEPENDS += libxcursor-dev
INSTDEPENDS += libxext-dev
INSTDEPENDS += libxft-dev
INSTDEPENDS += libxinerama-dev
INSTDEPENDS += pkg-config
INSTDEPENDS += zlib1g-dev
# Build-Depends-Arch:
INSTDEPENDS += cmake
#INSTDEPENDS += libasound2-dev [linux-any]
# Build-Depends-Indep:
INSTDEPENDS += doxygen-latex
INSTDEPENDS += texlive-font-utils
