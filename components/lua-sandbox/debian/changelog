lua-sandbox (1.2.1-4+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- Denis Kozadaev <denis@tambov.ru>  Wed, 03 Jan 2018 16:10:47 +0000

lua-sandbox (1.2.1-4) unstable; urgency=medium

  * Fix default path and cpath
  * Use the correct temp path for luasandbox-deb-multiarch.h

 -- Mathieu Parent <sathieu@debian.org>  Sat, 31 Dec 2016 15:19:09 +0100

lua-sandbox (1.2.1-3) unstable; urgency=medium

  * Add luasandbox-binary-doc.patch
  * Update patches and mention upstream pull-requests
  * Fix FTBFS:
    - on 32-bit (Closes: #845929), and
    - big-endian architectures (Closes: #845928)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 13 Dec 2016 21:34:02 +0100

lua-sandbox (1.2.1-2) unstable; urgency=medium

  * Improve unsandboxed mode by adding package.preload
  * Also expose package.sandboxed

 -- Mathieu Parent <sathieu@debian.org>  Thu, 17 Nov 2016 22:11:24 +0100

lua-sandbox (1.2.1-1) unstable; urgency=medium

  * New upstream version
    - Build-Depends: git no more needed
  * Add me to Uploaders
  * Add pkgconfig support, inspired by src:lua5.1
  * Restore lua.c from lua5.1
  * Make luasandbox behave like lua5.1
    + setprogdir and setpath (private) functions are back
    + luaopen_package has an additionnal argument "sandboxed"
      - which defaults to false
      - which is set to true from lsb_init
    + if sandboxed, behavior is not changed
    + if not sandboxed:
      - cpath and path, are set in "lsb_config" hidden table
      - package has a metatable which only allow access to cpath and path when
        not sandboxed
  * Set default path and cpath when not sandboxed
  * Run dh_makeshlibs with -c4 to ensure we don't miss symbol changes
  * Update debian/libluasandbox0.symbols
  * Remove unused lintian override

 -- Mathieu Parent <sathieu@debian.org>  Wed, 16 Nov 2016 22:41:36 +0100

lua-sandbox (1.1.0-2) unstable; urgency=medium

  * Conflicts on upstream package name (luasandbox) to avoid installing both
    together.

 -- Raphaël Hertzog <hertzog@debian.org>  Sat, 01 Oct 2016 15:16:16 +0200

lua-sandbox (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Add git to Build-Depends.
  * Update symbols file.
  * Install /usr/include/luasandbox.h

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 27 Sep 2016 15:40:32 +0200

lua-sandbox (1.0.3-1) unstable; urgency=medium

  * Initial release (Closes: #838969).

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 27 Sep 2016 10:51:39 +0200
