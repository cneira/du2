INSTDEPENDS += debhelper
INSTDEPENDS += cmake
INSTDEPENDS += zlib1g-dev
INSTDEPENDS += pkg-kde-tools
INSTDEPENDS += libboost-dev
INSTDEPENDS += libicu-dev
# Build-Depends-Indep:
INSTDEPENDS += doxygen
INSTDEPENDS += graphviz
INSTDEPENDS += gsfonts-x11
