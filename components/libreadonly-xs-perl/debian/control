Source: libreadonly-xs-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>
Section: perl
Priority: optional
Build-Depends: debhelper (>= 9.20120312~),
               perl
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-perl/packages/libreadonly-xs-perl.git
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/libreadonly-xs-perl.git
Homepage: https://metacpan.org/release/Readonly-XS/

Package: libreadonly-xs-perl
Architecture: any
Depends: ${perl:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: libreadonly-perl
Description: faster Readonly implementation
 The Readonly module (q.v.) is an effective way to create non-modifiable
 variables. However, it's relatively slow.
 .
 The reason it's slow is that is implements the read-only-ness of variables via
 tied objects. This mechanism is inherently slow. Perl simply has to do a lot
 of work under the hood to make tied variables work.
 .
 This module corrects the speed problem, at least with respect to scalar
 variables. When Readonly::XS is installed, Readonly uses it to access the
 internals of scalar variables. Instead of creating a scalar variable object
 and tying it, Readonly simply flips the SvREADONLY bit in the scalar's FLAGS
 structure.

