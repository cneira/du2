Source: inkscape
Section: graphics
Priority: optional
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders:
 Matteo F. Vescovi <mfv@debian.org>,
 Mattia Rizzolo <mattia@debian.org>
Build-Depends:
 bash-completion (>= 1:2.1-4.2),
 cmake,
 debhelper (>= 10),
 dh-python,
 gnome-pkg-tools,
 libart-2.0-dev (>= 2.3.10),
 libaspell-dev,
 libboost-dev,
 libcdr-dev,
 libdbus-1-dev,
 libdbus-glib-1-dev,
 libgc-dev (>= 1:6.8),
 libglib2.0-dev,
 libgsl-dev,
 libgtk2.0-dev (>= 2.10.0),
 libgtkmm-2.4-dev,
 libgtkspell-dev,
 liblcms2-dev,
 libmagick++-dev,
 libpango1.0-dev,
 libpng-dev,
 libpoppler-glib-dev,
 libpoppler-private-dev,
 libpopt-dev,
 libpotrace-dev,
 librevenge-dev,
 libsigc++-2.0-dev (>= 2.0.16-2),
 libtool,
 libvisio-dev,
 libwpg-dev (>= 0.3.0),
 libxml-parser-perl,
 libxml2-dev (>= 2-2.4.24),
 libxslt1-dev,
 pkg-config,
 python-dev,
 python-lxml,
 zlib1g-dev
Standards-Version: 3.9.8
Homepage: https://inkscape.org
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/inkscape.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-multimedia/inkscape.git

Package: inkscape
Architecture: any
Depends:
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends}
Recommends:
 aspell,
 imagemagick,
 libimage-magick-perl,
 libwmf-bin,
 python-lxml,
 python-numpy,
 python-scour,
 transfig
Suggests:
 dia | dia-gnome,
 libsvg-perl,
 libxml-xql-perl,
 pstoedit,
 python-uniconvertor,
 ruby
Description: vector-based drawing program
 Inkscape is an illustration editor which has everything needed to
 create professional-quality computer art. You can use it to make
 diagrams and illustrations, technical drawings, web graphics, clip art,
 icons and logos. A collection of hands-on tutorials show you how to
 combine lines, shapes and text of different types and styles to build
 up a picture.
 .
 A selection of powerful vector graphics editing tools comes as
 standard. There is excellent support for paths, gradients, layers,
 alpha transparency and text flow control. An extensive library of
 filters allow you to apply realistic effects and extensions allow you
 to work with bitmaps, barcodes and printing marks, amongst other things.
 .
 Most of the common vector formats are supported, including PDF, Adobe
 Illustrator and AutoCAD files, and it has unrivalled support for the
 SVG web graphics standard.
 .
 Between the suggested packages:
  * dia: to export Dia shapes;
  * libsvg-perl: to import .txt files (txt2svg extension);
  * libxml-xql-perl: to use the shadow effect;
  * python-uniconvertor: enables several import/export extensions;
  * pstoedit: to work with eps files;
  * ruby: there are several extensions written in ruby;
