-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBEoEbMcBEACg2ByFTN0inbeNg5aBs2H49AtW/eGqbiWMML3RwlfPqu+I2MGC
PeOHBWjtSWyPDixrL1DGDA4Cs0uoxk98sRZE8peAhGpFEdiAcGuQU/JcJ0gDTsfj
1WKMcWi6yI5eu8NinkW2pJuMgLpxNtD2j8wfegoBttB4omXinOpCHuz7lGYenbZk
6/DCgzVeq+ssOdfjPLSJJPIyIIwhdDorXX0pvzAou168LFlDJaWx7OytYfKz1zV/
f+bwnzbMRriAClJYgNl+UT+XnHO3zMIy1mSk4uffaDXeRPPO/R6lM/u7a5w9wHi/
oKIPHJ9BmsgA5vBImuNNRa2pnOHwpBnphnpvqLm/98JAJJfMkoefy2Oc2J6PxJla
pP090sXzt6T7YpR9epwZCO5+OU6sIbK/vjy1pi0hxx847H4hrKzW67kr9o5btjxm
FybqLTT+o01n7x9/A6SBE/vVAfZ1OYm0/DoSNdKpaQtvNeQ1h5gw7gY/uT8VCQB+
ZQVRQkInAqYSzO4oYPS9ynud5d3qNllpZs77EaEN5yVKZk/36QUGoRdbmpZoMTjB
aaM6G0MUO+1FikvBT+aDmomgD+JkDOZf1bIJaSg/QtIIjq5ALExbk1XDkL++XVDJ
9Ag7U467kinjcKWuVIr2aOMMSlXFuDFlsZbeJGCqkdkc2Ucdy1p0fZPWWQARAQAB
tChTaW1vbiBNY1ZpdHRpZSA8c21jdkBwc2V1ZG9yYW5kb20uY28udWs+iQI6BBMB
CAAkAhsDAh4BAheAAhkBBQJKnvRCBQsJCAcDBRUKCQgLBRYCAwEAAAoJEE3o/ypj
x8yQ2vcP/ijFM4ofsHUnqzGH/bgj15cJIrUYt4C/W70aou65kmaAuHbX5DwTDa+l
XTGg/HsOUBQa487u7v/Gpv6yLtRU+TUcdoTxcFNYjlIVtcNtOwrGcibLZRPrfEHL
lG3lgKjA+YjM6CpgB+4vSkecWPSfFmAFY2096Gi9vAy+v9KLfahlnyuh2G6MQqx4
NdiQKVdTTdKtHRasEBMGIMR4tQAiTG2VI+g8X+uugoEquwYqbpmYQdwGvohamWZB
xzTH8NkkGC0Y8yO3hId1kTdTCcQVgDznFpd4CABUXcQgUvc1/pZyVvozH46gCjU7
w6qAZfTb9/gN0/sA1zPrGjfuPAyf1NCH9d7bHgjpR5+PGS2KsB3L0OK6gMMJKAGk
kuiXZIFoOaQ8IFA5OKAFySGMRKkobppO60ehbPQ7VNKnluTOTgJTxwrw+iORKnLh
yZmdbx7D8eQfbpgw5vsbt49fUF6BxEgF6a32D7uzeEu/BBPH/+mOp7NSmimXzV2q
6FSgsrwN7uNDH9DmS+s1YkRB/J7bnIgxsdGjMdtVVz5EnEtZIyWZTLYbPNJksQen
I66bzGw+OtGmJyDeJrqzObo9sqefaj7fY9O8VH4hVxg/nL4gAAXCsejPRaoGd1cN
JuyRgzyyD9pghrrgTz1clkVoD3NC3Z9HC2FdvhF14zVnBfYv5lQ4tC9TaW1vbiBN
Y1ZpdHRpZSA8c2ltb24ubWN2aXR0aWVAY29sbGFib3JhLmNvLnVrPokCNwQTAQgA
IQIbAwIeAQIXgAUCSp70SgULCQgHAwUVCgkICwUWAgMBAAAKCRBN6P8qY8fMkAYU
D/9qijOq8qHRBWunLhdtbcOFSH5yxgENoZlyabqwytyfpN8bHrKJz39hjgLNdUcs
eI04FLvYMsMLzaVqpAN8RWaP8apD0v5RCSLkTPtOa944wOwY87MxsINFDUPI19Rj
w0ZafYVqwwPRymXuwklGcjkuXOuRW01UTxqkNMUsNUpcq0or1ffLqX5zORPrkJNi
xJyOXnsu3xhPAWOEFet76R2pdg07b4oCIK/uwiiJCcno2WPOG5tQszbdmGj0l4WE
UNvRzh6bOFnpxqIwnj1zsHyuX2bUZQYvr49eC96QYMkrVkU+DVweDouMTtqz1DKY
1+iAnvxl0uCtvJ57OxASrh/ppcQ3O58uCCWY+HlEcjQV017S4stP+cXCode8g9Ds
Vq1m6Qt1eH5HwgZElQhGJwuY/ycC/VUnIAQPZHSZwsjlRynjPGqbVYDq4O0wuO2u
qtwzcxfNUEs6PM56nfatV51lqBHVuDHUK9raM+yjnyvWvY89OHi/MRBdvcvTRPlL
BLp2gCG+T7uGnnR2XTAPZgV3CMmg3Hxe2qOMV/QyCNX++hVdmzY6CxK/Icn5Flu3
7u/Ss2x5IKNlEJ4TvY+ZKdLvokcThmPXwHANoE9Ojj+q0aUMbNnjAZ/eVMpTANqf
deJKIYnjrlNQ1yQW3yVcij6HyQBmwH5LdiI/E+sCqJnWE7QmU2ltb24gSmFtZXMg
TWNWaXR0aWUgKGJvcm4gMTk4My0wOC0yNSmJAjcEEwEIACECGwMCHgECF4AFAkqe
9EoFCwkIBwMFFQoJCAsFFgIDAQAACgkQTej/KmPHzJDVkw//WqYK1jnB0trOcPYC
PYCZddxmy6CJ5QBcN2o7FcL3XhJAdg2u7rA6GNIULr7Dg20qjdRoU4ShWjwy0HSo
p2atydLk0hvciIzDD10mCObjx7n8IMfrF1MyzZTFmKXoeBuqiVlzG1gWqGWat5Rp
KL1SEj5TlepSN2enHmeKtdnmuty4tPjRZE/SjJ/TQUUbVFTPx3z3yzWgMuByrBJr
j2haMKi0yI63YP8LwsPneIGJwR7BU4yvuTFilfwJqziQKHfvYAfaGnuxG+bd98eo
Gf43tJBt/KwUT9LMF2QmqWYTMZdlj1j6wBcFYAPoKZgAQ87Vn3jDbne+vz7PUvS1
I1lAsFYA42z0oBlT7K0bzuv17i1l+h7+IJc663gtYgglIivp8zGVXgEO0A3wtiGZ
uHhAegt9NaTc0de37Fcxd2Q+xVqonvnPq1Z6HHHo5ijfX9qrw4sVF6xGEQ8dV6vs
rC7mx/AA7LTMFOw//UMfnErqS95jpeqtcdcwv0jxYb1myzpVzZPSpAruzJwxOxMP
FW/Ms631YKQuJqt3slCM6kPJ7f33BbO/+rSgPHk3ipeGBvmzvtvpzMpgIHFad4pr
dSWbBEIv65LRlr8JgeRic4vCkNzoteEACONiye/LEJ0cBNubGCURAkJN0ePTE/h2
WtsWMOR9HrKbigEPHeVdxP6EQAK0IFNpbW9uIE1jVml0dGllIDxzbWN2QGRlYmlh
bi5vcmc+iQI3BBMBCAAhAhsDAh4BAheABQJKnvRKBQsJCAcDBRUKCQgLBRYCAwEA
AAoJEE3o/ypjx8yQi50P/AqX1l7MQSVVyb5/OCAPX7/RgI2BdVwi8NmPBAbaVW5D
P3ypvF5GVTuTZLKnjKRA/KlnUMp4ZdTYuGs5saXwccp8iIzx+Yjz7ETnKtQE+9cF
1BU3Nb85P9gKUcBFu4fmlThi9iEACRbV4PJMqE8q48qwF3WBjqEGhMP4nv02fuOD
6PxUXq9EUVzL3Paj5Vt/4D4lDKVAF8RIbVmdCNKk5swlx3TRAzHTwAEtDH6V1rIY
LYyFBknBKSGrhrQRg1QbOT9M34GkD0Dh7s2uRt3dQVW4zds2vSlZb7CSx2kpYDTz
N6mGpf0kgk7B+7bN+MWlTdb9GWlqEqEK51CLIGxLQscPtb7mFB8Z6JCqmSN+a46e
9/1tCc/VbVZPUs4taeGdaEtedHHWxoiPu5Ajdm1IIZRaPQn6BVmMaqoB9eo1RsOb
apYkORZo0q5iU9lVJqQIXLSfeiMNUrNhOLavYeEP1DLCFkXEsJQwVYeYQFDwoKpC
VCIyxF5+pIgMi0OlJHzEKGiSiSt7pgcbmqj4duPzdZD+EsvJ+DQhvtWUtaQcuRP3
Uzy7aQdpRHI9iDV42kqML0UmA7ZLyXNjAVWYqliVT+MdCVkdXtEG+LxNLxmvcgcZ
O/ehzAMhbpoKbYbt0E+Qf1BFzmqYsFrDoBZknNqwu+UG2CgrIdy4EkN1nt7dRBKY
uQINBEoEbdcBEADEsDOaYh5icY1nuEmnpAZXZme5G6u29N5rahji9ErEq9QBOAw8
6lhHqOg0IUCb5Ci5gJ0nIZo1bzUOMyA+VT96Rk0eQ9mpD7daLl9UsgaAnw3bvUjc
2L1p64AHNTH1TeErtZRu8ot0VPpu3QMCE3DfgVBOCTVl7nXnrPcnzav9fEwtuM98
WW32KcNaAlnnstN4ZeVNT6z5ew8PcawZ42qlptteZVITnI1Ex0HX+gtMXjx96Evu
vegPo0ovFTwhu8+gCTZZgib2ld2nhY7zbYlqZdWS971c4SCsbpTsvqe2yv2RfTj4
U+X+rF0tnv2UbhrfutrOavp9a+19vAbqGE4Cf+cArP6FOCF/c5rSv7/JTtv6TKRP
YgsHd4RbdbTwHJA/NblcnIp8Wfqp/pk+jgR/+RvWBMOwVMmjcv2qteB+HGdU7GdJ
wJcDWm78skCKNTXBvNuQOLmfmS87b1xPKG2zjiw1khW1geDVEAagLcSCpaCLzFBR
NFiKgoCLP8yCHKFCUZ/L5NNo5v82jvNpsqBs0b4r2Xc7WZQhrbCY7C3W5uHcz9KM
sHTl3ktGKlo4g7jTS28ntCPX1UbyqI69G2p/78eNnFMvdQpaAbY6DbglqqZqs03R
Qe8XOz1jDcUOlaUo3LCh/Nzu6cOf6Sy13WOLJf0nURxVB44Uvrhn3Hi+IwARAQAB
iQIlBBgBCAAPBQJKBG3XAhsMBQkSzAMAAAoJEE3o/ypjx8yQ650QAJqFhXlpkR2s
vmT3nX+zXyBZ83KmXkWGij7mVE9Ixm/OHtDryMCkg2PmnJTz+qfJjzSSaYXD8grm
4/Hv9nOiHjPS+ry2KAS86DZtpoUcCW6G0PAriCuXy7PGKYy6lNAv/ohty9v8MxaU
BnO8gQaFLgr0n7ZTEOA5VORQ/1I23tCF2PdSTKbZ8DZ6vp3DI7jdntNQ9qOURWKO
cAUhYkLanOuISqYGmn5O8az1nreBg73EMIZUgqkdSq4Qhi2vXx0LkLIppjgoITJV
sj+bDA9wKBMbWBzZuUeCxtTpClA5aUi1o+U923Mta+pBkHhK95LQA6an5j7GTtqV
hewB0LuISiYvo8pwhrJYTgOWxvUORu6MjAt3aSQ03+Qyey6q3/Dzzmflkjc6B8L4
fgFMpL3h+t/Ps663gdgTAcqCtVib49yQK3l/71ezRQkOV1urN7X98Vxhp0ESBJvy
3rEVdssn6Su6X8ipYyELHS46857+afsxKklzbqRL/H+kswekrbXFQJrFB5ZDa3un
nsg+FsEy+NSRgrShzit5+EgBGjoHWUuE1bWKltlg82Npit+sD4GVeAA0D8XsYYqX
Xf//AliE5K07bLQ+awzEApvQQWxp5WlMk6fKrRNWrd4dsY9qEa3Wq4GxNnLLIbKj
fqO+14gha9DuX9SrolCuuwhceBmxokvemQINBFYaLPsBEAC39yLPt1aNdaF00fDg
pNgzAmVkVIXx7elZpf0ttUbpqWHXOusMUso3UAPJsAanPbOBkP2dQaYq6JHPAzde
gZZxrd+GmTX+NwODbIhxplhG+t5hT48O6y3RNq3/r/Wplk4eq0igsNpAlULO/Xbe
BeOIFFOPiLPPLav+1vH8zdxS355E/CN/G4KGz2B+5U52rQCQZNO/DTrsbhDgbX3q
CefuxY/hTa5f4Xq4bruJwk0SO70TeY0mvYT4ipV8cZzCNDN5+tqmrYrwitud+tmP
I4OwtAvShvIYL0/QAe0oD1KoU6+Jbl6mGapR6dkSgD8SdX8M6mjYfRpProonmCB0
WURUdeMkGVmOC8BuNlKNG9fvYihDZHy+8iAjEm81JNHgU+RyHNtAGVp/o21oFsXZ
ygdYfspX65y+ZwbLS/HiUC7sIdOY8DpavyZXpORBtIgSKClajt6onqEKJFohh92H
+r0TbMTKfsS/mkON8FKxOUaiQ4yYHv9MZCnhO0EtpNjcfiH3l4wCy0H5WJkhG6Qu
quq9ASLu1yAiYHfnAn6oxOEDL9mJBOvF0718ggJ5VgX069gLkknH3wr6Fp8HPFNL
JTk9nxcRF1Vf5wu2kUpYLFks4EK9wcv+nXjP/qr+ADcxbR69jpBLIo1HnfFliScK
/4yCREwPGRbEvAIqMEYpTeFYSQARAQABtC9TaW1vbiBNY1ZpdHRpZSA8c2ltb24u
bWN2aXR0aWVAY29sbGFib3JhLmNvLnVrPokCNwQTAQoAIQUCVhos+wIbAwULCQgH
AwUVCgkICwUWAgMBAAIeAQIXgAAKCRBSpDoeS3ewWTFsD/wI+1bqHOp053eIDSjr
0A+jAK43U+zQXf23X65f4PFwzHISBHVJsZEPAGDT6EiWpUwWx4W/Rnvvfn5jcdqd
1PyWp2mr0CSE4JQjdI6Wnrh+Px6vzZADuk6dxyidMNDG9MQp1+2MKNROS0JUiVSc
wfOUTlyts8eacRFEK3X+EKUJd1RzmgC1dx4y72r8FNtTP2/dGeBNjqMASE6EHhAT
UqU11PqMqLRMwBuzzJsKkUgkg7ChpC8DnKQHaVCzTZPZT5cu1b7nG0pwSa5Ous7Y
b8kgwbAhJ6XDrcIwaKiamQskYIpCkdSM2ocvqV4DtYsn4lLyYG/LlihuPRk31+cn
HVGl/xciT+O+UGHfwFetP3nPwOFQWfZXBFsfryg8wOeFWTU2V/OwQ4a+49LuhzCV
b86Gdo5pQMyBArtZ0bji1ijoHOl51m+GIH6+to3gDPIaBjYBtsW02Qv9L3yIMNC6
AqXN+lKjIIAaD2rtGsn75VbhCdi9XGzO6uqrD0tsRpodWBKOAaofAFqAPwG+gUHA
gSgGjejahSBDTkEBmn7nEzm8ocdH4xRuCJAy1CHfnZoT1+eWCh26+D66ndVwyZXK
XcGhh/sfSUeWxi6MalcyUgLGgeiaYNlgmd3H8EHn7xmq5n/Ekbz0R2+rN/j8iPUs
DHNSowEPpWVRqBsg4dsqggGNsrkCDQRWGiz7ARAA8HuCGZfoCCgKMVlpxUtdh/c5
xpsTr2GaWkeAcNMfC2NQ/3aw/3QkKx58f19gSHDJxxKKQcyPNCBJC+QBOfF7XdL1
VECfEI5hRntX0fKm5fsAm/NhS3H3IWkFFMduFfGFNyy740onDSGljGA61zqaBE5C
blCAK5yO4Vaoo2r/d2BifqKJN89p57NBIo54PIgA6gQi1MKFAsUVcR5/Hqgaj8p1
tfCvGtAjwhfnQgc1g5WUrXrxwSNbpUwGDabsrtrWqcTZdWNqiI74/8191Ax9mfo9
cs2M1uysxkqAIms+Zm5tCVLddbca3GatSB+TS9dNno33s90pq3ZwYKPBLPZmNyKo
HIVE9o3CGQjK5JVUQCQVEdcMVCTHcSh62mCSU1SFUPfixU/RrW6lkq5QEf0cWjMd
ulXNnVf1o0ZH//ZeK/n3By2kBTHYD+wzI76Fyb9yfYuJxb8ci5AJuyc4XWDGtIKc
tJcUmxcj06LVSGrCGIJDuDsrRZfKAS45boe7im3ipeUqdHWZ+BNbw4rPciVbIrcC
CzZ+BVdy5kl071g91nqKzdSE4s8Gni6IyOM1LzNipZe2e30ek0tcW30HZSZ44Idw
WmPRRUrwN/+VdkKtWhUwV8FEVMFqjq7gItK7x+BH25hwjAhYhEB+5J0YD2LUdAOM
wfVYUJzkCVZg9SUVsq8AEQEAAYkCJQQYAQoADwIbDAUCVhou6QUJAeE1bgAKCRBS
pDoeS3ewWVPIEACjq7tHZnnkGVTEuI2lJqFCrWhHc+1KowvIV8MIGCKHdH4E9tZ2
9hTg4vf+LdmBsDJgTmXvag9flK2OihCtPEsQQzVTmJmGtbJydRKfJu4E8b1kaiwl
42qRV7GGTnEW9zCC101aUYF1zcSX1y7vdZ5g5oMpV5ep+7D+SWCrSZ/AokeOznoO
+chJPDv1iggE9w+D5Wpr2AAfHjTUHVKOw9GCiMgZc16EsL4gweVmnK/rMVkt0Tnn
tLUgx5aZzIyN+OKYUgLQkHPCidYYcGonEDDB5//1cww+PHAbwA8P1V2ogDq8HGdS
KiqF3UntiyInZs6wAyFMPBrakxgGKREYwtB2MdbTx9yV2HZInrMWnmwU601xcHJP
oVQXlSr8w3KHCwS3h2N/8L/ETqCYRaqQYuQV4/zGDP+fYdG3TquOVPZiHUbeJ8u3
Fmdt6nPpQjno5eElNlqM8H/fa+jlc2YWhlYDwZVQx8GerXmr78NVSH/D9r10iFDP
iUGrmKlxlkKKh042or+RHfyontdcbun8DZHAaoBmOyveyr2914akbLMlR5sRbdGQ
DFcYZDISIWmDjsA/L7I0eR2CtSACGXiUi3Y7Yh2iqKMKgBA3xTYjJIJ4rf4Yfdcb
/zvs1ZPVJqKNNtWZpHJc6zrDcoxslRTrNLLkPf+mEUkOXX/7HF3pCysVY7kCDQRW
Gi3/ARAA1JHCShXLX8wvdNEGoirOgwUOu+2xvqsC4k7tx8KclaXezSwOFKemD3yg
BCvgb/Byvdr6xkpZLsqadeEQZyfCjd3f3uxoqKAhWH8vfUteApKuXdqH87MjpCoH
jW2CCZFef4BGe5PDddBRftW18jQbNgMkoKTLwJpxYxiv2GIhWgnzPmCVifura5LI
DX5pL+oq5tbQEJwLWiDedbRcZCQPB0l3bdbx9x9BPFQ7L0XbvLB/tYjas5aXAH1e
wGAnjBWMeA17WdZjxBIb24iFqsqfSeaLeQPUnyelU+TrWnCwWjv+gKBIwBK8skqb
1sELSrT+zkfWz5clKHyHpPlKYHjN6acrvqCtXFMWyRCIN0QbdK46NgVaTobAdkwC
AZ8o3SCQ5ZFyKIXpc9Ym7ikaF+VPilk1tRDuSlFNfeOh8nJa3cKkY0o9BUjnDmSE
o4GxT4xoUAfDvvv8kXf9e5eFsaBX4YgWMnJd/nRgQbrsoMK/fC53KEeYaIDElLcg
u67VEO8//wJ62dtp9OCPP6QyNrEARiP0iTT+4kLaWZG+a2ujb7oLo8QU9/tbMm2Q
8LopoBw+ybVHobTlVqFEc2p1BQcc5CqD/dBNrD98xn3ic3lGIFECPQXjAbsA1P/J
U9802LMOz06lZO4BnoSCW4npzwG/ShNMFDk2CPiGA3blIvUuOiMAEQEAAYkERAQY
AQoADwUCVhot/wIbAgUJAeEzgAIpCRBSpDoeS3ewWcFdIAQZAQoABgUCVhot/wAK
CRACI6B42/9LZrQHEACStVbJfYQ8HNG1EFOCJjTY56zCzFIA0f2W3UNC09XMkDom
SwB9E62k5N6YZo2ZMFIkrTezR56P0tYioCoCacECleqeuETfxx8S0OFIuGEjZ4MU
w0QKEiGdlx/Kbn++KAtNj46QEIIMzSswlORHMaznnaDdorfR5nJ0KM8hv62QTI6B
6fmtsFs6xww0zdYeK/2pwi/mnYYApsQ1wqzR/kZR8Vuqu/gsyzKk/q3s/vCClJIS
Tl7QLuEHqtm4iL6b9IwKTb0vm37IaME7ATn9i44w8Qm8YQkJKKj6Ht6jlnX90usE
+cPBX464keIET5ptCLXqxNbHX05ASSMblJTwo/rRy2Zmj8VDfg22B5ArhuXaOZzp
vwtLHb3qO5S85c4gIbK7RRcSZayCpEq0FJNZJYp1tJhkFNQdBroGD5sAgIO7Es1u
yB3wyLsHgUXuRqaNyjkxe5wCXuw6R2xuzxco3E4XFRGGxf9B1uHgdumT5MEthKN4
ZSotKKoJ/saM7nIdsPyEJezTBY21III/wE7HfdWsIffBWQuRyMBBkfW3PcUZ6L+i
AhZUw7Y4Kv2s/cDODu7OYnpRmu5CNJIKALOPGAOsAsmahyj6pNWj88dYuZq9uOtR
tSYSUzwUKK0WSaHD7el9cekWYdPmmHhnykWMRSjzyX3JPmHqmp0qm7VxHhGZ44uT
D/9vCkllQPSLYgUDPVFdAmIXeWrq9c8sOD120DIraqNy0dF1wWx4U1vXG2JVGzJK
roOqaMS0LMoY98Sfi0c8Q5DSY9wbayvhC88kK9jx7i5hF+/JA82JN3S3VASH8++F
mKR2wca4eXEqwmIhcOrLFoJDLulTLH0iIe1SFcUZwJMno8a8UKNWMLP9H65v3DqN
khO+6gDt20ZQ25rrxDrtEpWIJmXlbUj18EXHpcr9q6SsVvfhT0qCVlAalpmG9/Da
h9N4i7yv6RWEzVt+I0whP25gn0ydQIkF8j8La9mM/54dURKE7IdPUG+VJdjS6E/o
iJKVZHEW4X/1e7TbFtR0lgkkurPhWtVsTI/CzXP4r+aWMkQKp5NLnIi15tsthbOv
LnCSHt5dsiSzQYs4ullGV1a6Pdsk0W6Yd3lzz8hqE+J+yZtnoPp/t2hARzA/OZjq
kqmwxVc883Yv5BC7HavMwjgBVpAsfJu/cbSOGXE1/zKSr8lLCESs8pnKUUhpfLfk
glw9jYNWltYRvCOaRmJyHb2WcS1ZOchmTXYZLgwAwoaLWdlA/HdNk8A72SZ8ozb0
JaIwQ4s8Ko4BVRxJ8qJOVvipPMFRxhbC6JTZA9O9IdNqd7d/uPrpnVo66FHtjSh1
k6zuGWYYg4LYPLVeHGazt6Qba5olz0ztN21wnNVCQQOK7g==
=HXzH
-----END PGP PUBLIC KEY BLOCK-----
