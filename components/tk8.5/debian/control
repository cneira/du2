Source: tk8.5
Section: libs
Priority: optional
Maintainer: Debian Tcl/Tk Packagers <pkg-tcltk-devel@lists.alioth.debian.org>
Uploaders: Sergei Golovan <sgolovan@debian.org>
Build-Depends: debhelper (>= 9.0.0), dpkg-dev (>= 1.16.1~), x11proto-core-dev,
 libx11-dev, libxss-dev, libxext-dev, libxft-dev, tcl8.5-dev (>= 8.5.14-3)
Standards-Version: 3.9.6
Homepage: http://www.tcl.tk/

Package: tk8.5
Section: interpreters
Priority: optional
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: xterm | x-terminal-emulator
Conflicts: tk40 (<= 4.0p3-2), libtk-img (<< 1.2.5)
Multi-Arch: foreign
Description: Tk toolkit for Tcl and X11, v8.5 - windowing shell
 Tk is a cross-platform graphical toolkit which provides the Motif
 look-and-feel and is implemented using the Tcl scripting language.
 This package contains the windowing Tcl/Tk shell (wish).

Package: libtk8.5
Section: libs
Priority: optional
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: libtcl8.5 (>= 8.5.14-3), ${shlibs:Depends}, ${misc:Depends}
Conflicts: tk8.5 (<<8.5.14-3), tk40 (<= 4.0p3-2), libtk-img (<< 1.2.5)
Replaces: tk8.5 (<<8.5.14-3)
Suggests: tk8.5
Provides: libtk
Multi-Arch: same
Description: Tk toolkit for Tcl and X11 v8.5 - run-time files
 Tk is a cross-platform graphical toolkit which provides the Motif
 look-and-feel and is implemented using the Tcl scripting language.
 This package contains the Tk library and supplementary packages you
 need to run Tk-enabled apps.

Package: libtk8.5-dbg
Section: debug
Priority: extra
Architecture: linux-any
Pre-Depends: ${misc:Pre-Depends}
Depends: libtk8.5 (= ${binary:Version}), ${misc:Depends}
Multi-Arch: same
Description: Symbol files for libtk8.5
 Tk is a cross-platform graphical toolkit which provides the Motif
 look-and-feel and is implemented using the Tcl scripting language.
 This package contains the debug symbols for libtk8.5, to assist
 in debugging applications built with tk8.5.  You will not normally
 need to install this package.

Package: tk8.5-dev
Section: libdevel
Priority: optional
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: dpkg-dev (>= 1.16.1~), x11proto-core-dev, libx11-dev, libxss-dev,
 libxext-dev, libxft-dev, tcl8.5-dev (>= 8.5.14-2),
 libtk8.5 (= ${binary:Version}), tk8.5, ${misc:Depends}
Suggests: tk8.5-doc
Multi-Arch: same
Description: Tk toolkit for Tcl and X11, v8.5 - development files
 Tk is a cross-platform graphical toolkit which provides the Motif
 look-and-feel and is implemented using the Tcl scripting language.
 This package contains the headers and libraries needed to extend
 or embed Tk.

Package: tk8.5-doc
Section: doc
Priority: optional
Architecture: all
Depends: ${misc:Depends}
Suggests: tk8.5
Conflicts: tkdoc, tk8.3-doc, tk8.4-doc, tk8.5 (<< 8.5.14-3~)
Replaces: tk8.5 (<< 8.5.14-3~)
Provides: tkdoc
Description: Tk toolkit for Tcl and X11, v8.5 - manual pages
 Tk is a cross-platform graphical toolkit which provides the Motif
 look-and-feel and is implemented using the Tcl scripting language.
 This package contains the manual pages for the Tk commands.
