Source: libcerf
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org> 
Uploaders: Eugen Wintersberger <eugen.wintersberger@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper (>= 9),
               automake,
               dh-autoreconf
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=debian-science/packages/libcerf.git
Vcs-Git: git://anonscm.debian.org/debian-science/packages/libcerf.gi
Homepage: http://apps.jcns.fz-juelich.de/doku/sc/libcerf

Package: libcerf1
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Complex error function library - binary files
 libcerf is a self-contained numeric library that provides an efficient and
 accurate implementation of the complex error functions, along with Dawson,
 Faddeeva, and Voigt functions.
 . 
 This package contains the runtime binary of the library.

Package: libcerf1-dbg
Architecture: linux-any
Section: debug
Priority: extra
Depends: libcerf1 (=${binary:Version}),
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Complex error function library - debugging symbols
 libcerf is a self-contained numeric library that provides an efficient and
 accurate implementation of the complex error functions, along with Dawson,
 Faddeeva, and Voigt functions.
 . 
 This package contains the debugging symbols for the library.

Package: libcerf-dev
Architecture: any
Section: libdevel
Depends: libcerf1 (= ${binary:Version}),
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Complex error function library - development files
 libcerf is a self-contained numeric library that provides an efficient and
 accurate implementation of the complex error functions, along with Dawson,
 Faddeeva, and Voigt functions.
 .
 This package contains the development files for libcerf. 

Package: libcerf-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Breaks: manpages-dev (<< 3.74)
Replaces: manpages-dev (<< 3.74)
Description: Complex error function library - development files
 libcerf is a self-contained numeric library that provides an efficient and
 accurate implementation of the complex error functions, along with Dawson,
 Faddeeva, and Voigt functions.
 .
 This package contains the man pages and html documentation for libcerf.
