Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libisofs
Upstream-Contact: libburn-hackers@pykix.org
Source: http://files.libburnia-project.org/releases/

Files: *
Copyright: 2007-2008 Mario Danic <mario.danic@gmail.com>
    2007-2008 Vreixo Formoso <metalpain2002@yahoo.es>
    2007-2016 Thomas Schmitt <scdbackup@gmx.net>
    2012      Vladimir Serbinenko
License: GPL-2+

Files: debian/*
Copyright: 2007      Mario Danic <mario.danic@gmail.com>
           2008-2008 Simon Huggins <huggie@earth.li>
    2008-2009 Matthew Rosewarne
    2011      Mats Erik Andersson <mats.andersson@gisladisker.se>
    2008-2012 George Danchev <danchev@spnet.net>
    2015-2016 Thomas Schmitt <scdbackup@gmx.net>
License: GPL-2

Files: demo/*
Copyright: 2007-2014, Vreixo Formoso, Thomas Schmitt
License: GPL-2+

Files: libisofs/*
Copyright: 2009-2015, Thomas Schmitt
  2007, 2008, Vreixo Formoso
License: GPL-2+

Files: libisofs/buffer.c
  libisofs/buffer.h
  libisofs/data_source.c
  libisofs/ecma119_tree.h
  libisofs/filesrc.c
  libisofs/filesrc.h
  libisofs/filter.h
  libisofs/find.c
  libisofs/fsource.c
  libisofs/iso1999.h
  libisofs/messages.h
  libisofs/tree.h
  libisofs/util_rbtree.c
  libisofs/writer.h
Copyright: 2007, 2008, Vreixo Formoso
License: GPL-2+

Files: libisofs/ecma119.c
  libisofs/joliet.c
  libisofs/rockridge.c
  libisofs/rockridge.h
  libisofs/util.c
Copyright: 2009-2015, Thomas Schmitt
  2007, Vreixo Formoso
  2007, Mario Danic
License: GPL-2+

Files: libisofs/filters/*
Copyright: 2009-2013, Thomas Schmitt
License: GPL-2+

Files: libisofs/hfsplus.c
Copyright: 2012, Vladimir Serbinenko
  2011, 2012, Thomas Schmitt
  2007, Vreixo Formoso
  2007, Mario Danic
License: GPL-2+

Files: libisofs/hfsplus.h
Copyright: 2012, Vladimir Serbinenko
License: GPL-2+

Files: libisofs/hfsplus_case.c
  libisofs/md5.c
  libisofs/md5.h
Copyright: 2009-2013, Thomas Schmitt
License: GPL-2+

Files: libisofs/hfsplus_classes.c
Copyright: 2012, Vladimir Serbinenko
  2012, Thomas Schmitt
  1991-2012, Unicode, Inc.
License: GPL-2+

Files: libisofs/hfsplus_decompose.c
Copyright: 2012, Vladimir Serbinenko
  2012, Thomas Schmitt
License: GPL-2+

Files: libisofs/joliet.h
Copyright: 2007, Vreixo Formoso
  2007, Mario Danic
License: GPL-2+

Files: libisofs/libisofs.h
Copyright: 2009-2015, Thomas Schmitt
  2007, 2008, Vreixo Formoso, Mario Danic
License: GPL-2+

Files: libisofs/make_isohybrid_mbr.c
Copyright: 2008-2015, Thomas Schmitt
  2002-2008, H. Peter Anvin
License: GPL-2+

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.
