Source: labltk
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>
Build-Depends:
 debhelper (>= 9),
 ocaml-nox (>= 4.02.1),
 ocaml-findlib,
 tk-dev,
 dh-ocaml
Standards-Version: 3.9.6
Homepage: https://forge.ocamlcore.org/projects/labltk/
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-ocaml-maint/packages/labltk.git
Vcs-Git: git://anonscm.debian.org/pkg-ocaml-maint/packages/labltk.git

Package: labltk
Architecture: any
Depends:
 liblabltk-ocaml-dev,
 ${shlibs:Depends},
 ${misc:Depends},
 ${ocaml:Depends}
Provides:
 ${ocaml:Provides}
Recommends: ocaml-findlib
Breaks: ocaml (<< 4.02.1)
Replaces: ocaml (<< 4.02.1)
Description: OCaml bindings to Tcl/Tk (executables)
 mlTk is a library for interfacing OCaml with the scripting language
 Tcl/Tk.
 .
 In addition to the basic interface with Tcl/Tk, this package contains
 the OCamlBrowser code editor / library browser written by Jacques
 Garrigue.

Package: liblabltk-ocaml-dev
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${ocaml:Depends}
Provides:
 ${ocaml:Provides}
Recommends: ocaml-findlib
Breaks: ocaml (<< 4.02.1)
Replaces: ocaml (<< 4.02.1)
Description: OCaml bindings to Tcl/Tk (dev. libraries)
 mlTk is a library for interfacing OCaml with the scripting language
 Tcl/Tk.
 .
 In addition to the basic interface with Tcl/Tk, this package contains
  * the "jpf" library, written by Jun P. Furuse; it contains a "file
    selector" and "balloon help" support;
  * the "frx" library, written by Francois Rouaix;
  * the "tkanim" library, which supports animated gif loading/display.
 .
 This package contains development libraries.

Package: liblabltk-ocaml
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${ocaml:Depends}
Provides:
 ${ocaml:Provides}
Recommends: ocaml-findlib
Breaks: ocaml-base (<< 4.02.1)
Replaces: ocaml-base (<< 4.02.1)
Description: OCaml bindings to Tcl/Tk (runtime libraries)
 mlTk is a library for interfacing OCaml with the scripting language
 Tcl/Tk.
 .
 In addition to the basic interface with Tcl/Tk, this package contains
  * the "jpf" library, written by Jun P. Furuse; it contains a "file
    selector" and "balloon help" support;
  * the "frx" library, written by Francois Rouaix;
  * the "tkanim" library, which supports animated gif loading/display.
 .
 This package contains runtime libraries.
