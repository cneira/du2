libauthen-pam-perl (0.16-3+dilos2) unstable; urgency=low

  * build for dilos

 -- Igor Kozhukhov <igor@dilos.org>  Mon, 12 Mar 2018 17:19:48 +0300

libauthen-pam-perl (0.16-3) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Update stand-alone license paragraphs to commonly used version (not
    limiting Debian to GNU/Linux, directly linking to GPL-1)
  * Declare compliance with Debian Policy 3.9.5
  * Switch to source format 3.0 (quilt)
  * Add spelling.patch fixing POD typos
  * Expand long description a bit
  * Ensure hardening flags really get passed

 -- Florian Schlichting <fsfs@debian.org>  Sat, 15 Mar 2014 23:43:50 +0100

libauthen-pam-perl (0.16-2) unstable; urgency=low

  [ gregor herrmann ]
  * Take over for the Debian Perl Group with maintainer's permission
    (http://lists.debian.org/debian-perl/2008/06/msg00039.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Florian Ragwitz
    <rafl@debianforum.de>); Florian Ragwitz <rafl@debianforum.de> moved
    to Uploaders.
  * Add debian/watch.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ryan Niebur ]
  * Remove Florian Ragwitz from Uploaders
  * Close ITA (Closes: #523126)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Bump Standards-Version to 3.8.2.
  * Convert debian/copyright to proposed machine-readable format.
  * Refresh rules for debhelper 7.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 20 Jul 2009 18:13:02 +0200

libauthen-pam-perl (0.16-1.1) unstable; urgency=low

  * Non-maintainer upload for the Perl 5.10 transition.
  * Don't try to remove /usr/share/perl5 if it doesn't exist. (Closes: #463550)

 -- Niko Tyni <ntyni@debian.org>  Wed, 02 Apr 2008 16:57:42 +0300

libauthen-pam-perl (0.16-1) unstable; urgency=low

  * New upstream release.

 -- Florian Ragwitz <rafl@debianforum.de>  Thu, 22 Sep 2005 06:13:25 +0200

libauthen-pam-perl (0.15-2) unstable; urgency=low

  * New Maintainer (Closes: #322095).
  * Rewrote debian/rules.
  * Updated Standards-Version.
  * Build-Depend on debhelper 4.
  * Better Description.
  * Updated debian/copyright.

 -- Florian Ragwitz <rafl@debianforum.de>  Fri, 12 Aug 2005 16:03:54 +0200

libauthen-pam-perl (0.15-1) unstable; urgency=low

  * New upstream version.

 -- Davide Puricelli (evo) <evo@debian.org>  Sat,  9 Apr 2005 16:45:24 +0200

libauthen-pam-perl (0.14-1) unstable; urgency=low

  * New upstream version; closes: #195126.

 -- Davide Puricelli (evo) <evo@debian.org>  Fri,  5 Sep 2003 19:15:39 +0200

libauthen-pam-perl (0.13-3) unstable; urgency=low

  * debian/copyright: added a pointer to Perl license; closes: #157546.

 -- Davide Puricelli (evo) <evo@debian.org>  Mon,  2 Sep 2002 10:29:15 +0200

libauthen-pam-perl (0.13-2) unstable; urgency=low

  * Recompiling against perl 5.8.0. 

 -- Davide Puricelli (evo) <evo@debian.org>  Sat,  3 Aug 2002 16:07:22 +0000

libauthen-pam-perl (0.13-1) unstable; urgency=low

  * New upstream version.

 -- Davide Puricelli (evo) <evo@debian.org>  Fri,  2 Aug 2002 12:30:12 +0200

libauthen-pam-perl (0.12-2) unstable; urgency=medium

  * debian/control: "Priority: optional" closes: #110104.

 -- Davide Puricelli (evo) <evo@debian.org>  Fri, 31 Aug 2001 19:03:10 +0200

libauthen-pam-perl (0.12-1) unstable; urgency=low

  * New upstream version.

 -- Davide Puricelli (evo) <evo@debian.org>  Sat, 21 Jul 2001 14:37:37 +0200

libauthen-pam-perl (0.11-1) unstable; urgency=low

  * New upstream version.
  * Updated debian/rules because of perl 5.6 and new perl policy.

 -- Davide Puricelli (evo) <evo@debian.org>  Thu, 15 Feb 2001 19:19:22 +0100

libauthen-pam-perl (0.10-2) unstable; urgency=low

  * New maintainer.
  * Updated debian/rules.
  * Added Build-Depends.

 -- Davide Puricelli (evo) <evo@debian.org>  Tue, 15 Aug 2000 13:28:28 +0200

libauthen-pam-perl (0.10-1) unstable; urgency=low

  * New upstream version, closes: #58158

 -- Ben Collins <bcollins@debian.org>  Sun, 23 Jul 2000 17:20:40 -0400

libauthen-pam-perl (0.09-2) frozen unstable; urgency=low

  * Send this to frozen too (see changelog below)

 -- Ben Collins <bcollins@debian.org>  Fri, 28 Apr 2000 15:45:49 -0400

libauthen-pam-perl (0.09-1) unstable; urgency=low

  * Fixed new() operator, closes: #56552
  * New upstream version, bug fixes, Changes:
      0.09  2000-Feb-03
        - fixed a bug in PAM.xs when more then one messages are given
          to the conversation function (thanks to Oleg Bulavsky
          <bulch@sibnet.ru>)
        - small changes to the test script
        - several old constant symbols are exported only on request;
          also created two tags :constants and :functions for easier
          exporting only some of the symbols
  * s,/usr/man,/usr/share/man,

 -- Ben Collins <bcollins@debian.org>  Fri, 28 Apr 2000 13:12:15 -0400

libauthen-pam-perl (0.08-1) unstable; urgency=low

  * New upstream version
  * FHS compliant, now standard version 3.0.1.1
  * acknowledged NMU, closes: #41532

 -- Ben Collins <bcollins@debian.org>  Sun,  3 Oct 1999 15:54:43 -0400

libauthen-pam-perl (0.05-1.1) unstable; urgency=low

  * NMU for the perl upgrade. Closes: #41532
  * Corrected the rules files to conform to perl policy 1.0.1
  * Compiled with perl-5.005.
  * Upgraded standars-version to 2.5.1 

 -- Raphael Hertzog <rhertzog@hrnet.fr>  Mon, 19 Jul 1999 19:08:21 +0200

libauthen-pam-perl (0.05-1) unstable; urgency=low

  * New upstream source

 -- Ben Collins <bcollins@debian.org>  Fri, 16 Apr 1999 12:40:27 -0400

libauthen-pam-perl (0.02-1) unstable; urgency=low

  * Initial Release.

 -- Ben Collins <bcollins@debian.org>  Sun,  7 Mar 1999 00:16:46 -0500

