Source: cloog
Priority: optional
Maintainer: Debian GCC Maintainers <debian-gcc@lists.debian.org>
Uploaders: Matthias Klose <doko@debian.org>, Michael Tautschnig <mt@debian.org>
Build-Depends: debhelper (>= 5), autotools-dev,
  libisl-dev (>= 0.15), libgmp-dev,
  texinfo, help2man
# Build-Depends-Indep: libpod-latex-perl | perl (<< 5.17.0) # not needed, no docs built
Standards-Version: 3.9.6
Section: libs
Homepage: http://www.CLooG.org

Package: libcloog-isl-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libisl-dev (>= 0.14), libgmp-dev, libcloog-isl4 (= ${binary:Version}),
  dpkg (>= 1.15.4) | install-info, ${shlibs:Depends}, ${misc:Depends}
Conflicts: libcloog-ppl-dev
Description: Chunky Loop Generator (development files)
 CLooG is a software which generates loops for scanning Z-polyhedra. That is,
 CLooG finds the code or pseudo-code where each integral point of one or more
 parametrized polyhedron or parametrized polyhedra union is reached. CLooG is
 designed to avoid control overhead and to produce a very efficient code.
 .
 This package contains the development files and the cloog binary.

Package: libcloog-isl4
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Chunky Loop Generator (runtime library)
 CLooG is a software which generates loops for scanning Z-polyhedra.
 .
 This package contains the runtime library.

Package: cloog-isl
Section: libs
Architecture: any
Depends: libcloog-isl4 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}
Conflicts: cloog-ppl (<< 0.15.11-4), libcloog-ppl-dev (<< 0.15.11-1)
Description: Chunky Loop Generator (runtime library)
 CLooG is a software which generates loops for scanning Z-polyhedra.
 .
 This package contains the cloog-isl binary.
