Source: ninja-build
Section: devel
Priority: optional
Maintainer: Felix Geyer <fgeyer@debian.org>
Build-Depends: asciidoc,
               debhelper (>= 9),
               docbook-xml,
               docbook-xsl,
               help2man,
               python,
               re2c,
               xsltproc
Homepage: https://ninja-build.org/
Vcs-Git: https://anonscm.debian.org/git/collab-maint/ninja-build.git
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/ninja-build.git
Standards-Version: 3.9.6

Package: ninja-build
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Conflicts: ninja
Description: small build system closest in spirit to Make
 Ninja is yet another build system. It takes as input the interdependencies of
 files (typically source code and output executables) and orchestrates
 building them, quickly.
 .
 Ninja joins a sea of other build systems. Its distinguishing goal is to be
 fast. It is born from the Chromium browser project, which has over 30,000
 source files and whose other build systems can take ten seconds to start
 building after changing one file. Ninja is under a second.
